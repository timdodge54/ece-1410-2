#include <iostream>


#ifndef RATIONAL_H
#define RATIONAL_H

class Rational {
	int numerator, denominator;
	
	public:
	
	Rational (int, int);
		void set_values(int, int); 
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void reduce(void);
		void print_frac(void);
		
	
};

#endif

