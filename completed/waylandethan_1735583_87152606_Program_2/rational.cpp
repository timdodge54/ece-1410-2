#include "rational.h"
#include <cstdlib>
using namespace std;



/******************************************************************************
@name	Rational
@breif	Default constructor that sets the values of numerator and denominator
@param	int a, int b
@retval	none
******************************************************************************/
Rational::Rational (int a, int b) {
	Rational::set_values(a, b);
}

/******************************************************************************
@name	Rational::add
@breif	adds value of inputed fraction to fraction of class Rational, and sets
		new Rational values to sum
@param	int n, int d
@retval	none
******************************************************************************/
void Rational::add(int n, int d) {
	numerator = numerator * d + denominator * n;
	denominator = denominator * d;
}

/******************************************************************************
@name	Rational::sub
@breif	subtracts value of inputed fraction from fraction of class Rational, 
		and sets new Rational values to difference
@param	int n, int d
@retval	none
******************************************************************************/
void Rational::sub(int n, int d) {
	numerator = numerator * d - denominator * n;
	denominator = denominator * d;
}

/******************************************************************************
@name	Rational::mul
@breif	multiplies value of inputed fraction to fraction of class Rational, 
		and sets new Rational values to product
@param	int n, int d
@retval	none
******************************************************************************/
void Rational::mul(int n, int d) {
	numerator = numerator * n;
	denominator = denominator * d;
}

/******************************************************************************
@name	Rational::div
@breif	divides value of inputed fraction from fraction of class Rational, 
		and sets new Rational values to quotient
@param	int n, int d
@retval	none
******************************************************************************/
void Rational::div(int n, int d) {
	numerator = numerator * d;
	denominator = denominator * n;
}
/******************************************************************************
@name	Rational::set_values
@breif	sets numerator of item of class Rational to input x and denominator to
		y
@param	int x, int y
@retval	none
******************************************************************************/
void Rational::set_values(int x, int y){
	numerator = x;
	denominator = y;
	
}


/******************************************************************************
@name	Rational::reduce
@breif	reduces input fration of class Rational to its simples form using
		Euclidian algorithm
@param	none
@retval	none
******************************************************************************/
void Rational::reduce(void) {
	int a, b;
	a = numerator;
	b = denominator;
	
	while (a != b) {
		if (b > a)	b = b-a;
		else a = a - b;
	}
	numerator = numerator / a;
	denominator = denominator / a;
}

/******************************************************************************
@name	Rational::print_frac
@breif	prints value of fraction
@param	none
@retval	none
******************************************************************************/
void Rational::print_frac(void) {
	cout << "Fraction is: " << numerator << "/" << denominator << endl << endl;
}

