#include "rational.h"
using namespace std;

/******************************************************************************
@name	main
@breif	asks for initial numerator and denominator to be stored in a class
		Rational. Enters a menu driven loop that allows the user to perform
		arithmatic operations on the fraction
		y
@param	none
@retval	int
******************************************************************************/

//Begin
int main() {
	int n, d;
	char input;
	//Ask for initial numerator
	cout << "Enter numerator: ";
	cin >> n;
	//Ask for initial denominator
	cout << "Enter denominator: ";
	cin >> d;
	
	//Declare fraction f1, reduce it, and print initial value
	Rational f1 (n, d);
	f1.reduce();
	f1.print_frac();
	
	//Menu Loop
	do {
		
		//Print options of operations to perform
		cout << "1. Add a rational\n";
		cout << "2. Subtract a rational\n";
		cout << "3. Multiply by a rational\n";
		cout << "4. Divide by a rational\n";
		cout << "0. Exit\n";
		
		//Get input
		cout << "Enter selection: ";
		cin >> input;
		if (input == '1' || input == '2' || input == '3' || input == '4') {
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
		}
		//try/catch block makes sure input is proper type char
		try {
			throw input;
		}
		catch (char e) {
			switch (e) {
				//If input is 1, add fraction and reduce
				case '1':	f1.add(n, d);
						f1.reduce();
						break;
				//If input is 2, subract fraction and reduce
				case '2': 	f1.sub(n, d);
						f1.reduce();
						break;
				//If input is 3, multiply fraction and reduce
				case '3':
						f1.mul(n, d);
						f1.reduce();
						break;
				//If input is 4, divide fraction and reduce
				case '4':	f1.div(n, d);
						f1.reduce();
						break;
				//If input is 0, do nothing
				case '0': 
						f1.reduce();
						break;
				//If input is invalid, print error message.
			default:	cout << "'" << e << "' is not a valid input!" << endl;
			
					break;
			
			}
		}
		//If type is invalid, print error message
		catch (...) {
			cout << "Invalid input!" << endl;
		}
	
	//print fraction
	f1.print_frac();
	cout << endl;
	cout << endl;
	
	//End loop when input is 0
	} while (input != '0');
	
	

	
	return 0;
}

//End