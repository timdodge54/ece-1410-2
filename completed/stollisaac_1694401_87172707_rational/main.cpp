
/******************************************************************************
* @name 	main
* @brief 	performs operation on a fraction
* @param 	none
* @retval 	none
******************************************************************************/
#include <iostream>
#include "rational.h"

using namespace std;

int main(void)
{
	int numerator, denominator;
	char selection = '9';

	// asks for a numerator and denominator and creates class object
	// reduces fraction if able and prints back to screen
	cout << "Enter a Numerator: ";
	cin >> numerator;
	cout << "Enter a denominator: ";
	cin >> denominator;
	Rational frac(numerator, denominator);
	cout << "Reduced Fraction is ";
	frac.getResults();
	cout << endl << endl << endl;

	// main menu loop
	while (selection != '0')
	{
		// prints menu and asks for selection
		cout << "1. " << "Add a rational" << endl;
		cout << "2. " << "Subtract a rational" << endl;
		cout << "3. " << "Multiply a rational" << endl;
		cout << "4. " << "Divide a rational" << endl;
		cout << "0. " << "Exit" << endl;
		cout << "Enter selection: ";
		cin >> selection;

		// error handling
		try
		{
			if (selection > '4' || selection < '0')
			{
				throw selection;
			}
		}
		catch (char e)
		{
			cout << "Illegal menu selection; try again.";
			cout << endl << endl << endl;
		}

		// performs operations based on menu selection
		switch (selection)
		{
			// adds fraction
			case '1':
				cout << "Enter numerator: ";
				cin >> numerator;
				cout << "Enter denominator: ";
				cin >> denominator;

				Rational(numerator, denominator);
				frac.add(numerator, denominator);
				break;

			// subracts fraction
			case '2':
				cout << "Enter numerator: ";
				cin >> numerator;
				cout << "Enter denominator: ";
				cin >> denominator;

				Rational(numerator, denominator);
				frac.sub(numerator, denominator);
				break;

			// multiplies fraction
			case '3':
				cout << "Enter numerator: ";
				cin >> numerator;
				cout << "Enter denominator: ";
				cin >> denominator;

				Rational(numerator, denominator);
				frac.mul(numerator, denominator);
				break;

			// divides fraction
			case '4':
				cout << "Enter numerator: ";
				cin >> numerator;
				cout << "Enter denominator: ";
				cin >> denominator;

				Rational(numerator, denominator);
				frac.div(numerator, denominator);
				break;

			// closes program
			case '0':
				selection = '0';
				break;
		}
		// prints results to screen
		if (selection > '0' && selection < '5')
		{
			frac.getResults();
			cout << endl << endl << endl;
		}
	}
	return 0;
}