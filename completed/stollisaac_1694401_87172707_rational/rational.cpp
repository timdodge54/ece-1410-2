#include "rational.h"
#include <iostream>
#include <cmath>

using namespace std;
/******************************************************************************
* @name 	Rational
* @brief 	creates class object
* @param 	int n, int d
* @retval 	none
******************************************************************************/
Rational::Rational(int n, int d)
{
	num = n;
	den = d;
	reduce();
}

/******************************************************************************
* @name 	add
* @brief 	adds two fractions
* @param 	int n, int d
* @retval 	none
******************************************************************************/
void Rational::add(int n, int d)
{
	num = (num * d) + (n * den);
	den = den * d;
	reduce();
}

/******************************************************************************
* @name 	sub
* @brief 	subtracts two fractions
* @param 	int n, int d
* @retval 	none
******************************************************************************/
void Rational::sub(int n, int d)
{
	num = (num * d) - (n * den);
	den = den * d;
	reduce();
}

/******************************************************************************
* @name 	mul
* @brief 	multiplies two fractions
* @param 	int n, int d
* @retval 	none
******************************************************************************/
void Rational::mul(int n, int d)
{
	num = num * n;
	den = den * d;
	reduce();
}

/******************************************************************************
* @name 	div
* @brief 	divides two fractions
* @param 	int n, int d
* @retval 	none
******************************************************************************/
void Rational::div(int n, int d)
{
	num = num * d;
	den = den * n;
	reduce();
}

/******************************************************************************
* @name 	getResluts
* @brief 	prints results of the two fractions
* @param 	none
* @retval 	none
******************************************************************************/
void Rational::getResults()
{
	cout << num << "/" << den;
}

/******************************************************************************
* @name 	reduce
* @brief 	finds the GCF and reduces the fraction
* @param 	none
* @retval 	none
******************************************************************************/
void Rational::reduce()
{
	int exp = 0;
	int factor = 0;
	int n = num;
	int d = den;

	while ((n % 2 == 0) && (d % 2 == 0)) 
	{
		n = n / 2;
		d = d / 2;
		exp = exp + 1;
	}
	while (n != d)
	{
		if (n % 2 == 0)
		{
			n = n / 2;
		}
		else if (d % 2 == 0)
		{
			d = d / 2;
		}
		else if (n > d)
		{
			n = (n - d) / 2;
		}
		else
		{
			d = (d - n) / 2;
		}
		
	}
	factor = n * pow(2, exp);
	num = num / factor;
	den = den / factor;
}