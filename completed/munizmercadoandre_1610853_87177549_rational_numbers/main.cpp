/*
2/2/2023
A02290559

FUnction Title: main.cpp

Summary: calculate rational fractions entered by user

Input: user inputs numbers
Outputs: numbers in a fraction

pseudocode
Begin
	include rational.h file
	ask user to enter numerator
	ask user to enter denominator
	create fraction
	ask user if he wants to add a rational
	ask user if he wants to substract a rational
	ask user if he wants to multiply a rational
	ask user if he wants to divide rational
End
*/
//Begin
#include <iostream>

// include rational.h file
#include "rational.h"

int main(void)
{
	int n;
	int d;

	// ask user to enter numerator
	std::cout << "Enter numerator: \n";

	//ask user to enter denominator
	std::cout << "Enter denominator: \n";

	// create fraction
	std::cout << "Fraction reduces to  \n";

	// ask user if he wants to add a rational
	std::cout << "1) add a rational\n";

	// ask user if he wants to substract a rational
	std::cout << "2) substract a rational \n";

	// ask user if he wants to multiply rational
	std::cout << "3) multiply by a rational\n";

	// ask user if he wants to divide rational
	std::cout << "4) divide by a rational\n";

	// end
	std::cout << "0 end\n";


	return 0;

}
// end