#include "rational.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cmath>


using namespace std;
/*************************
*	@name Rational
*	@brief constructs rational class and sets the starting values of 
numerator and denominator
*	@param int n, int d
*	@retval none
******************/
Rational::Rational (int n, int d)
{
numerator=n;//setting up the numerator
denominator=d;//setting up denominator
reduce(numerator, denominator);
cout <<"Fraction reduces to " << numerator << "/" << denominator << endl;
}
/*************************
*	@name add
*	@brief adds together the past rational with the new
*	@param n and d
*	@retval none
******************/
void Rational::add (int n, int d)
{
	//getting the same 
	// denominator then multiplying top and bot and adding the numerators
	numerator = (n * denominator) + (numerator * d);
	denominator = (d * denominator);
	reduce(numerator, denominator);


}
/*************************
*	@name sub
*	@brief subtracts the new value from the old
*	@param n and d
*	@retval none
******************/
void Rational::sub(int n, int d)
{
	//finding a common denominator
//	and multiplying the numerators then subtracting
	numerator = (numerator*d)-(n*denominator);
	denominator = (d * denominator);
	reduce(numerator, denominator);
	
}
/*************************
*	@name div
*	@brief divides the old rational by the new
*	@param n and d
*	@retval none
******************/
void Rational::div(int n, int d)
{//multiple by the reciprical
	numerator = d * numerator;
	denominator = n * denominator;
	reduce(numerator, denominator);
	
}
/*************************
*	@name mul
*	@brief multiples the two rationals
*	@param n and d
*	@retval none
******************/
void Rational::mul(int n, int d)
{//cross multiply
	numerator = n * numerator;
	denominator = d * denominator;
	reduce(numerator, denominator);
	

}
/*************************
*	@name reduce
*	@brief finds the GCF of the denominator and numerator
*	@param n and d
*	@retval none
******************/
void Rational::reduce(int, int)
{
	GCFN = numerator;
	GCFD = denominator;
	while(GCFN != GCFD)
		// sets up the loop for finding the GCF of the numerator and denominator
	{if(GCFD < 0||GCFN < 0)
		{break;
		}
		if(GCFN > GCFD)
		{
			GCFN = GCFN - GCFD;
		}
		if(GCFD > GCFN)
		{
			GCFD = GCFD - GCFN;
		}
	}
	numerator=numerator/GCFN;
	denominator=denominator/GCFN;
	cout << numerator << "/" << denominator<<endl<<endl;
}

