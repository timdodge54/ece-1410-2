#ifndef RATIONAL_H
#define RATIONAL_H
class Rational {//declaring all the classes and type
private:
	int numerator, denominator, GCF, GCFN , GCFD;
	void reduce(int, int);
	

public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void div(int n, int d);
	void mul(int n, int d);
	

};

#endif