/*************************************************
@name           rational
@brief          write a program that finds the rational number of a fraction
                that adds, subtracts, multiplies, divides and reduces the
                fraction.
@param          none
@return         none
*************************************************/
#include<iostream>
using namespace std;
class Rational                        //class
{
private:
    int numerator;
    int denominator;
public:
                                       //default constructor:
    Rational()
    {
        numerator = 0;
        denominator = 1;
    }
    Rational(int n, int d)
    {
        int g = gcd(n, d);
                                     //To get reduced form of fraction we use gcd
        numerator = n / g;
        denominator = d / g;

    }
    int getNumerator()              //gets numerator
    {
        return numerator;
    }
    void setNumerator(int n)
    {
        numerator = n;
    }
    int getDenominator()            //gets denominator
    {
        return denominator;
    }
    void setDenominator(int d)
    {
        denominator = d;
    }
    Rational add(Rational r)                  //function for addition
    {
        int  num = numerator * r.denominator + r.numerator * denominator;
        int denom = denominator * r.denominator;
        return Rational(num, denom);
    }
    Rational sub(Rational r)                  //function for subtraction
    {
        int num = numerator * r.denominator - r.numerator * denominator;
        int denom = denominator * r.denominator;
        return Rational(num, denom);
    }
    Rational mul(Rational r)                  //function for multiplication
    {
        int num = numerator * r.numerator;
        int denom = denominator * r.denominator;
        return  Rational(num, denom);
    }
    Rational div(Rational r)                  //function for division
    {
        int num = numerator * r.denominator;
        int denom = denominator * r.numerator;
        return Rational(num, denom);
    }

    int gcd(int a, int b)
    {
        if (b == 0)return a;
        return gcd(b, a % b);
    }

    void printRational()                      //prints rational number
    {
        cout << numerator << "/" << denominator;
    }
};

/**********************************************
@name           rational
@brief          write what will print to the screen from the class
@param          none
@return         none
**********************************************/
int main()
{
    int num1, den1;                     //declares the numerator and denominator
    cout << "Enter numerator: ";        //prompts for the screen
    cin >> num1;
    cout << "\nEnter denominator: ";
    cin >> den1;
    cout << "\nFraction reduces to: ";
    Rational a(num1, den1), b, c;
    cout << "\nFirst rational number is: ";
    a.printRational();
    cout << "\nSecond rational number is: ";
    b.printRational();
    cout << "\nAddition of the rational number is: ";
    c = a.add(b);
    c.printRational();
    cout << "\nSubtraction of the rational number is: ";
    c = a.sub(b);
    c.printRational();
    cout << "\nMultiplication of the rational number is: ";
    c = a.mul(b);
    c.printRational();
    cout << "\nDivision of the rational number is: ";
    c = a.div(b);
    c.printRational();
    return 0;                             //end

}