#include "rational.h"

/******************************************************************************
* @name Rational
* @brief Constructor for Rational class.
* @param n numerator to initialize class-owned fraction.
* @param d denominator to initialize class-owned fraction.
* @retval none
******************************************************************************/
Rational::Rational(int n, int d)
{
	// Set initial fraction values and reduce
	m_n = n;
	m_d = d;
	cout << "Fraction reduces to:" << endl;
	reduce();
}

/******************************************************************************
* @name add
* @brief Adds a rational number to the current fraction owned by class.
* @param n numerator of fraction to add.
* @param d denominator of fraction to add.
* @retval none
******************************************************************************/
void Rational::add(int n, int d)
{
	// Add fraction and reduce
	m_n = m_n * d + n * m_d;
	m_d = m_d * d;
	reduce();
}

/******************************************************************************
* @name sub
* @brief Subtracts a rational number from the current fraction owned by class.
* @param n numerator of fraction to subtract.
* @param d denominator of fraction to subtract.
* @retval none
******************************************************************************/
void Rational::sub(int n, int d)
{
	// Subtract fraction and reduce
	m_n = m_n * d - n * m_d;
	m_d = m_d * d;
	reduce();
}

/******************************************************************************
* @name mul
* @brief Multiplies a rational by the current fraction owned by class.
* @param n numerator of fraction to multiply.
* @param d denominator of fraction to multiply.
* @retval none
******************************************************************************/
void Rational::mul(int n, int d)
{
	// Multiply fraction and reduce
	m_n *= n;
	m_d *= d;
	reduce();
}

/******************************************************************************
* @name div
* @brief Divides the current fraction owned by class by a rational.
* @param n numerator of fraction to divide.
* @param d denominator of fraction to divide.
* @retval none
******************************************************************************/
void Rational::div(int n, int d)
{
	// Divide fraction and reduce
	m_n *= d;
	m_d *= n;
	reduce();
}

/******************************************************************************
* @name reduce
* @brief Reduces the class-owned fraction to simplist form and prints it.
* @param none
* @retval none
******************************************************************************/
void Rational::reduce()
{
	// Divide numerator and denominator by gcd
	int x = gcd(m_n, m_d);
	m_n /= x;
	m_d /= x;
	// Print fraction
	cout << m_n << "/" << m_d << endl;
}

/******************************************************************************
* @name gcd
* @brief Recursively applies the Euclidean algorithm to find the greatest \
*		 common denominator of two integers.
* @param a first input integer
* @param b first input integer
* @retval greatest common denominator of a and b
******************************************************************************/
int Rational::gcd(int a, int b)
{
	// Return value if equal
	if (a == b)
	{
		return a;
	}
	// Call with a less if a > b
	else if (a > b)
	{
		return gcd(a - b, b);
	}
	// Call with b less if b > a
	else
	{
		return gcd(a, b - a);
	}
}
