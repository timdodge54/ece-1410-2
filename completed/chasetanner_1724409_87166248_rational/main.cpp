#include "rational.h"

/******************************************************************************
* @name getFraction
* @brief Prompts and gets a fraction from the cli.
* @param n Fraction numerator reference
* @param d Fraction denominator reference
* @retval none
******************************************************************************/
void getFraction(int& n, int& d)
{
	// Get numerator
	cout << "Enter numerator: ";
	cin >> n;
	// Get denominator
	cout << "Enter denominator: ";
	cin >> d;
}

/******************************************************************************
* @name main
* @brief Creates a cli based UI for preforming arithmetic operations on \
*		 rational fractions.
* @param none
* @retval none
******************************************************************************/
int main(void)
{
	// Initialize variables
	int n, d;
	char option;

	// Get initial values and initialize class
	getFraction(n, d);
	Rational fraction(n, d);
	
	// Loop while option is not 0
	do
	{
		// Start error handeling
		try
		{
			// Print option menu
			cout << endl << endl << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a rational" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;

			// Get selected option
			cout << "Enter selection: ";
			cin >> option;

			// Evaluate entered option
			switch (option)
			{
				// Continue to program exit if 0
				case '0':
					break;
				// Add fraction if 1
				case '1':
					getFraction(n, d);
					fraction.add(n, d);
					break;
				// Subtract fraction if 2
				case '2':
					getFraction(n, d);
					fraction.sub(n, d);
					break;
				// Multiply fraction if 3
				case '3':
					getFraction(n, d);
					fraction.mul(n, d);
					break;
				// Divide fraction if 4
				case '4':
					getFraction(n, d);
					fraction.div(n, d);
					break;
				// Default throw error
				default:
					throw option;
			}
		}
		// Catch errors as characters
		catch (char e)
		{
			// Print error message
			cout << "Illegal option \'" << option << "\'!" << endl;
		}
	// End loop when option is 0
	} while (option != '0');

	// Return success code
	return 0;
}