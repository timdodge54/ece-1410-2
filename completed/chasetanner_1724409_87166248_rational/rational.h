#ifndef RATIONAL_H
#define RATIONAL_H

#include <iostream>

using namespace std;

class Rational {
public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
private:
	void reduce();
	int gcd(int a, int b);
	int m_n, m_d;
};

#endif