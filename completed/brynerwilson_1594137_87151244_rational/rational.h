#ifndef RATIONAL_HEADER
#define RATIONAL_HEADER

class Rational {
public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);

private:
	void reduce();
	int num, denom;
};

#endif
