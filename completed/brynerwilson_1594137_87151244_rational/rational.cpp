#include "rational.h"
#include <iostream>
using namespace std;

/*****************************************************************************
* @name 	Rational::Rational
* @brief	Rational class constructor
* @param	numerator and denominator
* @retval	none
*****************************************************************************/

Rational::Rational(int n, int d) {
	num = n;
	denom = d;
	cout << "Fraction reduces to ";
	Rational::reduce();
	return;
}

/*****************************************************************************
* @name 	Rational::reduce
* @brief	Reduces the fraction
* @param	none
* @retval	none
*****************************************************************************/

void Rational::reduce() {
	int a = num;
	int b = denom;
	//euclidean algorithm to find GCF
	while (a != b) {
		if (a > b) a -= b;
		else b -= a;
	}
	//divide numerator and denominator by GCF
	num /= a;
	denom /= a;
	//print result to screen
	cout << num << '/' << denom << endl << endl;
	return;
}

/*****************************************************************************
* @name 	Rational::add
* @brief	adds two fractions
* @param	numerator and denominator
* @retval	none
*****************************************************************************/

void Rational::add(int n, int d) {
	//multiply numerators by opposite denom and add
	num = num * d + n * denom;
	//multiply denominators
	denom *= d;
	//reduce!!!
	Rational::reduce();
	return;
}

/*****************************************************************************
* @name 	Rational::sub
* @brief	subtracts one fraction from another
* @param	numerator and denominator
* @retval	none
*****************************************************************************/

void Rational::sub(int n, int d) {
	//reuse add function (reduce located already in add)
	Rational::add(-n, d);
	return;
}

/*****************************************************************************
* @name 	Rational::mul
* @brief	multiplies two fractions
* @param	numerator and denominator
* @retval	none
*****************************************************************************/

void Rational::mul(int n, int d) {
	//multiply numerators by numerators and denominators by denominators
	num *= n;
	denom *= d;
	//reduce!!!
	Rational::reduce();
	return;
}

/*****************************************************************************
* @name 	Rational::div
* @brief	divides one fraction by another
* @param	numerator and denominator
* @retval	none
*****************************************************************************/

void Rational::div(int n, int d) {
	//Reuse multiply function, multiply by reciprocal
	Rational::mul(d, n);
	//reduces in multiply function
	return;
}