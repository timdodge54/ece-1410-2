#include "rational.h"
#include <iostream>
using namespace std;

/*****************************************************************************
* @name 	printmenu
* @brief	Prints a menu to the console and retrieves a menu selection
* @param	none
* @retval	menu selection
*****************************************************************************/

char printmenu()
{
	char s;
	//print that delicious menu
	cout << "1. Add a rational" << endl;
	cout << "2. Subtract a rational" << endl;
	cout << "3. Multiply by a rational" << endl;
	cout << "4. Divide by a rational" << endl;
	cout << "0. Exit" << endl;
	cout << "Enter selection: ";
	//await the user's will. Your brilliance in fractional numbers is only 
	//exceeded by your patience.
	cin >> s;
	return s;
}

/*****************************************************************************
* @name 	main
* @brief	menu-driven loop that performs rational number arithmetic
* @param	none
* @retval	none
*****************************************************************************/

int main(void)
{
	//get first rational from user
	int n, d;
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;
	Rational myRational(n, d);
	
	// while user does not enter 0, 
	//repeat menu -> operation loop
	char selection;
	while (1) {
		selection = printmenu();
		//try - catch to ensure valid menu selection
		try {
			switch (selection) {
			case '1': case '2': case '3': case '4': 
				//get a numerator and denominator from the user
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
			case '0': 
				break;
			default:
				throw 111;
			}
		}
		//if invalid selection, print error and exit program
		catch (int x) {
			cout << "Error " << x << endl;
			cout << "Invalid menu selection! Exiting..." << endl;
			exit(1);
		}

		//Perform operation based on menu selection
		switch (selection) {
		case '0':
			//if input is 0, exit program
			cout << endl << endl;
			cout << "Exiting..." << endl;
			exit(1);
			break;
			//each of these operations prints reduced result to screen
		case '1':
			myRational.add(n, d);
			break;
		case '2':
			myRational.sub(n, d);
			break;
		case '3':
			myRational.mul(n, d);
			break;
		case '4':
			myRational.div(n, d);
			break;
		
		}
		
	}

	

	return 0;
}