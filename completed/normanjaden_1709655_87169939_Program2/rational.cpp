#include "rational.h"

rational::rational(int n, int d) {
	num = n;
	den = d;
	simp();
}

/**
* @name add
* @breif add fractions to your object
* @param numerator and denominator
* @retval none
*/

void rational::add(int n, int d) {
	if (d == den) {
		num += n;
	}
	else {
		n = n * den;
		num = num * d;
		num += n;
		den *= d;
	}
	simp();
}

/**
* @name sub
* @breif subtracts fractions from your object
* @param numerator and denominator
* @retval none
*/

void rational::sub(int n, int d) {
	if (d == den) {
		num -= n;
	}
	else {
		n *= den;
		num *= d;
		den *= d;
		num -= n;
	}
	simp();
}

/**
* @name mul
* @breif multiplies fractions to your object
* @param numerator and denominator
* @retval none
*/

void rational::mul(int n, int d) {
	num *= n;
	den *= d;
	simp();
}

/**
* @name div
* @breif divides fractions to the object
* @param numerator and denominator
* @retval none
*/

void rational::div(int n, int d) {
	num *= d;
	den *= n;
	simp();
}

/**
* @name simp
* @breif simplifies the object
* @param none
* @retval none
*/

void rational::simp() {
	if (num == den) {
		num = 1;
		den = 1;
	}
	else if (num < den) {
		int count = num;

		while (count > 1) {
			if ((num % count) == 0 && (den % count) == 0) {
				num /= count;
				den /= count;
			}
			count--;
		}
	}
	else {
		int count = den;

		while (count > 1) {
			if ((num % count) == 0 && (den % count) == 0) {
				num /= count;
				den /= count;
			}
			count--;
		}
	}
}

/**
* @name getNum
* @breif returns the numerator
* @param none
* @retval numerator
*/

int rational::getNum()
{
	return num;
}

/**
* @name getDen
* @breif returns the denominator
* @param none
* @retval denominator
*/

int rational::getDen() {
	return den;
}