#include <string>
using namespace std;

class rational {
	int num;
	int den;
private:
	void simp();
public:
	rational (int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	int getNum();
	int getDen();
};