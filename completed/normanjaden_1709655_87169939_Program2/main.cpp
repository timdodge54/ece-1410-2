#include <iostream>
#include "rational.h"
using namespace std;

/**
* @name main
* @breif computes rational fractions
* @param none
* @retval none
*/

int main(void) {
	int num, den, func = 1;

	cout << "Enter Numerator: ";
	cin >> num;

	cout << "Enter denominator : ";
	cin >> den;

	rational fraction = rational(num, den);
	cout << "Fraction Reduces to " << fraction.getNum() << "/" << fraction.getDen() << endl;
	cout << endl;

	while (func != 0) {
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter Selection: ";
		try {
			cin >> func;
			if (func >= 0 && func <= 4) {
				throw func;
			}
			else {
				throw 'n';
			}
		}
		catch (char e) {
			cout << "Not a real output!" << endl;
			cout << endl;
		}
		catch (int func) {
			if (func == 0) {

			}
			else {
				cout << "Enter numerator: ";
				cin >> num;

				cout << "Enter denominator: ";
				cin >> den;


				switch (func) {

				case 1:
					fraction.add(num, den);
					break;
				case 2:
					fraction.sub(num, den);
					break;
				case 3:
					fraction.mul(num, den);
					break;
				case 4:
					fraction.div(num, den);
					break;
				}
			}
			cout << fraction.getNum() << "/" << fraction.getDen() << endl;
			cout << endl;
		}
	}
	return 0;
}