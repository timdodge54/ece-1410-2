/****************************************************************************** 
* @name main.cpp
* @brief Takes user-defined fraction and performs operations on stored
* fraction. Then reduces the fraction and prints it to the screen.
* @param none
* @retval  none 
******************************************************************************/ 

#include <iostream>
#include <istream>
#include <ostream>
#include <cmath>

#include "rational.h"

using namespace std;

int main()
{
	int n, d;
	n = 0;
	d = 0;
	char selection;
	selection = '1';
	
	// Determine first fraction
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;
	cout << "Your fraction: " << n << "/" << d << endl;
	
	// Call the constructor
	Rational rat = Rational(n,d);
	
	// Loop while usr selection does not equal 0
	while (selection != '0')
	{
		// Print selection options to screen
		cout << endl;
		cout << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		
		
		// Get selection from user
		cout << "Enter selection: " << endl;
		cin >> selection;
		
		// If selection = 0
		if (selection == '0')
		// Then
		{
			// Exit program
			return 0;
		}
		// Else if selection = 1
		else if (selection == '1')
		// Then
		{
			// Get new numerator and denominator from user
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// Call the addition function
			rat.add(n,d);
		}
		// Else if selection = 2
		else if (selection == '2')
		// Then
		{
			// Get new numerator and denominator from user
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// Call the subtraction function
			rat.sub(n,d);
		}
		// Else if selection = 3
		else if (selection == '3')
		// Then
		{
			// Get new numerator and denominator from user
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// Call the multiplication function
			rat.mul(n,d);
		}
		// Else if selection = 4
		else if (selection == '4')
		// Then
		{
			// Get new numerator and denominator from user
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// Call the division function
			rat.div(n,d);
		}
		// Else
		else 
		{
			try
			{
				throw selection;
			}
			catch (...)
			{
				cout << "Error! Please enter a value between 0 and 4!" << endl;
			}
			
		}
		
	}
	// EndLoop
	
	return 0;
}
