#include <iostream>
#include <istream>
#include <ostream>
#include <cmath>

#include "rational.h"

using namespace std;

/****************************************************************************** 
* @name Rational
* @brief Sets the user-entered values to the stored function values
* @param Values n and d
* @retval none
******************************************************************************/ 

Rational::Rational(int n, int d)
{
	// Set the user values to the stored fraction values
	numerator = n;
	denominator = d;
}

/****************************************************************************** 
* @name add
* @brief Adds the new fraction to the stored fraction
* @param Values n and d
* @retval none
******************************************************************************/ 

void Rational::add (int n, int d)
{
	// Calculate the numerator
	numerator = (numerator*d + n*denominator);
	// Calulate the denominator
	denominator = (denominator*d);
	// Call the reduce fraction function
	reduce();
	// Call the print function
	print();
}

/****************************************************************************** 
* @name sub
* @brief Subtracts the new fraction from the stored fraction
* @param Values n and d
* @retval none
******************************************************************************/ 

void Rational::sub (int n, int d)
{
	// Calculate the numerator
	numerator = (numerator*d - n*denominator);
	// Calulate the denominator
	denominator = (denominator*d);
	// Call the reduce fraction function
	reduce();
	// Call the print function
	print();
}

/****************************************************************************** 
* @name mul
* @brief Multiplies the new fraction by the stored fraction
* @param Values n and d
* @retval none
******************************************************************************/ 

void Rational::mul (int n, int d)
{
	// Calculate the numerator
	numerator = (numerator*n);
	// Calulate the denominator
	denominator = (denominator*d);
	// Call the reduce fraction function
	reduce();
	// Call the print function
	print();
}

/****************************************************************************** 
* @name div
* @brief Divides the stored fraction by the new fraction
* @param Values n and d
* @retval none
******************************************************************************/ 

void Rational::div (int n, int d)
{
	// Calculate the numerator
	numerator = (numerator*d);
	// Calulate the denominator
	denominator = (denominator*n);
	// Call the reduce fraction function
	reduce();
	// Call the print function
	print();
}

/****************************************************************************** 
* @name print
* @brief Prints the value of the stored and reduced fraction
* @param none
* @retval none
******************************************************************************/ 

void Rational::print ()
{
	// Print the reduced fraction to the screen
	cout << "Reduced fraction: " << numerator << "/" << denominator << endl;
}

/****************************************************************************** 
* @name reduce
* @brief Reduces the stored fraction via a Greatest Common Factor algorithm
* @param none
* @retval none
******************************************************************************/ 

void Rational::reduce()
{
	int gcf, work_n, work_d;
	gcf = 0;
	work_n = numerator;
	work_d = denominator;
	
	// Loop while the working numerator and denominator are not equal
	while (work_n != work_d)
	{
		// If the working numerator is greater than the working denominator
		if (work_n > work_d)
		// Then
		{
			// Subtract the working denominator from the working numerator
			work_n = work_n - work_d;
		}
		// Else
		else
		{
			// Subtract the working numerator from the working denominator
			work_d = work_d - work_n;
		}
	}
	
	// Set the greatest common factor equal to the working numerator,
	// which will now be the same as the working denominator
	gcf = work_n;
	
	// Divide both the numerator and denominator by
	// their greatest common factor
	numerator = numerator/gcf;
	denominator = denominator/gcf;
}
