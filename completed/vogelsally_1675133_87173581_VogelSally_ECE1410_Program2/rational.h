#ifndef RATIONAL_H
#define RATIONAL_H

class Rational
{
	public:
		Rational(int n,int d);
		void add(int n,int d);
		void sub(int n,int d);
		void mul(int n,int d);
		void div(int n,int d);
		void print();
	private:
	int numerator, denominator;
	void reduce();
};

#endif