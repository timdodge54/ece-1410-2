#ifndef RATIONAL_H
#define RATIONAL_H

#include <iostream>
using namespace std;

class Rational
{
private:
    int numerator;
    int denominator;
    void reduce();

public:
    Rational(int n, int d);
    void add(int n, int d);
    void sub(int n, int d);
    void mul(int n, int d);
    void div(int n, int d);
    void print();
};

#endif
