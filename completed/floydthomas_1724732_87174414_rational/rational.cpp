#include "rational.h"
#include <algorithm>
using namespace std;

/*****************************************************************************
 * @name Rational
 * @brief Takes an input fraction and reduces it.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

Rational::Rational(int n, int d)
{
    numerator = n;
    denominator = d;
    reduce();
}

/*****************************************************************************
 * @name main
 * @brief Adds two fractions.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

void Rational::add(int n, int d)
{
    numerator = numerator * d + denominator * n;
    denominator *= d;
    reduce();
}

/*****************************************************************************
 * @name main
 * @brief Subtracts two fractions.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

void Rational::sub(int n, int d)
{
    numerator = numerator * d - denominator * n;
    denominator *= d;
    reduce();
}

/*****************************************************************************
 * @name main
 * @brief Multiplies two fractions.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

void Rational::mul(int n, int d)
{
    numerator *= n;
    denominator *= d;
    reduce();
}

/*****************************************************************************
 * @name main
 * @brief Divides two fractions.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

void Rational::div(int n, int d)
{
    numerator *= d;
    denominator *= n;
    reduce();
}

/*****************************************************************************
 * @name main
 * @brief Reduces a given fraction.
 * @param none
 * @retval  none
*****************************************************************************/

void Rational::reduce()
{
    int divisor = min(numerator, denominator);
    while (divisor > 1) {
        if (numerator % divisor == 0 && denominator % divisor == 0) {
            numerator /= divisor;
            denominator /= divisor;
            break;
        }
        divisor--;
    }
}

/*****************************************************************************
 * @name main
 * @brief Prints a fraction.
 * @param int n, int d
 * @retval  none
*****************************************************************************/

void Rational::print()
{
    cout << numerator << "/" << denominator;
}