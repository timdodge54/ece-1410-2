#include <iostream>
#include "rational.h"

/*****************************************************************************
 * @name main
 * @brief Performs arithmetic operations on fractions.
 * @param none
 * @retval  none
*****************************************************************************/
int main()
{
    int n, d;
    cout << "Enter the numerator: ";
    cin >> n;
    cout << "Enter the denominator: ";
    cin >> d;

    // Create a rational object with the entered numerator and denominator
    Rational rational(n, d);

    // Display the reduced fraction
    cout << "Fraction reduces to ";
    rational.print();
    cout << endl << endl << endl;

    char choice;
    // Loop until the user decides to quit
    do {
        // Display the menu options
        cout << "1. Add a fraction" << endl;
        cout << "2. Subtract a fraction" << endl;
        cout << "3. Multiply by a fraction" << endl;
        cout << "4. Divide by a fraction" << endl;
        cout << "0. Quit" << endl;
        cout << "Enter selection: ";
        cin >> choice;

        // Perform the chosen operation
        switch (choice) {
        case '1':
            cout << "Enter numerator: ";
            cin >> n;
            cout << "Enter denominator: ";
            cin >> d;
            rational.add(n, d);
            rational.print();
            cout << endl << endl << endl;
            break;
        case '2':
            cout << "Enter numerator: ";
            cin >> n;
            cout << "Enter denominator: ";
            cin >> d;
            rational.sub(n, d);
            rational.print();
            cout << endl << endl << endl;
            break;
        case '3':
            cout << "Enter numerator: ";
            cin >> n;
            cout << "Enter denominator: ";
            cin >> d;
            rational.mul(n, d);
            rational.print();
            cout << endl << endl << endl;
            break;
        case '4':
            cout << "Enter numerator: ";
            cin >> n;
            cout << "Enter denominator: ";
            cin >> d;
            rational.div(n, d);
            rational.print();
            cout << endl << endl << endl;
            break;
        case '0':
            break;
        default:
            cout << "Invalid choice, please try again." << endl << endl
                << endl;
            break;
        }
    } while (choice != '0');
    return 0;
}
