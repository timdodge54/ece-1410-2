#include <iostream>
#include <fstream>
#include "rational.h"
using namespace std;

/****
* @name		add
* @brief	adds a fraction to the current fraction
* @param	numerator and denominator
* @retval	none
****/
void Rational::add(int n, int d) {
	int num, den;
	int a, b;

		cout << "Enter numerator: ";
		cin >> num;
		cout << "Enter denominator: ";
		cin >> den;

	n = (n * den) + (num * d);
	d *= den;
	
	//reduces fraction
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a -= b;
		}
		else
		{
			b -= a;
		}
	}
	n /= a;
	d /= a;

	cout << n << "/" << d << "\n" << endl;

	cur_num = n;
	cur_den = d;
}

/****
* @name		sub
* @brief	subtracts a fraction from the current fraction
* @param	numerator and denominator
* @retval	none
****/
void Rational::sub(int n, int d) {
	int num, den;
	int a, b;

	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;

	n = (n * den) - (num * d);
	d *= den;

	//reduces fraction
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a -= b;
		}
		else
		{
			b -= a;
		}
	}
	n /= a;
	d /= a;

	cout << n << "/" << d << "\n" << endl;

	cur_num = n;
	cur_den = d;
}

/****
* @name		mul
* @brief	multiplies a fraction to the current fraction
* @param	numerator and denominator
* @retval	none
****/
void Rational::mul(int n, int d) {
	int num, den;
	int a, b;

	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;

	n *= num;
	d *= den;

	//reduces fraction
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a -= b;
		}
		else
		{
			b -= a;
		}
	}
	n /= a;
	d /= a;

	cout << n << "/" << d << "\n" << endl;

	cur_num = n;
	cur_den = d;
}

/****
* @name		div
* @brief	divides a fraction from the current fraction
* @param	numerator and denominator
* @retval	none
****/
void Rational::div(int n, int d) {
	int num, den;
	int a, b;

	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;

	n *= den;
	d *= num;

	//reduces fraction
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a -= b;
		}
		else
		{
			b -= a;
		}
	}
	n /= a;
	d /= a;

	cout << n << "/" << d << "\n" << endl;

	cur_num = n;
	cur_den = d;
}