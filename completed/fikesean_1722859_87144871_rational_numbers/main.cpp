#include <iostream>
#include <fstream>
#include <typeinfo>
#include "rational.h"
using namespace std;

/****
* @name		main
* @brief	performs arithmetic on fractions
* @param	none
* @retval	none
****/
int main()
{
	int n, d;
	int a, b;
	int select;

	Rational r;

	try
	{
		cout << "Enter numerator: ";
		cin >> n;
		cout << "Enter denominator: ";
		cin >> d;
		if (typeid(n) != typeid(int) || typeid(d) != typeid(int))
		{
			throw 1;
		}
	}
	catch (int e)
	{
		if (e == 1)
		{
			cout << "Illegal input." << endl;
		}
		return 0;
	}

	//reduces fraction
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a -= b;
		}
		else
		{
			b -= a;
		}
	}
	n /= a;
	d /= a;

	cout << "Fraction reduces to " << n << "/" << d << "\n\n\n";

	

	while (1)
	{
		cout << "1. Add a rational\n2. Subtract a rational\n"
			<< "3. Multiply by a rational\n4. Divide by a rational\n"
			<< "0. Exit" << endl;
		try
		{
			cout << "Enter selection: ";
			cin >> select;
			if (select > 4 || select < 0)
			{
				throw 1;
			}
		}
		catch (int e)
		{
			if (e == 1)
			{
				cout << "illegal input." << endl;
			}
			return 0;
		}
		
		if (select == 0)
		{
			return 0;
		}
		else if (select == 1)
		{
			r.add(n, d);
			n = r.cur_num;
			d = r.cur_den;
		}
		else if (select == 2)
		{
			r.sub(n, d);
			n = r.cur_num;
			d = r.cur_den;
		}
		else if (select == 3)
		{
			r.mul(n, d);
			n = r.cur_num;
			d = r.cur_den;
		}
		else if (select == 4)
		{
			r.div(n, d);
			n = r.cur_num;
			d = r.cur_den;
		}
	}
}