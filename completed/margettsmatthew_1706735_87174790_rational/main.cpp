#include <iostream>
#include "rational.h"
using namespace std;
/********************************************************************* 
* @name main 
* @brief Iterates a menu to add, subtract, multiply, or divide a fraction by another fraction
* @param none 
* @retval  none 
*********************************************************************/
int main(){
	int num, denom;
	char menu = 1;//create variables
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";//Ask for initial values
	cin >> denom;
	Rational frac (num , denom);//Construct Rational object 'frac'
	cout << "Fraction reduces to: " << frac.getNum() << "/" << frac.getDenom();
	
	while (menu != 0){//Menu iteration until quit
		cout << endl << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		try{//try/catch to detect improper selection
			cin >> menu;
			if (menu == '0'){//Exit if 0 is entered
				return 0;
			}
			else if (menu == '1'){//add fraction if 1
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> denom;
				frac.add(num,denom);
				cout << frac.getNum() << "/" << frac.getDenom();
			}
			else if (menu == '2'){//subtract fraction if 2
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> denom;
				frac.sub(num,denom);
				cout << frac.getNum() << "/" << frac.getDenom();
			}
			else if (menu == '3'){//multiply fraction if 3
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> denom;
				frac.mul(num,denom);
				cout << frac.getNum() << "/" << frac.getDenom();
			}
			else if (menu == '4'){//divide fraction if 4
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> denom;
				frac.div(num,denom);
				cout << frac.getNum() << "/" << frac.getDenom();
			}
			else{//otherwise, throw error
				throw menu;
			}
		}
		catch (...){//catch error
			cout << "Error: " << menu << " Is not a legal option";
		}
	}
	
}