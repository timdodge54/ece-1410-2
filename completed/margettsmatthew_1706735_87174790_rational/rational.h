/********************************************************************* 
* @name Rational
* @brief Class that stores a rational number, along with useful functions
* @param none
* @retval  none 
*********************************************************************/
class Rational{
	private:
		int num, denom;
		void reduce();
	public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	int getNum () {return num;}
	int getDenom () {return denom;}
};