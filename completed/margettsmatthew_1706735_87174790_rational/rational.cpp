#include "rational.h"
/********************************************************************* 
* @name Rational
* @brief Constructor for Rational class, defines num and denom values
* @param int n, int d
* @retval  none 
*********************************************************************/
Rational::Rational(int n, int d){
	num = n;
	denom = d;
	reduce();
}
/********************************************************************* 
* @name add
* @brief Adds two fractions together
* @param int n, int d
* @retval  none 
*********************************************************************/
void Rational::add(int n, int d){
	num = (num * d) + (n * denom);
	denom = denom * d;
	reduce();
}
/********************************************************************* 
* @name sub
* @brief Subtracts two fractions
* @param int n, int d
* @retval  none 
*********************************************************************/
void Rational::sub(int n, int d){
	num = (num * d) - (n * denom);
	denom = denom * d;
	reduce();
}
/********************************************************************* 
* @name mul
* @brief multiplies two fractions
* @param int n, int d
* @retval  none 
*********************************************************************/
void Rational::mul(int n, int d){
	num = num * n;
	denom = denom * d;
	reduce();
}
/********************************************************************* 
* @name div
* @brief Divides two fractions
* @param int n, int d
* @retval  none 
*********************************************************************/
void Rational::div(int n, int d){
	mul( d, n);
	reduce();
}
/********************************************************************* 
* @name reduce
* @brief reduces the stored fraction to its smallest numerator/denominator
* @param none
* @retval  none 
*********************************************************************/
void Rational::reduce(){
	int flag = 0;
	int n = num;
	int d = denom;//Creates values to temporarily hold num and denom
	while(!flag){//Loop until d equals n
		if(n > d){
			n -= d;
		}
		else if(d > n){//Subtract the larger by the smaller
			d -= n;
		}
		else if (d == n){
			flag = 1;
		}
	}
	num /= n;//divide num and denom by the LCD
	denom /= d;
}