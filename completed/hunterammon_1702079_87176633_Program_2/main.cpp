#include <iostream>
#include <fstream>
#include "rational.h"
using namespace std;

/*********************************************************************
* @name main
* @brief controls use of manipulating rational numbers
* @param none
* @retval  none
*********************************************************************/

int main(void) {
	bool run = true;//all this stuff is self explanitory (Uncle Bob)
	bool valid_selection = true;
	int a, b;
	cout << "Enter a numerator: ";
	cin >> a;
	cout << "Enter a denominator: ";
	cin >> b;
	
	Rational fract(a, b);

	cout << "Fraction reduces to ";
	fract.print();
	
	while (run) {
		
		valid_selection = true;
		char selection;
		cout << "\n\n1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> selection;
		try {
			throw selection;
		}
		catch (char e) {
			if (e < 48 || e > 52) {
				cout << "invalid selection, must be a number 0-4" << endl;
				valid_selection = false;
			}
		}
		if (valid_selection == true) {
			if (selection == 48) {
				exit(1);
			}

			cout << "Enter a numerator: ";
			
				cin >> a;
			
			cout << "Enter a denominator: ";
			
				cin >> b;
			
			
			
			if (selection == 49) {
				fract.add(a, b);
			}
			if (selection == 50) {
				fract.subtract(a, b);
			}
			if (selection == 51) {
				fract.multiply(a, b);
			}
			if (selection == 52) {
				fract.divide(a, b);
			}
			fract.print();
		}
	}




	return 0;
}


