#include "rational.h"
#include <iostream>
#include <fstream>
using namespace std;

Rational::Rational(int a, int b) {
	numerator = a;
	denominator = b;
	reduce();
};

/*********************************************************************
* @name add
* @brief adds input fraction to existing fraction
* @param numerator denominator
* @retval  none
*********************************************************************/

void Rational::add(int a, int b) {
	numerator = denominator * a + b * numerator;
	denominator *= b;
	reduce();
}

/*********************************************************************
* @name subtract
* @brief subtracts input fraction to existing fraction
* @param numerator denominator
* @retval  none
*********************************************************************/

void Rational::subtract(int a, int b) {
	numerator = b * numerator-denominator * a;
	denominator *= b;
	reduce();
}

/*********************************************************************
* @name multiply
* @brief multiplys input fraction to existing fraction
* @param numerator denominator
* @retval  none
*********************************************************************/

void Rational::multiply(int a, int b) {
	numerator *= a;
	denominator *= b;
	reduce();
}

/*********************************************************************
* @name divide
* @brief divides input fraction to existing fraction
* @param numerator denominator
* @retval  none
*********************************************************************/

void Rational::divide(int a, int b) {
	numerator *= b;
	denominator *= a;
	reduce();
}

/*********************************************************************
* @name print
* @brief prints existing fraction
* @param none
* @retval  none
*********************************************************************/

void Rational::print() {
	cout << numerator << "/" << denominator << endl;
}

/*********************************************************************
* @name reduce
* @brief reduces existing fraction
* @param none
* @retval  none
*********************************************************************/

void Rational::reduce() {
	int a = numerator;
	int b = denominator;
	while (a != b) {//uses a method to extract GCF
		if (a > b) {
			a -= b;
		}
		else if (b>a) {
			b -= a;
		}
	}
	numerator /= a;
	denominator /= b;
}
