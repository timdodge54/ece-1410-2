class Rational {
	public:
		Rational(int a, int b);
		void add(int a, int b);
		void subtract(int a, int b);
		void multiply(int a, int b);
		void divide(int a, int b);
		void print();
	private:
		void reduce();
		int numerator, denominator;

};