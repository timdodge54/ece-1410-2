#ifndef	RATIONAL_H
#define RATIONAL_H
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
using namespace std;

class Rational {
private:
	int numerator, denominator;
public:
	Rational(int, int);
	void reduce();
	int gcd(int n, int d);
	void add();
	void sub();
	void mul();
	void div();
};

#endif