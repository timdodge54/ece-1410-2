/******************************************************************************
* @name 	main
* @brief	program that performs arithmetic on fractions. the results of all
*			arithmetic operations overwrite the previous values. fractions
*			should always be in reduced form.
* @param	none
* @retval	none
******************************************************************************/
#include "rational.h"

int main() {
	int in, n, d;
	in = 5;
	// create a class called Rational
	// ask for numerator and denominator
	cout << "Enter Numerator: ";
	cin >> n;
	cout << "Enter Denominator: ";
	cin >> d;
	Rational rat(n, d);
	rat.reduce();
	// print reduced fraction
	// start loop - while (!in == 0)?
	// loop until user selects 0
	while (in != 0) {
		// print selection menu
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply a rational" << endl;
		cout << "4. Divide a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> in;
		// respond to selected output
		// Addition - input is 1
		if (in == 1) {
			rat.add();
		}

		// Subtraction - input is 2
		else if (in == 2) {
			rat.sub();
		}

		// Multiplication - input is 3
		else if (in == 3) {
			rat.mul();
		}

		// Division - input is 4
		else if (in == 4) {
			rat.div();
		}

		else if (in == 0) {
			return 0;
		}

		else {
			cout << "Invalid Selection: Program Exit!" << endl;
			return 0;
		}
	}

	return 0;
}
