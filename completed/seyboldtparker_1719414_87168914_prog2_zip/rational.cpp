#include "rational.h"
/******************************************************************************
* @name 	rational
* @brief	function that assigns n to numerator and d to denominator
* @param	int n int d
* @retval	none
******************************************************************************/
Rational::Rational(int n, int d) {
	numerator = n;
	denominator = d;
}



/******************************************************************************
* @name 	reduce
* @brief	function that reduces new fraction
* @param	none
* @retval	none
******************************************************************************/
void Rational::reduce() {
	int div = gcd(numerator, denominator);
	numerator /= div;
	denominator /= div;
	cout << "New Fraction: " << numerator << "/" << denominator << endl << endl;
}


/******************************************************************************
* @name 	gcd
* @brief	function that reduces new fraction
* @param	int n, int d
* @retval	int
******************************************************************************/
int Rational::gcd(int n, int d) {
	if (d == 0) {
		return n;
	}
	return gcd(d, n % d);
}


/******************************************************************************
* @name 	add
* @brief	function that adds the old fraction by new fraction
* @param	none
* @retval	none
******************************************************************************/
void Rational::add() {
	int n, d;
	cout << "Enter Numerator: ";
	cin >> n;
	cout << "Enter Denominator: ";
	cin >> d;
	numerator = ((n * denominator) + (numerator * d));
	denominator = (d * denominator);
	reduce();
}

/******************************************************************************
* @name 	sub
* @brief	function that subtracts the old fraction by new fraction
* @param	none
* @retval	none
******************************************************************************/
void Rational::sub() {
	int n, d;
	cout << "Enter Numerator: ";
	cin >> n;
	cout << "Enter Denominator: ";
	cin >> d;
	numerator = ((numerator * d) - (n * denominator));
	denominator = (d * denominator);
	reduce();
}

/******************************************************************************
* @name 	mul
* @brief	function that multiplies the old fraction by new fraction
* @param	none
* @retval	none
******************************************************************************/
void Rational::mul() {
	int n, d;
	cout << "Enter Numerator: ";
	cin >> n;
	cout << "Enter Denominator: ";
	cin >> d;
	numerator = (n * numerator);
	denominator = (d * denominator);
	reduce();
}

/******************************************************************************
* @name 	div
* @brief	function that divides the old fraction by new fraction
* @param	none
* @retval	none
******************************************************************************/
void Rational::div() {
	int n, d;
	cout << "Enter Numerator: ";
	cin >> n;
	cout << "Enter Denominator: ";
	cin >> d;
	numerator = (n * denominator);
	denominator = (d * numerator);
	reduce();
}