#include "myclasses.h"

/**
@name	reduce
@brief	finds the gcf and reduces the fraction 
@param
@retval	
*/
using namespace std;
void Rational::reduce()
{
	int g, d=0, a = numerator, b = denominator;
	//while loop till one is odd
	while (a % 2 == 0 && b % 2 == 0)
	{
		// take both of them and div 2
		a = a / 2;
		b = b / 2;
		d = d + 1;
	}
	while (a != b)
	{
		// do some stuff from the slide
		if (a % 2 == 0)
		{
			a = a / 2;
		}
		else
		{
			if (b % 2 == 0)
			{
				b = b / 2;
			}
			else
				if (a > b)
				{
					a = (a - b) / 2;
				}
				else
				{
					b = (b - a) / 2;
				}
		}
	}
	// make g=a
	g = a;
	if (d == 0)
	{
		// a quick thing if d is 0
		numerator = numerator / g;
		denominator = denominator / g;
	}
	else
	{
		// the loop to unwind those twos
		while (d != 0)
		{
			g = g * 2;
			d--;
		}
		numerator = numerator / g;
		denominator = denominator / g;
	}

}

/**
@name	add
@brief	takes two fractions and combines them with addition
@param
@retval
*/
void Rational::add(int n, int d)
{
	int x1, x2;
	// multiply the numerator of the current fraction to the denominator of
	// the new fraction

	x1 = numerator * d;


	// multiply the numerator of the new fraction to the denominator of
	// the current fraction

	x2 = denominator * n;


	// add these two results together and set as the numerator

	numerator = x1 + x2;


	// multiply the denominators together and set the result to the denom.

	denominator = denominator * d;
	Rational::reduce();
}

/**
@name	sub
@brief	subtracts two fractions
@param
@retval
*/
void Rational::sub(int n, int d)
{
	int first, second;
	// multiply the numerator of the current fraction to the denominator of
	// the new fraction

	first = numerator * d;


	// multiply the numerator of the new fraction to the denominator of
	// the current fraction

	second = denominator * n;


	// subtract these two results and set as the numerator

	numerator = first - second;


	// multiply the denominators together and set the result to the denom. 

	denominator = denominator * d;
	Rational::reduce();
}

/**
@name	mul
@brief	multiplies two fractions
@param
@retval
*/
void Rational::mul(int n, int d)
{
	// mult the two numerators

	numerator = numerator * n;


	// mult the two denominators

	denominator = denominator * d;
	Rational::reduce();
}

/**
@name	div
@brief	flips the second fraction and multiplies them
@param
@retval
*/
void Rational::div(int n, int d)
{
	// mult the num of the current and the denom of the new to get the num

	numerator = numerator * d;


	// mult the num of the new and the denom of the current to get the denom

	denominator = denominator * n;
	Rational::reduce();
}

/**
@name	printdastuff
@brief	prints da stuff(prints the fraction to the screen)
@param
@retval
*/
void Rational::printdastuff()
{
	// call reduce and print the resulting fraction
	Rational::reduce();
	cout << numerator << "/" << denominator;
}
