/**
@name	main
@brief	does fraction math for you and reduces the answer plus user friendly:)
@param
@retval integer
*/

#include "myclasses.h"
#include<iostream>
using namespace std;


int main(void)
{
	int nplaceholder, dplaceholder, flag;
	char v1;
	
	// do the first part of the program ask for fract and simplify

	cout << "Enter numerator:";
	cin >> nplaceholder;
	cout << "Enter denominator: ";
	cin >> dplaceholder;
	Rational fract(nplaceholder, dplaceholder);
	cout << "fraction reduces to:";
	fract.printdastuff();

	// make a loop that never ends(will the program crash idk)
	while (1==1)
	{
		flag = 1;
		//print the options avaliable 
		cout << endl << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational";
		cout << endl << "4. Divide by a rational" << endl << "0. Exit";
		cout << endl;
		cout << "enter selection: ";
		// ask for users answer

		cin >> v1;

		// do a throw catch
		try
		{
			if (v1 >= 5 || v1 < 0)
			{
				throw 20;
			}
		}
		catch (int e)
		{
			cout << "'" << v1 << "' is not a legal option!"<<endl<<endl;
			flag = 0;
		}
		// if 0. return 0 and exit the program
		if (v1 == 0)
		{
			return 0;
		}

		// ask for and cin the num and denom of the new fract
		if (flag == 1)
		{
			cout << "Enter numerator: ";
			cin >> nplaceholder;
			cout << "Enter denominator: ";
			cin >> dplaceholder;
		}
		//if 1. use the Add funct
		if (v1 == 1)
		{
			fract.add(nplaceholder, dplaceholder);
		}
		// if 2. use the subtract funct
		if (v1 == 2)
		{
			fract.sub(nplaceholder, dplaceholder);
		}
		// if 3. use the mult funct
		if (v1 == 3)
		{
			fract.mul(nplaceholder, dplaceholder);
		}
		// if 4 use the divide funct
		if (v1 == 4)
		{
			fract.div(nplaceholder, dplaceholder);
		}
		// place the new fraction into the class and print it to the screen
		Rational(nplaceholder, dplaceholder);
		if (flag == 1)
		{
			fract.printdastuff();
			cout << endl << endl;
		}
	}

}