#ifndef MYCLASSES_H
#define MYCLASSES_H
#include <iostream>
#include <cmath>
class Rational {
private:
	void reduce();
	int numerator, denominator;
public:
	Rational(int name1, int name2)
	{
		numerator = name1;
		denominator = name2;
	}
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void printdastuff();
};
#endif