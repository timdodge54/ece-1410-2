#include "Rational.h"
/****************************************************************************
* @name     Rational
* @brief    prints output
* @param	none
* @retval   none
*****************************************************************************/
Rational::Rational(int n,int d)
{
    numerator=n;
    denominator=d;
    reduce();
	cout<<"Fraction reduces to "<<numerator<<"/" <<denominator<<endl<<endl<<endl;;
}
/****************************************************************************
* @name     add
* @brief    preforms addition on fractions
* @param	none
* @retval   none
*****************************************************************************/
void Rational:: add(int n,int d)
{
    numerator=numerator*d+denominator*n;
    denominator=denominator*d;	
    reduce();
	cout<<numerator<<"/"<<denominator<<endl<<endl<<endl;
}
/****************************************************************************
* @name     sup
* @brief    preforms subtraction on fractions
* @param	none
* @retval   none
*****************************************************************************/
void Rational::sub(int n,int d)
{
    numerator=numerator*d-denominator*n;
    denominator=n*d;
    reduce();
	cout<<numerator<<"/"<<denominator<<endl<<endl<<endl;
}
/****************************************************************************
* @name     mul
* @brief    preforms multiplication on fractions
* @param	none
* @retval   none
*****************************************************************************/
void Rational::mul(int n,int d)
{
    numerator=numerator*n;
    denominator=denominator*d;
    reduce();
	cout<<numerator<<"/"<<denominator<<endl<<endl<<endl;
}
/****************************************************************************
* @name     div
* @brief    preforms division on fractions
* @param	none
* @retval   none
*****************************************************************************/
void Rational::div(int n,int d)
{
    numerator=numerator*d;
    denominator=denominator*n;
    reduce();
	cout<<numerator<<"/"<<denominator<<endl<<endl<<endl;
	
}
/****************************************************************************
* @name     reduce
* @brief    preforms reduction on fractions
* @param	none
* @retval   none
*****************************************************************************/
void Rational::reduce()
{
    int largest = numerator>denominator?numerator:denominator;
    int gcd = 0; // greatest common divisor

    for( int loop = 2; loop <= largest/2; ++loop )
    {  
        if ( numerator% loop == 0 && denominator % loop == 0 )
        gcd = loop;
    }
    if (gcd != 0)  {
      numerator /= gcd;
      denominator/= gcd;
    } 
}

