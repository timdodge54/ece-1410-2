#include "rational.h"

/****************************************************************************
* @name     main
* @brief    user interface and selection for rational class
* @param	none
* @retval   none
*****************************************************************************/
int main(){
	//declare necessary variables and ask for user input
	int numerator, denominator;
	cout<<"Enter numerator: ";
	cin>>numerator;
	cout<<"Enter denominator: ";
	cin>>denominator;
	
	
	Rational R(numerator,denominator);
	
	
    int pick=1;
	//loop until user selects 0
	while(pick!=0){
	try{
		cout<<"1. Add a rational"<<endl;
    cout<<"2. Subtract a rational"<<endl;
    cout<<"3. Multiply by a rational"<<endl;
    cout<<"4. Divide by a rational"<<endl;
    cout<<"0. Exit"<<endl;
		cout<<"Enter a selection: ";
		cin>>pick;
		//test cases to determine mathematical operation
		switch(pick)
            {
                case 1:
                cout<<"Enter numerator: ";
                cin>>numerator;
                cout<<"Enter denominator: ";
                cin>>denominator;
                R.add(numerator,denominator);
                break;
                case 2:
                cout<<"Enter numerator: ";
                cin>>numerator;
                cout<<"Enter denominator: ";
                cin>>denominator;
                R.sub(numerator,denominator);
                break;
                case 3:
                cout<<"Enter numerator: ";
                cin>>numerator;
                cout<<"Enter denominator: ";
                cin>>denominator;
                R.mul(numerator,denominator);
                break;
                case 4:
                cout<<"Enter numerator: ";
                cin>>numerator;
                cout<<"Enter denominator: ";
                cin>>denominator;
                R.div(numerator,denominator);
                break;
                case 0:
                return 0;
                default:
                throw pick;
            }
          
        }catch(int c)
        {
            cout<<c<<" is not a valid option. "<<"Please Enter valid option"<<endl;
        }
    }

    return 0;
}
	
	
