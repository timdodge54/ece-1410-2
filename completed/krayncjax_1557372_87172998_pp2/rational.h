#ifndef rational_h
#define rational_h
#include <iostream>
#include <cmath>

using namespace std;

class Rational{
	private:
		int numerator;
		int denominator;
	public:
		Rational(int n, int d);
		void add(int n,int d);
        void sub(int n,int d);
        void mul(int n,int d);
        void div(int n,int d);
        void reduce();
		

};
#endif