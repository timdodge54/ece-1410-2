#include "rational.h"


/*****************************************************************************
* @name		main
* @brief	asks user for an initial numerator and denominator, then presents
*			a menu to the user that presents various arithmetic operations to
*			be performed on the fraction. exits when option 0 is selected
* @param	none
* @retval	none
*****************************************************************************/

using namespace std;

int main(void)
{
	char select;
	int inden, innum, valid, e;

//prompt user for a numerator
	cout << "Enter a numerator: ";
//get numerator
	cin >> innum;
//prompt user for a denominator
	cout << "Enter a denominator: ";
//get denominator
	cin >> inden;
//provide numerator and denominator to rational
	Rational frac(innum, inden);
	cout << innum << "/" << inden << " reduces to ";
	frac.print();
	
//do loop
	do
	{
		valid = 1;
		e = 0;
		//prompt user with selection screen
		cout << "Please make a selection: " << endl;
		cout << "0 : Exit" << endl << "1 : Add" << endl;
		cout << "2 : Subtract" << endl << "3 : Multiply" << endl;
		cout << "4 : Divide" << endl << "Selection: ";
		//get char
		cin >> select;
		//try block to catch exceptions
		try
		{
			if (select > 52 || select < 48 )
			{
				throw 1;
			}
		}
		catch (int e)
		{
			if (e == 1)
			{
				cout << "'" << select << "' is not a valid option. Please" ;
				cout << " select an option within" << endl;
				cout << "the specified range." << endl << endl;
				valid = 0;
			}
		}
		
		//if valid selection, switch depending on selection
		if (valid == 1)
		{
			//switch
			switch (select)
			{
			//case 49 (char 1): add
			case 49:
				cout << "Enter numerator: ";
				cin >> innum;
				cout << "Enter denominator: ";
				cin >> inden;
				frac.add(innum, inden);
				cout << "Result is: ";
				frac.print();
				break;

			//case 50 (char 2): subtract
			case 50:
				cout << "Enter numerator: ";
				cin >> innum;
				cout << "Enter denominator: ";
				cin >> inden;
				frac.subtract(innum, inden);
				cout << "Result is: ";
				frac.print();
				break;

			//case 51 (char 3): multiply
			case 51:
				cout << "Enter numerator: ";
				cin >> innum;
				cout << "Enter denominator: ";
				cin >> inden;
				frac.multiply(innum, inden);
				cout << "Result is: ";
				frac.print();
				break;

			//case 52 (char 4): divide
			case 52:
				cout << "Enter numerator: ";
				cin >> innum;
				cout << "Enter denominator: ";
				cin >> inden;
				frac.divide(innum, inden);
				cout << "Result is: ";
				frac.print();
				break;

			default:
				return 0;
				break;
			}

		}
		//endloop when char == 0
	} while (select != 0);

	return 0;
//end
}