#ifndef RATIONAL_H
#define RATIONAL_h
#include <iostream>

class Rational {
public:
	Rational(int n, int d);
	void add(int n, int d);
	void subtract(int n, int d);
	void multiply(int n, int d);
	void divide(int n, int d);
	void print();

private:
	void reduce();
	int numerator, denominator;

};
#endif

