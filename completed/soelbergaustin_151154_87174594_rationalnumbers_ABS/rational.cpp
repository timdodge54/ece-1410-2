#include "rational.h"

/*****************************************************************************
* @name		Rational
* @brief	constructor for rational class, places values in the numerator
*			and denominator values
* @param	integers n and d for numerator and denominator
* @retval	none
*****************************************************************************/
Rational::Rational(int n, int d)
{
	numerator = n;
	denominator = d;
	reduce();
}

/*****************************************************************************
* @name		add
* @brief	adds a new fraction to the current private fraction value
* @param	integers n and d for numerator and denominator to be added
* @retval	none
*****************************************************************************/
void Rational::add(int n, int d)
{
	//store new denominator value
	int temp = d;
	//set new fraction to common denominator of both fractions
	d *= denominator;
	n *= denominator;
	//set old fraction to common denominator of both fractions
	denominator *= temp;
	numerator *= temp;
	//add numerators
	numerator += n;
	//reduce
	reduce();
}

/*****************************************************************************
* @name		subtract
* @brief	subtracts a new fraction from the current private fraction value
* @param	integers n and d for numerator and denominator to  be subtracted
* @retval	none
*****************************************************************************/
void Rational::subtract(int n, int d)
{
	//store new denominator value
	int temp = d;
	//set new fraction to common denominator of both fractions
	d *= denominator;
	n *= denominator;
	//set old fraction to common denominator of both fractions
	denominator *= temp;
	numerator *= temp;
	//subtract numerators
	numerator -= n;
	//reduce
	reduce();
}

/*****************************************************************************
* @name		multiply
* @brief	multiplys current private fraction value by a new fraction
* @param	integers n and d for numerator and denominator to be multiplied
* @retval	none
*****************************************************************************/
void Rational::multiply(int n, int d)
{
	//multiply numerators and denominators
	denominator *= d;
	numerator *= n;
	//reduce
	reduce();
}

/*****************************************************************************
* @name		divide
* @brief	divides a new fraction from the current private fraction value
* @param	integers n and d for numerator and denominator
* @retval	none
*****************************************************************************/
void Rational::divide(int n, int d)
{
	//cross multiply
	denominator *= n;
	numerator *= d;
	//reduce
	reduce();
}

/*****************************************************************************
* @name		print
* @brief	prints the current private fraction value
* @param	none
* @retval	none
*****************************************************************************/
void Rational::print()
{
	std::cout << numerator << "/" << denominator;
	std::cout << std::endl << std::endl;
}

/*****************************************************************************
* @name		reduce
* @brief	finds the greatest common factor of the current  private numerator
*			and denominator values, and then divides them by the GCF		
* @param	none
* @retval	none
*****************************************************************************/
void Rational::reduce()
{
	int a, b, d = 0, g, i, pwr = 1, gcf;
	//set temps
	a = numerator;
	b = denominator;
	//find GCF using binary algorithm
	while (a % 2 == 0 && b % 2 == 0)
	{
		a /= 2;
		b /= 2;
		d += 1;
	}
	//pwr = 2 to the d power
	for (i = 1; i <= d; i++)
	{
		pwr *= 2;
	}
	while (a != b)
	{
		if (a % 2 == 0)
		{
			a /= 2;
		}
		else if (b % 2 == 0)
		{
			b /= 2;
		}
		else if (a > b)
		{
			a = (a - b) / 2;
		}
		else
		{
			b = (b - a) / 2;
		}
	}
	g = a;
	gcf = g * pwr;
	//divide numerator and denominator by gcf
	numerator /= gcf;
	denominator /= gcf;
}