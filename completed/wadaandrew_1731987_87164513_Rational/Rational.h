#pragma once
/********************************************************************* 
* @brief class declaration for Rational
*********************************************************************/ 
class Rational {
	private:
		//data and hidden functions
		int num;
		int	den;
		void simp();
		int gcd(int a, int b);
	//declare public funtions for accessing data
	public:
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void printfrac();
		void setval(int n, int d);
};