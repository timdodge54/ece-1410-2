#include "Rational.h"
#include <iostream>
using namespace std;
/********************************************************************* 
* @name add 
* @brief adds fractions to what is stored in object of class rational
* @param int n, int d
* @retval  none 
*********************************************************************/ 
void Rational::add(int n, int d) {
	//teno makes code cleaner
	int temp = (num * d) + (den * n);
	den *= d;
	num = temp;
	simp();
}
/********************************************************************* 
* @name sub
* @brief subtracts fractions from what is stored in object of class rational
* @param int n, int d
* @retval  none 
*********************************************************************/ 
void Rational::sub(int n, int d) {
	//temp for easier readability
	int temp = (num * d) - (den * n);
	den *= d;
	num = temp;
	simp();
}
/********************************************************************* 
* @name mul
* @brief multiplies fractions by what is stored in object of class rational
* @param int n, int d
* @retval  none 
*********************************************************************/ 
void Rational::mul(int n, int d) {
	num *= n;
	den *= d;
	//simplifies result
	simp();
}
/********************************************************************* 
* @name div
* @brief divides fraction held in rational class by operators
* @param int n, int d
* @retval  none 
*********************************************************************/ 
void Rational::div(int n, int d) {
	num *= d;
	den *= n;
	//simplifies result
	simp();
}
/********************************************************************* 
* @name printfrac
* @brief Prints fraction to console 
* @param none 
* @retval  none 
*********************************************************************/ 
void Rational::printfrac() {
	//prints formatted fraction
	cout << num << "/" << den << endl << endl<<endl;
}
/********************************************************************* 
* @name setval
* @brief sets the value of the item in class
* @param int n, int d
* @retval  none 
*********************************************************************/
void Rational::setval(int n, int d) {
	//basically a wierd constructor
	num = n, den = d;
	simp();
}
/********************************************************************* 
* @name simp
* @brief simplifies fraction
* @param none 
* @retval  none 
*********************************************************************/ 
void Rational::simp() {
	//gets GCD by calling funct
	int gc = gcd(num, den);
	num /= gc;
	den /= gc;
	//check if it did anything before printing
	if (gc != 1) {
		cout << "Simplifies to: "; 
	}
}
/********************************************************************* 
* @name gcd
* @brief finds greatest common denominator 
* @param int n, int d
* @retval  int GCD
*********************************************************************/ 
int Rational::gcd(int a, int b)
{	//simple gcd algorithm
    if (a == 0)
        return b;
    return gcd(b % a, a);
}