#include "Rational.h"
#include <iostream>
#include <fstream>
#include <cstdio>
using namespace std;
int chint(char digit);
/********************************************************************* 
* @name main 
* @brief allows user to perform basic operations on a fraction
* @param none 
* @retval  none 
*********************************************************************/ 

int main () {
	// create and store values of numerator and denomenator
	int tn, td;
	cout << "Enter a numerator: ";
	cin >> tn;
	cout << "Enter a denominator: ";
	cin >> td;
	// create object of Rational class and set values to temp
	Rational rat;
	rat.setval(tn, td);
	rat.printfrac();
	//while loop for main iteration
	int flag = 1;
	while (flag == 1) {
		//try catch block for error handling.
		try {
			//temp char to store choice
			char selection;
			cout << "1. Add a rational" << endl << "2. Subtract a rational" << 
				endl << "3. Multiply by a rational" << endl <<
				"4. Divide by a rational" << endl << "0. Exit" <<
				endl << "Enter selection:";
			//gets temp char
			cin>>selection;
			//converts char to int
			int test = chint(selection);
			if (test <= 4 && test >=0) {
				switch (test) {
				//exits program
				case 0:
					return 0;
				//handles addition
				case 1:
					//gets num and denom
					cout << "Enter a numerator: ";
					cin >> tn;
					cout << "Enter a denominator: ";
					cin >> td;
					//calls respective function and prints value
					rat.add(tn, td);
					rat.printfrac();
					break;
				//handles subtraction
				case 2:
					//gets num and denom
					cout << "Enter a numerator: ";
					cin >> tn;
					cout << "Enter a denominator: ";
					cin >> td;
					//calls respective function and prints value
					rat.sub(tn, td);
					rat.printfrac();
					break;
				//handles multiplication
				case 3:
					//gets num and denom
					cout << "Enter a numerator: ";
					cin >> tn;
					cout << "Enter a denominator: ";
					cin >> td;
					//calls respective function and prints value
					rat.mul(tn, td);
					rat.printfrac();
					break;
				//handles division
				case 4:
					//gets num and denom
					cout << "Enter a numerator: ";
					cin >> tn;
					cout << "Enter a denominator: ";
					cin >> td;
					//calls respective function and prints value
					rat.div(tn, td);
					rat.printfrac();
					break;
				}

				
				
				
				
			}
			//throws selection if it is not a valid answer
			else {
				throw selection;
			}
		}
		//catch for unexpected behavior
		catch (char canu) {
			cout << endl << "Invalid selection.  You entered: " << (canu)
				<<endl<<endl;
		}
		//catches misc behavior
		catch (...) {
			cout << endl << "Invalid selection." << endl << endl;
		}
	}



}
/********************************************************************* 
* @name chint
* @brief converts a char into an int
* @param character
* @retval  int value corresponding to char
*********************************************************************/ 
int chint(char digit) {
	//returns input as an int
    return (int) (digit - '0');
}