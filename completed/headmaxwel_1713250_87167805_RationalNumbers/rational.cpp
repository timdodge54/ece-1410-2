#include <iostream>
#include <cmath>
#include "rational.h"

/***************************************************************
 * @name Rational
 * @param n
 * @param d
 * @brief constructor assigns values to numerator and denominator
 * @retval void
 ***************************************************************/

Rational::Rational(int n, int d){
    //sets values
    numerator = n;
    denominator = d;
    //reduces fraction and prints to screen
    reduce();
}
/***************************************************************
 * @name add
 * @param n
 * @param d
 * @brief finds a common denominator, then adds fractions
 * @retval void
 ***************************************************************/

void Rational::add(int n, int d){
    //asks for a numerator from the user
   std::cout<<"Enter a numerator: ";
    //gets numerator
    std::cin >> n;
    //asks for a denominator from the user
    std::cout<<"Enter a denominator: ";
    //gets denominator
    std::cin >> d;
    //calculates new fraction
    n *= denominator;
    numerator *=d;
    denominator *=d;
    numerator += n;
    //reduces fraction and prints to screen
    reduce();
}
/***************************************************************
 * @name sub
 * @param n
 * @param d
 * @brief finds a common denominator, then subtracts fractions
 * @retval void
 ***************************************************************/

void Rational::sub(int n, int d) {
    //asks for a numerator from the user
    std::cout<<"Enter a numerator: ";
    //gets numerator
    std::cin >> n;
    //asks for a denominator from the user
    std::cout<<"Enter a denominator: ";
    //gets denominator
    std::cin >> d;
    //calculates new fraction
    n *= denominator;
    numerator *=d;
    denominator *=d;
    numerator -= n;
    //reduces fraction and prints to screen
    reduce();
}
/***************************************************************
 * @name mul
 * @param n
 * @param d
 * @brief multiplies the two fractions together
 * @retval void
 ***************************************************************/

void Rational::mul(int n, int d) {
    //asks for a numerator from the user
    std::cout<<"Enter a numerator: ";
    //gets numerator
    std::cin >> n;
    //asks for a denominator from the user
    std::cout<<"Enter a denominator: ";
    //gets denominator
    std::cin >> d;
    //calculates new fraction
    numerator *= n;
    denominator *= d;
    //reduces fraction and prints to screen
    reduce();
}
/***************************************************************
 * @name div
 * @param n
 * @param d
 * @brief multiplies the original fraction by the reciprocal
 *        of the new fraction
 * @retval void
 ***************************************************************/

void Rational::div(int n, int d) {
    //asks for a numerator from the user
    std::cout<<"Enter a numerator: ";
    //gets numerator
    std::cin >> n;
    //asks for a denominator from the user
    std::cout<<"Enter a denominator: ";
    //gets denominator
    std::cin >> d;
    //calculates new fraction
    numerator *= d;
    denominator *= n;
    //reduces fraction and prints to screen
    reduce();
}
/***************************************************************
 * @name reduce
 * @param none
 * @brief finds the GFC of a fraction and divides the numerator
 *        and denominator by it to get the simplest for of the
 *        fraction
 * @retval void
 ***************************************************************/

void Rational::reduce(){

    int g,h,n,d;
    //assigns placeholder values for numerator and denominator
    n = numerator;
    d = denominator;
    //sets exponent value to 0
    h=0;
    //checks for even numerator and denominator
    while((n % 2 == 0) && (d % 2 == 0)){
        n /= 2;
        d /=2;
        h++;
    }
    //simplifies n and d to prime factors
    while(n!=d){
        if(n % 2 ==0){
            n /= 2;
        }else if(d % 2 == 0){
            d /= 2;
        }else if(n>d){
            n = (n - d)/2;
        }else{
            d = (d - n)/2;
        }
    }
    //sets g to n
    g = n;
    //calculates GCF
    g = g * pow(2, h);
    //reduces fraction by GCF
    numerator /= g;
    denominator /= g;
    //prints to screen
    print();
}
/***************************************************************
 * @name print
 * @param none
 * @brief prints new fraction to the screen
 * @retval void
 ***************************************************************/

void Rational::print(){
    //print to screen
    std::cout<<"Fraction Reduced: "<<numerator<<"/"<<denominator<<std::endl;
    //creates room for next lines of text
    std::cout<<std::endl<<std::endl;
}