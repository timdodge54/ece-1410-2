#include <iostream>
#include "rational.h"

using namespace std;

/********************************************************************
 * @name main
 * @param void
 * @brief program gets a fraction from user, gives arithmetic options
 *        to the user, then calculates the reduced answer and prints
 *        it to the screen
 * @retval int
 *******************************************************************/

int main() {    //Begin
    //creates variables
    int n, d, i;

    //asks for a numerator from the user
    cout<<"Enter a numerator: ";
    //gets numerator
    cin >> n;
    //asks for a denominator from the user
    cout<<"Enter a denominator: ";
    //gets denominator
    cin >> d;
    //creates fraction
    Rational t (n,d);
    //give user menu options
    while (i != 0)
    {
        //prints user options to screen
        cout<<"1. Add a rational"<<endl<<"2. Subtract a rational"<<endl;
        cout<<"3. Multiply by a rational"<<endl<<"4. Divide by a rational"<<endl;
        cout<<"0. Exit"<<endl<<"Enter Selection: ";
        //grabs user input. assigns value to i
        cin >> i;
        //executes conditions based on user input
        try{
            //adds fraction
            if(i==1){
                t.add(n, d);
            //subtracts fraction
            } else if (i==2){
                t.sub(n,d);
            //multiplies fraction
            }else if (i==3){
                t.mul(n,d);
            //divides fraction
            }else if (i==4){
                t.div(n,d);
            }else{
                throw i;
            }
        }
        //prints error message to screen
        catch (int i){
            cout<< "'"<< i << "'" << " is not a valid option."<<endl<<endl;
        }
    }
    return 0;
}   //End
