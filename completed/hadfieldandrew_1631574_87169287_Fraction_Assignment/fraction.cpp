#include "frac.h"
using namespace std;


/******************************************************************************
* @name main
* @brief asks the user to input a fraction and then performs arithmatic
*        operations on the fraction
* @param none
* @return none
******************************************************************************/
int main()
{
    int num, denom;
    char selec;

    //Prompts the user to enter a numerator and stores it
    cout << "Enter Numerator: ";
    cin >> num;

    //Checks if what they entered is a number, and if not have them try again
    while (cin.fail())
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << "\nPlease enter a valid number\n\n";
        cout << "Enter Numerator: ";
        cin >> num;
    }

    ////Prompts the user to enter a denominator and stores it
    cout << "Enter Denominator: ";
    cin >> denom;

    ////Checks if what they entered is a number, and if not have them try again
    while (cin.fail())
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cout << "\nPlease enter a valid number\n\n";
        cout << "Enter Denominator: ";
        cin >> denom;
    }

    //Stores the numerator and denominator in the constructor
    Rational frac (num, denom);

    //Runs the simplify function
    frac.simplify(num, denom);

    do
    {
        //Prints the options for the user
        cout << "1. Add a rational" << endl;
        cout << "2. Subtract a rational" << endl;
        cout << "3. Multiply a rational" << endl;
        cout << "4. Divide a rational" << endl;
        cout << "0. Exit" << endl;
        cout << "Enter selection: ";
        cin >> selec;
        
        //Reads which number the user enters
        switch (selec)
        {
            case '1':
                //Runs the add function
                frac.add(num, denom); 
                break;
            case '2':
                //Runs the subtract function
                frac.sub(num, denom);
                break;
            case '3':
                //Runs the multiply function
                frac.mul(num, denom);
                break;
            case '4':
                //Runs the divide function
                frac.divide(num, denom);
                break;
            case '0':
                //Exits the code
                cout << "Please enjoy your day!" << endl;
                return 0;
            default:
                //Tells the to try again 
                //Repeats the loop
                cout << "Please enter a valid number" << endl;
                break;
        }
    } while (true);
}