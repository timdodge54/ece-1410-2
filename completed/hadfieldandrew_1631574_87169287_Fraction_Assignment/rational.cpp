#include "frac.h"
using namespace std;

Rational::Rational (int n, int d)
{
    num = n;
    denom = d;
}

/******************************************************************************
* @name findGCD
* @brief finds the greatest common denominator of two integers
* @param a, b
* @return a
******************************************************************************/
int Rational::findGCD(int a, int b) 
{
    //checks if b is zero
    if (b == 0)
    {
        return a;
    }
    //calls itself after putting b as a and finding the remainder of a and b
    return findGCD(b, a % b);
}

/******************************************************************************
* @name simplify
* @brief simplifies a fracion
* @param num, denom
* @return none
******************************************************************************/
void Rational::simplify(int& num, int& denom)
{
    int GCD;
    //Runs the findGCD function
    GCD = findGCD(num, denom);
    //Divides the numerator by the GCD
    num /= GCD;
    //Divedes the denominator by the GCD
    denom /= GCD;
    //Prints the simplified function
    cout << "Simplified: " << num << "/" << denom << "\n" << endl;
}

/******************************************************************************
* @name add
* @brief adds a new fraction to the old one
* @param num, denom
* @return none
******************************************************************************/
void Rational::add(int& num, int& denom)
{
    int num2, denom2, temp, temp2;
    //gets the new numerator and denominator
    cout << "Enter numerator: ";
    cin >> num2;
    cout << "Enter denominator: ";
    cin >> denom2;
    //finds a common multiple and multiplies the numerators by that
    temp = num * denom2;
    temp2 = num2 * denom;
    //adds the numerators together
    num = temp + temp2;
    //sets the denominator as the common multiple
    denom = denom * denom2;
    //Resets the constructor and runs the simplify function
    Rational frac (num, denom);
    frac.simplify(num, denom);
}

/******************************************************************************
* @name sub
* @brief subtracts a new fraction to the old one
* @param num, denom
* @return none
******************************************************************************/
void Rational::sub(int& num, int& denom)
{
    int num2, denom2, temp, temp2;
    //gets the new numerator and denominator
    cout << "Enter numerator: ";
    cin >> num2;
    cout << "Enter denominator: ";
    cin >> denom2;
    //finds a common multiple and multiplies the numerators by that
    temp = num * denom2;
    temp2 = num2 * denom;
    //subtracts the numerators
    num = temp - temp2;
    //sets the denominator as the common multiple
    denom = denom * denom2;
    //Resets the constructor and runs the simplify function
    Rational frac (num, denom);
    frac.simplify(num, denom);
}

/******************************************************************************
* @name add
* @brief multiplies a new fraction to the old one
* @param num, denom
* @return none
******************************************************************************/
void Rational::mul(int& num, int& denom)
{
    int num2, denom2;
    //gets the new numerator and denominator from the user
    cout << "Enter numerator: ";
    cin >> num2;
    cout << "Enter denominator: ";
    cin >> denom2;
    //multiples the numerators together
    num *= num2;
    //multiplies the denominators together
    denom *= denom2;
    //Resets the constructor
    Rational frac (num, denom);
    //Runs the simplify function
    frac.simplify(num, denom);
}

/******************************************************************************
* @name add
* @brief divides a new fraction to the old one
* @param num, denom
* @return none
******************************************************************************/
void Rational::divide(int& num, int& denom)
{
    int num2, denom2;
    //gets the new numerator and denominator from the user
    cout << "Enter numerator: ";
    cin >> num2;
    cout << "Enter denominator: ";
    cin >> denom2;
    //multiples the old numerator with the new denominator
    num *= denom2;
    ////multiples the old denominator with the new numerator
    denom *= num2;
    //Resets the constructor
    Rational frac (num, denom);
    ////Runs the simplify function
    frac.simplify(num, denom);
}