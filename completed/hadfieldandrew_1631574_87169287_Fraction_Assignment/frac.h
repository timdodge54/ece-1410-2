#include <iostream>
#include <string>
#include <fstream>

class Rational {
    private:
        int num, denom;

    public:
        Rational (int n, int d);
        int findGCD(int a, int b);
        void simplify(int& num, int& denom);
        void add(int& n, int& d);
        void sub(int& n, int& d);
        void mul(int& n, int& d);
        void divide(int& n, int& d);
};

