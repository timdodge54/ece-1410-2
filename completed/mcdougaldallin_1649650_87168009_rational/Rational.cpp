#include "Rational.h"
#include <algorithm>

/*****************************************************************************
* @name Rational
* @brief Initilizes type Rational with a numberator and a denominator
* @param int n, int d
* @retval  none
*****************************************************************************/
Rational::Rational(int n, int d) {
	numerator = n;
	denominator = d;
	simplify();
}


/*****************************************************************************
* @name add
* @brief Adds a fraction to the current fraction
* @param int n, int d
* @retval  none
*****************************************************************************/
void Rational::add(int n, int d) {
	// Calulate new numerator
	numerator = (numerator * d) + (n * denominator);

	// Calculate new denominator
	denominator *= d;

	// Simplify
	simplify();
}


/*****************************************************************************
* @name sub 
* @brief Subtracts a fraction from the current fraction
* @param int d, int d
* @retval  none
*****************************************************************************/
void Rational::sub(int n, int d) {
	// Calulate new numerator
	numerator = (numerator * d) - (n * denominator);

	// Calculate new denominator
	denominator *= d;

	// Simplify
	simplify();
}


/*****************************************************************************
* @name mul
* @brief Multiplies the current fraction by a new fraction
* @param int n, int d
* @retval  none
*****************************************************************************/
void Rational::mul(int n, int d) {
	// Calulate new numerator
	numerator = numerator * n;

	// Calculate new denominator
	denominator *= d;

	// Simplify
	simplify();
}


/*****************************************************************************
* @name div
* @brief Divides the current fraciton by a new fraction
* @param int n, int d
* @retval  none
*****************************************************************************/
void Rational::div(int n, int d) {
	// Division is multiplication by reciprocal
	mul(d, n);

	// Simplify
	simplify();
}


/*****************************************************************************
* @name simplify
* @brief simplifies the current faction to its most reduced form
* @param none
* @retval  none
*****************************************************************************/
void Rational::simplify() {
	int GCF{}, i;
	// Find greatest common factor
	// Start with smallest between numerator and denominator
	i = std::min(numerator, denominator);
	while (i > 0) {
		// Check if both are divisable by i
		if (!(numerator % i) && !(denominator % i)) {
			// Set GCF to i and exit loop
			GCF = i;
			i = 0;
		}
		else { i--; }
	}

	// Divide numerator and denominator by GCF
	numerator = numerator / GCF;
	denominator = denominator / GCF;
}