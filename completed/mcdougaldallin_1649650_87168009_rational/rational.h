
class Rational
{
	// Private Functions
	void simplify();
public:
	// Data Members
	int numerator, denominator;

	// Class Constructor
	Rational(int, int);

	// Public Functions
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
};