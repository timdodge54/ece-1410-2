/*****************************************************************************
* @name main
* @brief Displays a menu to add, subtract, multipy, and divide two fractions.
* @param none
* @retval  none
*****************************************************************************/

#include <iostream>
#include "rational.h"
using namespace std;

int menu();

int main() {
	int num, den, selection;
	// Initilize starting fraction
	cout << "Enter Numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;
	Rational fraction(num, den);
	// Print reduced form
	cout << "Fraction reduces to ";
	cout << fraction.numerator << "/" << fraction.denominator << "\n";

	// Menu loop
	while (selection = menu()) {
		// Get new numerator and denominator
		cout << "Enter Numerator: ";
		cin >> num;
		cout << "Enter denominator: ";
		cin >> den;

		// Perform function based on selection
		switch (selection) {
		case 1:
			fraction.add(num, den);
			break;
		case 2:
			fraction.sub(num, den);
			break;
		case 3:
			fraction.mul(num, den);
			break;
		case 4:
			fraction.div(num, den);
			break;
		default:
			break;
		}

		// Print new fraction
		cout << fraction.numerator << "/" << fraction.denominator << "\n";
	}
	return 0;
}

/*****************************************************************************
* @name menu
* @brief Displays a menu and returns a selection. If invalid selection is made
*	retry.
* @param
* @retval  none
*****************************************************************************/

int menu() {
	char selection;
	int val;
	// Display menu
	cout << "1. Add a rational\n";
	cout << "2. Subtract a rational\n";
	cout << "3. Multiply by a rational\n";
	cout << "4. Divide by a rational\n";
	cout << "0. Exit\n";
	while (1) {
		cout << "Enter Selection: ";
		cin >> selection;
		// Get selection
		try {
			val = (int)selection - 48;
			if (!(val <= 4) || !(val >= 0)) {
				throw selection;
			}
			else {
				return val;
			}
		}
		catch (char sel) {
			cout << sel << " is not a valid selection.\n";
		}
	}
}