/******************************************************************************
*	@name		main
*	@brief		enters a user-driven loop to perform various operations on 
*				entered fractions.
*	@param		none
*	@retval		none
******************************************************************************/	
#include <iostream>
#include <iomanip>
#include <cmath>
#include "rational.h"

using namespace std;
int main ()
{
	//Begin
	//Get a fraction from the user
	int n, d, a, b;
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;
	
	//Store the fraction inside a class and output the fraction to the screen
	Rational fraction (n, d);
	cout << "Fraction reduces to ";
	fraction.retfrac();
	
	//While loop until the user exits the program
	while (true)
	{
		//initiate numerator, denominator, and selection variables for the loop
		int num = 0, den = 0;
		char s = '0';
		
		//Print the selection options to the screen
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		//Store user's selection into selection variable s
		cout << "Enter selection: ";
		cin >> s;
		
		//Try block
		//If s equals an int between 0 and 4, then throw the value of s
		//Else throw 5
		try
		{
			if(s == '0' || s == '1' || s == '2' || s == '3' || s =='4')
			{
				throw s;
			}
			
			else	
			{
				throw '5';
			}
		}
		catch (char e)
		{
			//If 1 is caught, then get fraction from user and perform addition 
			//on the two fractions and reduce the result
			if (e == '1')
			{
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				fraction.add(num, den);
				fraction.retfrac();
			}
			
			//If 2 is caught, then get fraction from user and subtract the 
			//second fraction from the first and reduce the result
				if (e == '2')
			{
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				fraction.sub(num, den);
				fraction.retfrac();
			}
			
			//If 3 is caught, then get fraction from user and multiply 
			//the two fractions together and reduce the result
				if (e == '3')
			{
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				fraction.mul(num, den);
				fraction.retfrac();
			}
			
			//If 4 is caught, then get fraction from user and divide the 
			//second fraction from the first and reduce the result
				if (e == '4')
			{
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				fraction.div(num, den);
				fraction.retfrac();
			}
			
			//If 0 is caught, then exit the program
				if (e == '0')
			{
				exit(0);
			}
			
			//If 5 is caught, then an illegal selection was made
			//Exit the program with an error message
			if (e == '5')
			{
			cout << "Illegal selection entered." << endl;
			exit(1);
			}
		}
	}
	
	//End
	return 0;
}