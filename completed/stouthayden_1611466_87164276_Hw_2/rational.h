#ifndef RATIONAL_H
#define RATIONAL_H

class Rational {
	private:
		float numerator, denominator;
		void reduce();
		
	public:
		Rational( int, int);
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void retfrac();
};

#endif