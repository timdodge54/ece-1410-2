#include <iostream>
#include <iomanip>
#include <cmath>
#include "rational.h"
using namespace std;

/******************************************************************************
*	@name		Rational::Rational
*	@brief		stores two integers into the class as numertor and denominator
*	@param		two ints n and d
*	@retval		N/A
******************************************************************************/	
Rational::Rational(int n, int d)
{
	numerator = n;
	denominator = d;
	reduce();
}

/******************************************************************************
*	@name		Rational::retfrac
*	@brief		returns a fraction to the screen
*	@param		none
*	@retval		none
******************************************************************************/	

void Rational::retfrac()
{
	cout << numerator << "/" << denominator << "\n" << "\n" << endl;
}

/******************************************************************************
*	@name		Rational::add
*	@brief		adds two fractions
*	@param		two ints n and d
*	@retval		none
******************************************************************************/	

void Rational::add(int n, int d)
{
	numerator = (d * numerator) + (denominator * n);
	denominator = d * denominator;
	reduce();
}

/******************************************************************************
*	@name		Rational::sub
*	@brief		subtracts a fraction from the stored fraction
*	@param		two ints n and d
*	@retval		none
******************************************************************************/	

void Rational::sub(int n, int d)
{
	numerator = (d * numerator) - (denominator * n);
	denominator = d * denominator;
	reduce();
}

/******************************************************************************
*	@name		Rational::mul
*	@brief		multiplies two fractions together
*	@param		two ints n and d
*	@retval		none
******************************************************************************/	

void Rational::mul(int n, int d)
{
	denominator = d * denominator;
	numerator = n * numerator;
	reduce();
}

/******************************************************************************
*	@name		Rational::div
*	@brief		divides a fraction from the stored fraction
*	@param		two ints n and d
*	@retval		none
******************************************************************************/	

void Rational::div(int n, int d)
{
	denominator = denominator * n;
	numerator = numerator * d;
	reduce();
}

/******************************************************************************
*	@name		Rational::div
*	@brief		divides a fraction from the stored fraction
*	@param		two ints n and d
*	@retval		none
******************************************************************************/	

void Rational::reduce()
{
	int a = numerator;
	int b = denominator;
	int d = 0;
	
	//This while loop solves for the exponent to calculate gcf
	while (( a % 2) == 0 && (b % 2) ==0)
	{
		a /= 2;
		b /= 2;
		d += 1;
	}
	
	//This while loop solves for the constant needed to multiply to find gcf
	while (a != b)
	{
		//If a is even then divide it by 2
		if ((a % 2) == 0 )
		{
			a /= 2;
		}
		
		//Else if b is even then divide it by 2
		else if ((b % 2) == 0 ) 
		{
			b /= 2;
		}
		
		//Else if a is greater than b, then a is the difference divided by 2
		else if (a > b)
		{
			a = (a - b)/2;
		}
		
		//Else b is the difference of b - a all divided by 2 
		else
		{
			b = (b - a)/2;
		}
	}

	//Compute the gcf and divide the numerator and denominator to reduce the fraction
	int gcf = a * (pow(2, d));
	numerator = numerator / gcf;
	denominator = denominator / gcf;
	
}
