/**************************************************************************************************************************************
* @name    main
* @brief
* @param   none
* @retval  none
***************************************************************************************************************************************/


#include <iostream>
#include <cmath>
#include <iomanip>
#include "rational.h"

using namespace std;

int main()
{
	int numerator, denominator;

	cout << "Enter numerator: ";
	cin >> numerator;
	cout << "Enter demoninator: ";
	cin >> denominator;
	Rational r(numerator, denominator);
	//menu driven loop
	//try catch block for illegal menu selections

	char choice = 1;



	while (choice != '0')
	{
		//menu driven loop with 6 selections 0 to quit.
		cout << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		Rational reduce(numerator, denominator);
		Rational print();
		cin >> choice;
		
		// make sure menu selection is valid
		try 
		{
			if (choice == '1')
				throw 1;
			if (choice == '2')
				throw 2;
			if (choice == '3')
				throw 3;
			if (choice == '4')
				throw 4;
			if (choice < '1' || choice > '4')
				cout <<endl << "Invalid selection!";
		}

		catch (int e)
		{
			if (e == 1) //selection 1 add
			{	
				 
				  r.add(numerator, denominator);
				  
				  r.print();
				 
			}
			else if (e == 2) //selection 2 subtract 
			{

				cout << "2";
			}
			else if (e == 3) //selection 3 multiply
			{

				cout << "3";


			}
			else if (e == 4) // selection 4 divide
			{
				cout << "4";

			}
			

		}
		










	}








	return 0;
}