/*************************************************************************************************************************
* Class Rational
*
* Summary: A class for dealing with rational numbers, adding, subtracting, multiplying, dividing
* reducing and printing.
******************************************************************************************************************************/

#ifndef RATIONAL_H
#define RATIONAL_H

class Rational {

public:
	Rational(int n, int d); //should call reduce
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void print();

private:
	void reduce();
	int numer, denom;
};
#endif

//constructor
//print the fraction public getter
