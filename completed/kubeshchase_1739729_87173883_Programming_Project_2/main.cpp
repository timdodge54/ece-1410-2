#include <iostream>
#include "rational.h"
using namespace std;

/*****************************************************************************
* @name		main
* @brief	Performs arithmatic on fractions, dictated by user
* @param	none
* @retval	none
*****************************************************************************/

int main() {
	int n, d, selection;
	enum errors {NONE, USER_EXIT, BAD_ENTRY};

	//Initialize the fraction
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;
	Rational frac(n, d);
	
	//Infinite loop
	while (true) {
		try {
			//Prompt user for action
			cout << endl << endl;
			cout << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a rational" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;
			cout << "Enter selection: ";
			cin >> selection;

			//Check if selection requires an exit
			switch (selection) {
			case 0:
				throw USER_EXIT;
				break;
			case 1: break;
			case 2: break;
			case 3: break;
			case 4: break;
			default:
				throw BAD_ENTRY;
				break;
			}

			//Call math functions based on input
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			switch (selection) { 
			case 1: 
				frac.add(n, d); 
				break;
			case 2:
				frac.sub(n, d);
				break;
			case 3:
				frac.mul(n, d);
				break;
			case 4:
				frac.div(n, d);
				break;
			}

			//Print resultant fraction
			frac.print();
		}
		//Handle exceptions
		catch (enum errors code) {
			if (code == USER_EXIT) {
				cout << "Exiting" << endl;
				return 0;
			}
			if (code == BAD_ENTRY) {
				cout << "There is no function associated with this selection";
				cout << endl << endl;
			}
		}
	}

	return 0;
}