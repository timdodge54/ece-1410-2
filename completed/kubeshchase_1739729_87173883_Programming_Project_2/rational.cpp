#include "rational.h"
#include <iostream>
using namespace std;

/*****************************************************************************
* @name		Rational 
* @brief	initializes num and denom variables
* @param	int numerator, int denominator
* @retval	none
*****************************************************************************/
Rational::Rational(int n, int d) {
	num = n;
	denom = d;
	simplify();
	return;
}
/*****************************************************************************
* @name		add
* @brief	adds two fractions
* @param	int numerator, int denominator
* @retval	none
*****************************************************************************/
void Rational::add(int n, int d) {
	tempNum = num * d;
	n = n * denom;
	num = tempNum + n;
	denom = d * denom;
	simplify();
	return;
}
/*****************************************************************************
* @name		sub
* @brief	subtracts user input fraction from stored fraction
* @param	int numerator, int denominator
* @retval	none
*****************************************************************************/
void Rational::sub(int n, int d) {
	tempNum = num * d;
	n = n * denom;
	num = tempNum - n;
	denom = d * denom;
	simplify();
	return;
}
/*****************************************************************************
* @name		mul
* @brief	multiplies stored fraction with user input
* @param	int numerator, int denominator
* @retval	none
*****************************************************************************/
void Rational::mul(int n, int d) {
	num = n * num;
	denom = d * denom;
	simplify();
	return;
}
/*****************************************************************************
* @name		div
* @brief	Divides stored by user input
* @param	int numerator, int denominator
* @retval	none
*****************************************************************************/
void Rational::div(int n, int d) {
	num = num * d;
	denom = denom * n;
	simplify();
	return;
}
/*****************************************************************************
* @name		print
* @brief	prints "numerator/denominator"
* @param	none
* @retval	none
*****************************************************************************/
void Rational::print() {
	cout << num << "/" << denom;
	return;
}

/*****************************************************************************
* @name		simplify
* @brief	simplifies stored fraction
* @param	none
* @retval	none
*****************************************************************************/
void Rational::simplify() {
	int a, b;

	//Euclidean algorithm for GCF
	for (a = num, b = denom; a != b; ) {
		if (a > b) {
			a = a - b;
		}
		else {
			b = b - a;
		}
	}
	num = num / a;
	denom = denom / a;

	return;
}