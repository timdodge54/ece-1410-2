#include "Rational.h"

/*************************************************************************
* @name main
* @brief creates a table of quadratic roots
* @param none
* @retval none
*************************************************************************/
int main(void) //Begin
{
	int n, d, flag = 0;

	cout << "Enter numerator: ";	 //ask for initial fraction from user
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;

	Rational fract(n, d);       // Constructor to initialize fract

	cout << "Fraction reduces to ";
	fract.printwhatwehave();	// print reduced version of fract


	while (flag != 1)		// loop until 0 or invalid entry is entered
	{

		switch (MenuSelection())  //use switch for selection menu
		{
			case '1': //call add function 
			{
				int n, d;
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
				fract.add(n, d);
				fract.printwhatwehave(); //print new reduced fract
			}
			break;
			case '2': //call subtract function
			{
				int n, d;
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter numerator: ";
				cin >> d;
				fract.sub(n, d);
				fract.printwhatwehave(); //print new reduced fract
			}
			break;
			case '3': // call multiply function
			{
				int n, d;
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter numerator: ";
				cin >> d;
				fract.mul(n, d);
				fract.printwhatwehave(); //print new reduced fract
			}
			break;
			case '4': // call division function
			{
				int n, d;
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter numerator: ";
				cin >> d;
				fract.div(n, d);
				fract.printwhatwehave(); //print new reduced fract
			}
			break;
			case '0': //quit program if 0 is entered
			{
				flag++;
			}
			break;
			default: // throw exception & print error message if not 0-4
			{
				try { throw 1; }
				catch (int e) { cout << "Invalid Entry! Try Again!";}
			}
		}
	}

	return 0; //End
}