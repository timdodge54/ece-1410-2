#include "Rational.h"

/*************************************************************************
* @name reduce
* @brief reduces the fraction within the class Rational
* @param none
* @retval none
*************************************************************************/
void Rational::reduce() {
	int a, b;

	a = num; //get private values of numerator
	b = denom; // get private values of denominator

	while (a != b) //loop until a equals b
	{
		if (a > b) //if a is bigger subtract b from it
		{
			a = (a - b);
		}
		else
		{
			b = (b - a); //if b is bigger subtract a from it
		}
	}

	num = num / a;     // divide by gcf to get reduced fraction
	denom = denom / b;

}



/*************************************************************************
* @name printwhatwehave
* @brief prints the current values of num and denom to the screen
* @param none
* @retval none
*************************************************************************/
void Rational::printwhatwehave() {
	Rational::reduce();				//reduce fractions
	cout << num << "/" << denom;	//print to screen
}


/*************************************************************************
* @name Rational
* @brief constructor that builds the initial fraction
* @param numerator and denominator input from user
* @retval none
*************************************************************************/
Rational::Rational(int n, int d) {
	num = n;			//constructor to assign initial values
	denom = d;
}


/*************************************************************************
* @name add
* @brief adds a fraction to the existing fraction
* @param numerator and denominator of fraction to be added
* @retval none
*************************************************************************/
void Rational::add(int n, int d) {
	num *= d;		//cross multiply
	n *= denom;
	denom *= d;
	num += n;		//add numerators across
}


/*************************************************************************
* @name sub
* @brief subtracts a fraction from the existing fraction
* @param numerator and denominator of fraction to be subtracted
* @retval none
*************************************************************************/
void Rational::sub(int n, int d) {
	num *= d;	//cross multiply
	n *= denom;
	denom *= d;
	num -= n;	//subtract numerators
}


/*************************************************************************
* @name mul
* @brief multiplies a fraction with the existing fraction
* @param numerator and denominator of fraction to be multiplied
* @retval none
*************************************************************************/
void Rational::mul(int n, int d) {
	num *= n;	//multiply numerators across
	denom *= d;	//multiply denominators across
}


/*************************************************************************
* @name div
* @brief divides the existing fraction by a new fraction
* @param numerator and denominator of new fraction
* @retval none
*************************************************************************/
void Rational::div(int n, int d) {
	num *= d;	//multiply by reciprocal
	denom *= n;
}



/*************************************************************************
* @name MenuSelection
* @brief prints the menu to the screen and collects users selection
* @param none
* @retval selection of the user
*************************************************************************/
char MenuSelection() {
	char selection;
	cout << endl << endl << endl;
	cout << "1. Add a rational" << endl;		//print menu to the screen
	cout << "2. Subtract a rational" << endl;
	cout << "3. Multiply by a rational" << endl;
	cout << "4. Divide by a rational" << endl;
	cout << "0. Exit" << endl;
	cout << "Enter selection: ";
	cin >> selection;	// grab selection as a char
	return (selection); //return selection char
}