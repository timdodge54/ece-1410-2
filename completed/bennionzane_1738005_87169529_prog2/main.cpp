#include "rational.h"
#include <iostream>

/*********************
* @name		main
* @brief	provide menu for user to do math with fractions
* @param	none
* @retval	int
*********************/



int main(void) {
	using namespace std;
	Rational frac;
	int num, den, inp, loop;
	loop = 1;
	
	//get initial numerator
	while (loop) {
		cout << "Enter numerator: ";
		cin >> num;

		//catch illegal inputs
		try {
			throw num;
		}
		catch (int e) {
			if (e < 0) {
				cout << "Not valid numerator" << endl;
			}
			else {
				loop = 0;
			}
		}
	}
	loop = 1;

	//get initial denominator
	while (loop) {
		cout << "Enter denominator: ";
		cin >> den;

		//catch illegal inputs 
		try {
			throw den;
		}
		catch (int e) {
			if (e < 0) {
				cout << "Not valid denominator" << endl;
			}
			else {
				loop = 0;
			}
		}
	}
	loop = 1;

	//set frac values
	frac.set_values(num, den);

	//loop
	while(true){
		while (loop) {
			//menu to select between functions
			cout << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a rational" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;
			cout << "enter selection: ";
			cin >> inp;


			//catch illegal inputs *repeat menu block
			try {
				throw inp;
			}
			catch (int e) {
				if (e == 0) {
					exit(1);
				}
				else if (e > 4 || e < 0) {
					cout << "Not a valid input" << endl << endl;
				}
				else {
					loop = 0;
				}			
				
			}
		}
		loop = 1;

		cout << "Enter numerator: ";
		cin >> num;
		cout << "Enter denominator: ";
		cin >> den;

		//execute functions
		if (inp == 1) {
			frac.add(num, den);
		}
		if (inp == 2) {
			frac.sub(num, den);
		}
		if (inp == 3) {
			frac.mul(num, den);
		}
		if (inp == 4) {
			frac.div(num, den);
		}
	}
	
	return 0;
}