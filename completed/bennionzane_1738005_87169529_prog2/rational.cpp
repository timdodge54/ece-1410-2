#include "rational.h"
#include <iostream>

using namespace std;

/*********************
* @name		set_values
* @brief	reduce fraction and set/print values of num and den
* @param	num and den
* @retval	none
*********************/
void Rational::set_values(int n, int d) { 
	num = n;
	den = d;

	//reduce fraction
	//if n != d
	while (n != d) {
		//if n>d
		if (n < d) {
			d -= n;
		}
		//if n<d
		if (d < n) {
			n -= d;
		}
	}
	num = num/n;
	den = den/d;

	//print fraction
	cout << num << "/" << den << endl << endl;
}

/*********************
* @name		add
* @brief	add two fractions
* @param	num and den
* @retval	none
*********************/
void Rational::add(int n, int d) {
	//get common denominator and add
	if (d == den) {
		set_values(n + num, den);
	}
	else {
		set_values(((den * n) + (d * num)), den * d);
	}
}

/*********************
* @name		sub
* @brief	subtract two fractions
* @param	num and den
* @retval	none
*********************/
void Rational::sub(int n, int d) {
	//get common denominator and subtract
	if (d == den) {
		set_values(n - num, den);
	}
	else {
		set_values(((d * num) - (den * n)), den * d);
	}
}

/*********************
* @name		mul
* @brief	multiply two fractions
* @param	num and den
* @retval	none
*********************/
void Rational::mul(int n, int d) {
	//multiply numerators and denominators
	set_values(num * n, den * d);
}

/*********************
* @name		div
* @brief	divide two fractions
* @param	num and den
* @retval	none
*********************/
void Rational::div(int n, int d) {
	//multiply by reciprical
	set_values(num * d, den * n);
}