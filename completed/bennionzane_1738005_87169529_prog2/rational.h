#ifndef RATIONAL
#define RATIONAL
class Rational {
private:
	int num, den;

public:
	void set_values(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
};

#endif