#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include "rational.h"
using namespace std;
/*********************************************************************
* @name		main
* @brief	Prompts user for a fraction, then as requested, it will add,
*			subtract, multiply, and divide then printing results to screen.
* @param	none
* @retval	none
*********************************************************************/
int main() { // Begin
	Rational ratio; // declare class
	int num1;
	int den1; // define needed variables
	int num2;
	int den2;
	int sel = 100;
	cout << "Enter numerator: ";
	cin >> num1; // get initial numerator
	cout << "Enter denominator: ";
	cin >> den1; // get initial denominator
	ratio.rational(num1, den1); // declare initial fraction
	cout << "Fraction reduces to "; 
	ratio.print(); // print simplified fraction
	while (sel != 0) { // while the menu selection isn't zero
		cout << "\n \n \n1. Add a rational \n2. Subract a rational";
		cout << "\n3. Multiply by a rational \n4. Divide by a rational";
		cout << "\n0. Exit";
		cout << "\nEnter a selection: "; // print the menu options
		try{ // try
			char selection;
			cin >> selection; // get menu selection
			sel = int(selection);
			if (selection == '0') { // if menu selection is zero
				return 0; // exit program
			}
				if (selection >= '0' && selection <= '4') { // if input valid
					cout << "Enter numerator: ";
					cin >> num2; // get the numerator of fraction #2
					cout << "Enter denominator: ";
					cin >> den2; // get the denominator of fraction #2
					switch (sel) {
						case '1': // if addition was selected
							ratio.add(num2, den2); 
							ratio.print(); // add and print results
							break;
						case '2': // if subtraction was selected
							ratio.sub(num2, den2);
							ratio.print(); // subtract and print results
							break;
						case '3': // if multiplication was selected
							ratio.mul(num2, den2);
							ratio.print(); // multiply and print results
							break;
						case '4': // if division was selected
							ratio.div(num2, den2);
							ratio.print(); // divide and print results
							break;
						case '0': // exit if 0 was selected
							return 0;

						default: // don't do anything as default
							cout << " "; // an error will be thrown
							break;
					}
				}
				else { // if input is not valid, throw the input
					throw selection;
				}
		} 
		catch (char c) { // catch the invalid input
			cout << "Your input '" << c << "' is not a valid."; // print error
		}
	}
	return 0; // End
}