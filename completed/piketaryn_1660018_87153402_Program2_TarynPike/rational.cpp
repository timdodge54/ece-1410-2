using namespace std;
#include "rational.h"
#include <iostream>
/*********************************************************************
* @name		Rational::rational
* @brief	stores the intital numerator and denominator.
* @param	int num1, int den1
* @retval	none
*********************************************************************/
void Rational::rational(int num1, int den1) { // Begin
	n = num1;
	d = den1; // set the numerator and denominator
	reduce(); // reduce if needed
} // End
/*********************************************************************
* @name		Rational::print
* @brief	Prints the reduced fraction to the screen.
* @param	none
* @retval	none
*********************************************************************/
void Rational::print() { // Begin
	cout << n << "/" << d; // print to screen
} // End
/*********************************************************************
* @name		Rational::reduce
* @brief	reduces the current fraction.
* @param	none
* @retval	none
*********************************************************************/
void Rational::reduce() { // Begin
	for (int t=0; t <= n && t <= d; t++) { // loop through all possibilities
		for (int i = 1; i <= n && i <= d; i++) { // find the next LCD.
			if (n % i == 0 && d % i == 0) { // see if larger is divisible
				n = n / i;
				d = d / i; // simplify the numerator and denominator
			}
		}
	}
} // End
/*********************************************************************
* @name		Rational::add
* @brief	adds fraction to the current.
* @param	int num2, int den2 // (the other fraction)
* @retval	none
*********************************************************************/
void Rational::add(int num2, int den2) { // Begin
	n = (n * den2) + (num2 * d);
	d = d * den2; // add the fractions
	reduce(); // reduce if needed.
} // End
/*********************************************************************
* @name		Rational::sub
* @brief	Subtracts fraction from the current.
* @param	int num2, int den2 // (the other fraction)
* @retval	none
*********************************************************************/
void Rational::sub(int num2, int den2) { // Begin
	n = (n * den2) - (num2 * d);
	d = d * den2; // subtract the fractions
	reduce(); // reduce if needed
} // End
/*********************************************************************
* @name		Rational::mul
* @brief	multiplies current fraction with given
* @param	int num2, int den2 // (the other fraction)
* @retval	none
*********************************************************************/
void Rational::mul(int num2, int den2) { // Begin
	d = d * den2;
	n = n * num2; // multiply the fractions
	reduce(); // reduce if needed
} // End
/*********************************************************************
* @name		Rational::div
* @brief	divides current fraction with given
* @param	int num2, int den2 // (the other fraction)
* @retval	none
*********************************************************************/
void Rational::div(int num2, int den2) { // Begin
	d = d * num2;
	n = n * den2; // divide the fractions
	reduce(); // reduce if needed
} // End
