#ifndef RATIONAL_H
#define RATIONAL_H
class Rational {
public:
	void rational(int num1, int den1);
	void add(int num2, int den2);
	void sub(int num2, int den2);
	void mul(int num2, int den2);
	void div(int num2, int den2);
	void print();
private:
	void reduce();
	int n, d;
};
#endif