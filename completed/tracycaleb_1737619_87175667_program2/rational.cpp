#include "rational.h"

/**
@name		set
@brief		assigns values to the numerator and denominator variables
@param		values for the numerator and denominator
@retval		none
*/
void Rational::set(int n, int d) {
	numerator = n;
	denominator = d;
}

/**
@name		add
@brief		adds two rational numbers
@param		numerator and denominator of 2nd rational
@retval		none
*/
void Rational::add(int n, int d) {
	int commonden;

	commonden = denominator * d;
	numerator = numerator * d;
	n = n * denominator;

	numerator = numerator + n;
	denominator = commonden;
}

/**
@name		sub
@brief		subtracts two rational numbers
@param		numerator and denominator of 2nd rational
@retval		none
*/
void Rational::sub(int n, int d) {
	int commonden;

	commonden = denominator * d;
	numerator = numerator * d;
	n = n * denominator;

	numerator = numerator - n;
	denominator = commonden;
}

/**
@name		mul
@brief		multiplies two rationals
@param		numerator and denominator of 2nd rational
@retval		none
*/
void Rational::mul(int n, int d) {
	numerator = numerator * n;
	denominator = denominator * d;
}

/**
@name		div
@brief		divides two rationals
@param		numerator and denominator of 2nd rational
@retval		none
*/
void Rational::div(int n, int d) {
	numerator = numerator * d;
	denominator = denominator * n;
}

/**
@name		reduce
@brief		reduces the rational to its simplest form
@param		none
@retval		none
*/
void Rational::reduce() {
	int a, b;
	a = numerator;
	b = denominator;

	// loop uses the euclidian algorithm to find the GCD
	while (a != 0 && b != 0) {
		if (a > b) {
			a = a % b;
		}
		else if (b > a) {
			b = b % a;
		}
		else {	
			a = 0;
		}
	}

	// divide numerator and denominator by the GCD
	if (a == 0) {
		numerator = numerator / b;
		denominator = denominator / b;
	}
	else if (b == 0) {
		numerator = numerator / a;
		denominator = denominator / a;
	}
}

/**
@name		print
@brief		prints a rational number
@param		none
@retval		none
*/
void Rational::print() {
	cout << numerator << '/' << denominator << endl << endl;
} 