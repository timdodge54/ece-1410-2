/**
@name		main
@brief		performs operations on positive rational values
@param		none
@retval		none
*/

#include "rational.h"

int main() {
	int num, den, x;
	Rational fraction;

	// collect input for first rational
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;

	// store, reduce, and print the rational
	fraction.Rational::set(num, den);
	fraction.Rational::reduce();
	fraction.Rational::print();

	
	do {
		// collect input for chosen operation
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> x;

		if (x == 0) { exit(0); }

		
		
		
		// exception handlin
		try {
			if (0 > x || x > 4) {
				throw 1;
			}
		}
		catch (...) {
			cout <<endl<<"'"<< x <<"'"<<"is not a valid option!"<<endl<<endl;
		}
		// perform chosen operation
		switch (x) {
		case 4: // operations for division
			cout << "Enter a numerator: ";
			cin >> num;
			cout << "Enter a denominator: ";
			cin >> den;
			fraction.Rational::div(num, den);
			fraction.Rational::reduce();
			fraction.Rational::print();
			break;
		case 3: // operations for multiplication
			cout << "Enter a numerator: ";
			cin >> num;
			cout << "Enter a denominator: ";
			cin >> den;
			fraction.Rational::mul(num, den);
			fraction.Rational::reduce();
			fraction.Rational::print();
			break;
		case 2: // operations for subtraction
			cout << "Enter a numerator: ";
			cin >> num;
			cout << "Enter a denominator: ";
			cin >> den;
			fraction.Rational::sub(num, den);
			fraction.Rational::reduce();
			fraction.Rational::print();
			break;
		case 1: // operations for addition
			cout << "Enter a numerator: ";
			cin >> num;
			cout << "Enter a denominator: ";
			cin >> den;
			fraction.Rational::add(num, den);
			fraction.Rational::reduce();
			fraction.Rational::print();
			break;
		}	
	} while (x != 0);

	return 0;
}