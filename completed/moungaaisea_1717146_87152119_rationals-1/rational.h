class Rational
{
	private:
		int numer, denom;
	public:
		Rational(int n, int d);
		void setval(int n, int d);
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
};

void reduce(int &n, int &d);
int greatestcommonfactor(int a, int b);