#include "rational.h"
#include <iostream>
using namespace std;
/********************************************************************* 
* @name Rational::Rational
* @brief constructor for default values
* @param none 
* @retval  none 
*********************************************************************/

Rational::Rational(int n, int d)
{
	//sets the default values of n/d
	numer = n;
	denom = d;
}

/********************************************************************* 
* @name Rational::setval
* @brief sets user values
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void Rational::setval(int n, int d)
{
	//sets the user values of n/d
	numer = n;
	denom = d;

	//auto reduces user input
	reduce(numer, denom);
	
	//prints the reduced form
	cout << "Fraction reduces to " << numer << "/" << denom;
}

/********************************************************************* 
* @name Rational::add
* @brief adds rationals
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void Rational::add(int n, int d)
{
	//setting variables
	int placeholdern, placeholderd;
	
	//calculates the common factor
	placeholderd = denom;
	
	denom *= d;
	numer *= d;
	
	n *= placeholderd;
	
	//adding the two rationals together
	numer += n;
	
	//reducing
	reduce(numer, denom);	
	
	//printing results
	cout << numer << "/" << denom;
}

/********************************************************************* 
* @name Rational::sub
* @brief subtracts rationals
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void Rational::sub(int n, int d)
{
	//setting variables
	int placeholdern, placeholderd;
	
	//calculates the common factor
	placeholderd = denom;
	
	denom *= d;
	numer *= d;
	
	n *= placeholderd;
	
	//subtracting the two rationals together
	numer -= n;
	
	//reducing
	reduce(numer, denom);	
	
	//printing results
	cout << numer << "/" << denom;
}

/********************************************************************* 
* @name Rational::mul
* @brief multiplies rationals
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void Rational::mul(int n, int d)
{
	//multiplying rationals
	numer *= n;
	denom *= d;
	
	//reducing
	reduce(numer, denom);
	
	//printing results
	cout << numer << "/" << denom;
}

/********************************************************************* 
* @name Rational::div
* @brief divides rationals
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void Rational::div(int n, int d)
{
	//inverting the input and multiplying rationals
	numer *= d;
	denom *= n;
	
	//reducing
	reduce(numer, denom);
	
	//printing results
	cout << numer << "/" << denom;
}

/********************************************************************* 
* @name greatestcommonfactor
* @brief loops while getting the gcd for the divisor
* @param int a (numerator), int b (denominator)
* @retval  integer (divisor)
*********************************************************************/

int greatestcommonfactor(int a, int b)
{
    //special case if b is 0, gcd is a
	if (b == 0)
	{
		return a;
	}
	
	//returns gcd of b and a % b recursively
    return greatestcommonfactor(b, a % b); 
}

/********************************************************************* 
* @name reduce
* @brief finds the value of the reduced rationals
* @param int n (numerator), int d (denominator)
* @retval  none 
*********************************************************************/

void reduce(int &n, int &d)
{
	 // get the gcd of numerator and denominator
    int divisor = greatestcommonfactor(n, d);
	
    n /= divisor; //divide numerator by gcd
    d /= divisor; //divide denominator by gcd
}