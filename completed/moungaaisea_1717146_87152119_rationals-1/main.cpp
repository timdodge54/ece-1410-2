/********************************************************************* 
* @name main 
* @brief calculates rational numbers based on the user's request 
* @param none 
* @retval  none 
*********************************************************************/

#include <iostream>
#include "rational.h"

using namespace std;

int main()
{
	//Begin
	//setting class
	Rational frac(1,1);
	int top, bot;
	char choice;
	
	cout << "Enter numerator: ";
	cin >> top;
	cout << "Enter denominator: ";
	cin >> bot;
	frac.setval(top, bot);
	
	//Setting loop for menu
	while(choice != '0')
	{
		//printing menu
		cout << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		//Asking user choice
		cout << "Enter selection: ";
		cin >> choice;
		
		//try/catch block for wrong inputs
		try
		{
			throw choice;
		}
		catch (char e)
		{
			//If their choice is not in between 0-4, it thows an exception
			if(e > '4' || e < '0')
			{
				cout << e << " is not a valid selection!";
			}
		}
		
		switch(choice)
		{
			//case 1 for adding
			case 49:
				cout << "Enter numerator: ";
				cin >> top;
				cout << "Enter denominator: ";
				cin >> bot;
				frac.Rational::add(top, bot);
				break;
			//case 2 for subtracting
			case 50:
				cout << "Enter numerator: ";
				cin >> top;
				cout << "Enter denominator: ";
				cin >> bot;
				frac.Rational::sub(top, bot);
				break;
			//case 3 for multiplying
			case 51:
				cout << "Enter numerator: ";
				cin >> top;
				cout << "Enter denominator: ";
				cin >> bot;
				frac.Rational::mul(top, bot);
				break;
			//case 4 for dividing
			case 52:
				cout << "Enter numerator: ";
				cin >> top;
				cout << "Enter denominator: ";
				cin >> bot;
				frac.Rational::div(top, bot);
				break;
				
			//All of them break and loop again until 0 is entered
		}
	}
	
	//End
	return 0;
}