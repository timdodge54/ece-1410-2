/*********************
@name	main
@brief	Calls functions of the class Rational to perform math 
operations on the fractions provided by the user
@param Fractions from the user
@retval	Outputs simplified rationals to terminal
*********************/

#include "rational.h"


int main(void)
{
	int n, d, n2, d2;
	char choice = 1;
	//Initialize variables

	cout << "Enter the numerator: ";
	cin >> n;
	cout << "Enter the denominator: ";
	cin >> d;
	Rational frac(n, d);
	cout << "Fraction reduces to ";
	frac.printoutput();
	//Prints the reduced fraction entered by the user

	while (true)
	{
		
		cout <<"1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Mutiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter Selection: ";
		//Prints Menu Selection

		cin >> choice;
		//Receives input from user

		//Checks if the input is a valid input.
		try {
			if (int(choice) < 48 || int(choice > 52)) {
				throw (choice);
			}
			}
		//If it is not enter an error message and reprint menu options
		catch (char choice) {
			cout << "Please enter a number between 0-4. " << choice <<
			" is not a valid entry. " << endl;
		}


	//If input is zero exit the program
		if (choice == 48) {
			exit(1);
		}
		//Receive values for the new fraction
		cout << "Enter the numerator: ";
		cin >> n2;
		cout << "Enter the denominator: ";
		cin >> d2;
		
		//If Choice is one
		if (int(choice) == 49)
		{
			//Call the Addition function
			frac.add(n2, d2);
		}
		//If Choice is two
		else if (int(choice) == 50)
		{
			//Call the Subtraction Function
			frac.sub(n2, d2);
		}
		//If Choice is three
		else if (int(choice) == 51)
		{
			//Call the Multiplication function
			frac.mul(n2, d2);
		}
		//If Choice is four
		else if (int(choice) == 52)
		{
			//Call the Division function
			frac.div(n2, d2);
		}
			
		//Print the new fraction to the screen
		frac.printoutput();
		
	}

}