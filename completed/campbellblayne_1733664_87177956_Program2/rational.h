#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <iomanip>
using namespace std;
class Rational
{
public:
    Rational(int n, int d);
    void add(int n, int d);
    void sub(int n, int d);
    void mul(int n, int d);
    void div(int n, int d);
    void printoutput();
    

private:
    int Numerator, Denominator;
    int gcd(int a, int b) {
        return (b == 0) ? a : gcd(b, a % b);
    };

};