/*********************
@name	Rational
@brief	Holds the functions that are called in the main function
to perform operations on fractions
@param	the numerator and denominator entered by the user
@retval	A new numerator and denominator 
*********************/ 
#include "rational.h"
Rational::Rational(int n, int d): Numerator(n), Denominator(d) {
	int g = gcd(Numerator, Denominator);
	Numerator /= g;
	Denominator /= g;
    
    //Reduces the fraction to the most simplified form
	}


void Rational::add(int n, int d) {
    Numerator = Numerator * d + n * Denominator;
    Denominator *= d;
    int g = gcd(Numerator, Denominator);
    Numerator /= g;
    Denominator /= g;

    //Adds two fractions together and reduces it to the simplest form
}
void Rational::sub(int n, int d) {
    Numerator = Numerator * d - n * Denominator;
    Denominator *= d;
    int g = gcd(Numerator, Denominator);
    Numerator /= g;
    Denominator /= g;

    //Subtracts one fraction from another and reduces it to the 
    //Simplest form
}
void Rational::mul(int n, int d) {
    Numerator *= n;
    Denominator *= d;
    int g = gcd(Numerator, Denominator);
    Numerator /= g;
    Denominator /= g;

    //Mutiplies two fractions and reduces it to its simplest form

}
void Rational::div(int n, int d) {
    Numerator *= d;
    Denominator *= n;
    int g = gcd(Numerator, Denominator);
    Numerator /= g;
    Denominator /= g;

    //Divides one fraction by another and reduces it to its
    //simplest form
}
void Rational::printoutput() {
    cout << Numerator << "/" << Denominator << endl<<endl<<endl;

    //Prints the updated fraction to the screen
}

