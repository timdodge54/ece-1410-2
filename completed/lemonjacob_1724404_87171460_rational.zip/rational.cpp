#include "rational.h"
#include <iostream>

using namespace std;


/*************************************************************************
* @name Rational
* @brief this is the constructor for the Rational class
* @param the numerator and denominator
* @retval none
*************************************************************************/
Rational::Rational(int num, int den)
{
	numerator = num;
	denominator = den;
	simplify();
};

/*************************************************************************
* @name showFrac
* @brief this function prints the fraction in the form
* numerator/denominator
* @param none
* @retval none
*************************************************************************/
void Rational::showFrac()
{
	cout << numerator << "/" << denominator << endl;
}

/*************************************************************************
* @name add
* @brief this function adds the fraction object to another fraction, then
* simplifies the result
* @param two ints, n and d
* @retval none
*************************************************************************/
void Rational::add(int n, int d)
{
	numerator = numerator*d + n*denominator;
	denominator *= d;
	simplify();
}

/*************************************************************************
* @name sub
* @brief this function subtracts the fraction object by another fraction,
* then simplifies the result
* @param two ints, n and d
* @retval none
*************************************************************************/
void Rational::sub(int n, int d)
{
	numerator = numerator * d - n * denominator;
	denominator *= d;
	simplify();
}

/*************************************************************************
* @name mul
* @brief this function multiplies the fraction object by another fraction,
* then simplifies the result
* @param two ints, n and d
* @retval none
*************************************************************************/
void Rational::mul(int n, int d)
{
	numerator *= n;
	denominator *= d;
	simplify();
}

/*************************************************************************
* @name div
* @brief this function divides the fraction object by another fraction,
* then simplifies the result
* @param two ints, n and d
* @retval none
*************************************************************************/
void Rational::div(int n, int d)
{
	numerator *= d;
	denominator *= n;
	simplify();
}

/*************************************************************************
* @name simplify
* @brief this function simplifies a fraction that is part of the 
* rational class
* @param none
* @retval none
*************************************************************************/
void Rational::simplify()
{
	int divisor;
	divisor = getDivisor(numerator, denominator);
	numerator /= divisor;
	denominator /= divisor;
}

/*************************************************************************
* @name getDivisor
* @brief this function gets the greatest common divisor of two numbers
* using the euclidean algorithm
* @param two integers, x and y
* @retval the greatest common denominator
*************************************************************************/
int Rational::getDivisor(int x, int y)
{
	if (x == y) 
	{
		return x;
	}
	else 
	{
		if (x < y) 
		{
			return getDivisor(x, y - x);
		}
		else 
		{
			return getDivisor(x - y, y);
		}
	}
}