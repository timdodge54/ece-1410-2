#pragma once
/********************************************************************
Class rational
summary: this does arithmetic operations on fractions
and simplification of rational numbers
********************************************************************/
class Rational {
private:
	int numerator, denominator;
	void simplify();
	int getDivisor(int x, int y);
	//int divisor(int x, int y);
public:
	Rational(int num, int den);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void showFrac();
};