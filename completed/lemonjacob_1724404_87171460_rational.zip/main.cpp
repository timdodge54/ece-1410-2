
#include <iostream>
#include <fstream>
#include "rational.h"

using namespace std;


/*************************************************************************
* @name main
* @brief this function utilizes the rational class to perform 
* arithmetic operations on rational numbers, and can simplify all
* calculated rational numbers
* @param none
* @retval none
*************************************************************************/

int main(void)
{
	int num, den, n, d;
	int done = 0;
	char menu = 5;
	//ask for and get the numerator and denominator
	cout << "enter numerator: ";
	cin >> num;
	cout << "enter denominator: ";
	cin >> den;
	//create the fraction object
	Rational frac(num, den);
	//print the fraction
	cout << "fraction reduces to: ";
	frac.showFrac();

	//loop until we are done, until they enter '0'
	while (done == 0)
	{
		//present the menu
		cout << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply a rational" << endl;
		cout << "4. Divide a rational" << endl;
		cout << "0. Exit" << endl;
		//ask for, and get menu selection
		cout << "Enter selection: ";
		cin >> menu;
		
		//test for good menu inputs using try, throw, and catchs
		try
		{
			//if there is a bad menu selection
			if (menu != '1' && menu != '2' && menu != '3' && menu != '4' && menu != '0')
			{
				//throw menu
				throw menu;
			}
		}
		//catch the menu thrown - it is a char
		catch (char e)
		{
			//print error message
			cout << menu << " Is an illegal menu selection. Try again or type 0 to exit" << endl;
			//this is to make sure the menu selection goes through the loop and doesn't do anything weird
			menu = 5;
		}
		//switch through the possible menu selections
		switch (menu)
		{
			//case 1 is addition
			case '1':
			{
				//ask for and get another fraction's numerator and denominator for adding
				cout << "enter numerator: ";
				cin >> n;
				cout << "enter denominator: ";
				cin >> d;
				//add the fractions and show simplified result
				frac.add(n, d);
				frac.showFrac();
			}break;
			//case 2 is subtraction
			case '2':
			{
				//ask for and get another fraction to subtract
				cout << "enter numerator: ";
				cin >> n;
				cout << "enter denominator: ";
				cin >> d;
				//subtract the fractions and show simplified result
				frac.sub(n, d);
				frac.showFrac();
			}break;
			//case 3 is multiplication
			case '3':
			{
				//ask for and get another fraction to multiply by
				cout << "enter numerator: ";
				cin >> n;
				cout << "enter denominator: ";
				cin >> d;
				//multiply fractions and show the simplified result
				frac.mul(n, d);
				frac.showFrac();
			}break;
			//case 4 is division
			case '4':
			{
				//ask for and get another fraction to divide 
				cout << "enter numerator: ";
				cin >> n;
				cout << "enter denominator: ";
				cin >> d;
				//divide fractions and show the simplified result
				frac.div(n, d);
				frac.showFrac();
			}break;
			//case 0 is for exiting
			case '0':
			{
				cout << endl;
				//change flag variable to exit the loop
				done = 1;
			}break;
		}
	}//end while loop, which will be done when 0 is entered and flag is changed
	return 0;
}