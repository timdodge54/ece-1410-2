#ifndef RATIONAL_H
#define RATIONAL_H

class Rational {
public:
    Rational(int numerator , int denominator );
    void add(int numerator, int denominator);
    void sub(int numerator, int denominator);
    void mul(int numerator, int denominator);
    void div(int numerator, int denominator);
    void reduce();
    int gN() const;
    int gD() const;

private:
    int numerator;
    int denominator;
};

#endif
