#include <iostream>
#include "rational.h"


/*********************************************************************
* @name main
* @brief takes input for fraction and does a series of calculations
* @param none
* @retval  none
*********************************************************************/

int main() {
    int numerator, denominator; //ints for numerator and denomionator 
    char choice; //menu selection char
    int n, d;  // second numerator and denominator intts

    //ask user for fraction
    std::cout << "Enter numerator: ";
    std::cin >> numerator;
    std::cout << "Enter denominator: ";
    std::cin >> denominator;

    // creating fraction
    Rational fraction(numerator, denominator);

    // reducing fraction
    std::cout << "Fraction reduces to: " << fraction.gN() 
        << "/" << fraction.gD() << std::endl;
    std::cout << std::endl;

    // while loop until user exits
    while (true) {

        // printing selection menu
        std::cout << "1 - Add a rational" << std::endl;
        std::cout << "2 - Subtract a rational" << std::endl;
        std::cout << "3 - Multiply a rational" << std::endl;
        std::cout << "4 - Divide a rational" << std::endl;
        std::cout << "0 - Exit" << std::endl;
        std::cout << "Enter selection: ";
        std::cin >> choice;

        // if statement for exit
        if (choice == '0') {
            break;
        }


        // try-catch block 
        try {
            switch(choice){
            case '1':
                // Add 
                // ask user for numerator and denominator 
                std::cout << "Enter the numerator: ";
                std::cin >> n;
                std::cout << "Enter the denominator: ";
                std::cin >> d;
                // using add method
                fraction.add(n, d);
                // printing summed reduced form
                std::cout << "Fraction reduces to: " << fraction.gN() 
                    << "/" << fraction.gD() << std::endl;
                std::cout << std::endl;
                break;


                //case 1, 2, 3, 4 have the same functionality, 
                //expect they sub, mul and div instead of ass
            case '2':
                std::cout << "Enter the numerator: ";
                std::cin >> n;
                std::cout << "Enter the denominator: ";
                std::cin >> d;
                fraction.sub(n, d);
                std::cout << "Fraction reduces to: " << fraction.gN() 
                    << "/" << fraction.gD() << std::endl;
                std::cout << std::endl;
                break;
            case '3':
                std::cout << "Enter the numerator: ";
                std::cin >> n;
                std::cout << "Enter the denominator: ";
                std::cin >> d;
                fraction.mul(n, d);
                std::cout << "Fraction reduces to: " << fraction.gN() 
                    << "/" << fraction.gD() << std::endl;
                std::cout << std::endl;
                break;
            case '4':
                std::cout << "Enter the numerator: ";
                std::cin >> n;
                std::cout << "Enter the denominator: ";
                std::cin >> d;
                fraction.div(n, d);
                std::cout << "Fraction reduces to: " << fraction.gN() 
                    << "/" << fraction.gD() << std::endl;
                std::cout << std::endl;
                break;
            default:
                // Invalid choice print
                throw "Invalid choice";
                break;
            }
        }
        catch (const char* msg) {
            // If exeption print message.
            std::cout << msg << std::endl;
        }
    }

    return 0;
}