#include "rational.h"
#include <numeric>


/*********************************************************************
* @name Rational
* @brief initialize the numerator and denominator
* @param none
* @retval  none
*********************************************************************/

// initialize the numerator and denominator 
Rational::Rational(int numerator, int denominator)
    : numerator(numerator), denominator(denominator) {
    // reduce function 
    reduce();
}

/*********************************************************************
* @name add
* @brief adds two rationals
* @param none
* @retval  none
*********************************************************************/

//  add two rationals 
void Rational::add(int n, int d) {
    // formula 
    numerator = numerator * d + n * denominator;
    denominator *= d;
    // simplify the fraction 
    reduce();
}

/*********************************************************************
* @name sub
* @brief subtracts two rationals
* @param none
* @retval  none
*********************************************************************/


// function to subtract 
void Rational::sub(int n, int d) {
    // formula 
    numerator = numerator * d - n * denominator;
    denominator *= d;
    // simplify the fraction 
    reduce();
}

/*********************************************************************
* @name mul
* @brief multiplys two rationals
* @param none
* @retval  none
*********************************************************************/

// function to multiply
void Rational::mul(int n, int d) {
    // formula 
    numerator *= n;
    denominator *= d;
    // simplify the fraction 
    reduce();
}

/*********************************************************************
* @name div
* @brief divides two rationals
* @param none
* @retval  none
*********************************************************************/

// function to divide 
void Rational::div(int n, int d) {
    // formula
    numerator *= d;
    denominator *= n;
    // simplify the fraction 
    reduce();
}

/*********************************************************************
* @name gcd
* @brief find the greatest common denominator
* @param none
* @retval  none
*********************************************************************/

// gcd function
int gcd(int a, int b) {
    while (b != 0) {
        int t = b;
        b = a % b;
        a = t;
    }
    return a;
}
/*********************************************************************
* @name reduce
* @brief reduces the rational
* @param none
* @retval  none
*********************************************************************/

// simplify the fraction
void Rational::reduce() {
    // find the gcd 
    int g = gcd(abs(numerator), abs(denominator));
    // divide 
    numerator /= g;
    denominator /= g;
}

// function for numerator
int Rational::gN() const {
    return numerator;
}

// function for denominator
int Rational::gD() const {
    return denominator;
}