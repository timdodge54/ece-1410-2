#ifndef RATIONAL_H
#define RATIONAL_H
#include <iostream>
class Rational {
private:
	int num, den;
	void reduce();
public:
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	Rational(int n, int d);
	void print();
};
#endif