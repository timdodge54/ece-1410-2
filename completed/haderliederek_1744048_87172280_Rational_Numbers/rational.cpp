#include "rational.h"
using namespace std;
/*********************************************************************
* @name add
* @brief Performs addition on rational expressions
* @param two ints: n and d
* @retval  none
*********************************************************************/
void Rational::add(int n, int d)
{
	if (den == d)
	{
		num += n;
	}
	else {
		num = num * d;
		n = n * den;
		den = den * d;
		num += n;
	}
	reduce();
	print();
	return;
}
/*********************************************************************
* @name sub
* @brief Performs subtraction on rational expressions
* @param two ints: n and d
* @retval  none
*********************************************************************/
void Rational::sub(int n, int d)
{
	if (den == d)
	{
		num -= n;
	}
	else {
		num = num * d;
		n = n * den;
		den = den * d;
		num -= n;
	}
	reduce();
	print();
	return;
}
/*********************************************************************
* @name mul
* @brief Performs multiplication on rational expressions
* @param two ints: n and d
* @retval  none
*********************************************************************/
void Rational::mul(int n, int d)
{
	num = num * n;
	den = den * d;
	reduce();
	print();
}
/*********************************************************************
* @name div
* @brief Performs division on rational expressions
* @param two ints: n and d
* @retval  none
*********************************************************************/
void Rational::div(int n, int d)
{
	num = num * d;
	den = den * n;
	reduce();
	print();
}
/*********************************************************************
* @name Rational
* @brief Constructs a rational expression
* @param two ints: n and d
* @retval  none
*********************************************************************/
Rational::Rational(int n, int d)
{
	num = n;
	den = d;
	reduce();
	print();
}
/*********************************************************************
* @name reduce
* @brief Reduces a rational expression
* @param none
* @retval  none
*********************************************************************/
void Rational::reduce()
{
	int i;
	bool flag;

	if (num < den)
	{
		i = num;
	}
	else {
		i = den;
	}
	flag = false;
	while (!flag)
	{
		if ((num % i == 0) && (den % i == 0))
		{
			flag = true;
		}
		else {
			i--;
		}
	}
	num /= i;
	den /= i;
}
/*********************************************************************
* @name print
* @brief Prints a rational expression
* @param none
* @retval  none
*********************************************************************/
void Rational::print()
{
	cout << num << "/" << den << endl << endl;
}