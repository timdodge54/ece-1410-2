#include "rational.h"
using namespace std;
/*********************************************************************
* @name main
* @brief Performs basic arithmetic on rational expressions
* @param none
* @retval  none
*********************************************************************/
int main(void)
{
	int num, den;
	char check;
	int term = 0;
	//get expression information
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> den;
	cout << "Fraction reduces to ";
	//construct r and print reduced form
	Rational r(num, den);
	//menu-driven loop
	for (;term != 1;)
	{
		//describe choices
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		//ask for selection
		cout << "Enter selection: ";
		cin >> check;
		//check input
		try {
			//throw exceptions
			if (check < '0' || check > '4')
			{
				throw (check);
			}
			//select functionality
			switch (check)
			{
			//exit
			case '0': term = 1;
				break;
			//add
			case '1': cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				r.add(num, den);
				break;
			//subtract
			case '2': cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				r.sub(num, den);
				break;
			//multiply
			case '3': cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				r.mul(num, den);
				break;
			//divide
			case '4': cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
				r.div(num, den);
				break;
			}
		}
		//catch exceptions
		catch (char sel) {
			cout << endl << sel << " is not a valid input. Try again." << endl << endl;
		}
	}
	return 0;
}