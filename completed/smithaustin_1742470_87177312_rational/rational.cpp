#include <iostream>
#include <cmath>
#include "rational.h"
using namespace std;
/********************************************
* @name rational
* @brief Functions to add, subtract, divide, multipy, print and reduce 
* @param none
* @retval none
********************************************/

//Greates Common Factor maker
int gcd(int a, int b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}
    //set rational param
    Rational::Rational(int n, int d) {
        DENER = d;
        NUMER = n;
        reduce();
    } 
    //add function
    void Rational::add(int n, int d) {
        NUMER = NUMER * n + d * DENER;
        DENER = DENER * d;
        reduce();
    }
    //subtract function
    void Rational::sub(int n, int d) {
        NUMER = NUMER * d - n * DENER;
        DENER = DENER * d;
        reduce();
    }
    //multiply function
   void Rational::mul(int n, int d) {
        NUMER = NUMER * n;
        DENER = DENER * d;
        reduce();
    }
   //divide function
   void Rational::div(int n, int d) {
        NUMER = NUMER * d;
        DENER = DENER * n;
        reduce();
    }
   //print rational
   void Rational::print() {
       cout << NUMER << "/" << DENER << endl;
   }


   //reducer
    void Rational::reduce() {
        int gcd1 = gcd(NUMER, DENER);
        NUMER /= gcd1;
        DENER /= gcd1;
    }

