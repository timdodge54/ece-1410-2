
#ifndef RATIONAL_H
#define RATIONAL_H
#include<iostream> 
using namespace std;

int gcd(int a, int b);

class Rational {
public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void print();
private:
	void reduce();
	int NUMER, DENER;
};



#endif