#include <iostream>
#include <cmath>
#include "rational.h"
using namespace std;
/********************************************
* @name main
* @brief Menu select for rationals
* @param none
* @retval none
********************************************/
int main() {
    int numerator, denominator;
    //initial input
    cout << "Enter a numerator: ";
    cin >> numerator;
    cout << "Enter a denominator: ";
    cin >> denominator;
    Rational ASD(numerator, denominator);
    //forever loop
    while (true) {
    ASD.print();
    //selection menu
    cout << "\n \nEnter the operation you want to perform:\n";
    cout << "1. Addition\n";
    cout << "2. Subtraction\n";
    cout << "3. Multiplication\n";
    cout << "4. Division\n";
    cout << "0. Quit\n";
    char choice;
        //input choice
        cout << "Enter your choice: ";
        cin >> choice;
        try {
            //sorter
        switch (choice) {
        case 48:
            cout << "Quitting...";
            return 0;
        case 49: //add
            cout << "Enter numerator: ";
            cin >> numerator;
            cout << "Enter denominator: ";
            cin >> denominator;
            ASD.add(numerator, denominator);
            break;
        case 50://subtract
            cout << "Enter numerator: ";
            cin >> numerator;
            cout << "Enter denominator: ";
            cin >> denominator;
            ASD.sub(numerator, denominator);
            break;
        case 51://multiply
            cout << "Enter numerator: ";
            cin >> numerator;
            cout << "Enter denominator: ";
            cin >> denominator;
            ASD.mul(numerator, denominator);
            break;
        case 52://divide
            cout << "Enter numerator: ";
            cin >> numerator;
            cout << "Enter denominator: ";
            cin >> denominator;
            ASD.div(numerator, denominator);
            break;
        default: //Other
            throw choice;
        }
    }   //if error
        catch (char problem) {
            cout << "'" << problem << "' is not a valid input" << endl;
        }
    }
       
        
}

