#include <iostream>
using namespace std;
#include "rational.h"

/**************************
* @name main
* @brief Runs a user interactive calculator that can 
	perform functions on fractions
* @param none
* @retval none
****************************/
int main() {
	int selection;
	int num; int denom; 
	int newNum; int newDenom;

	//get user input for starting fraction 
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> denom;
	//reduce if possible
	Rational fract(num, denom);

	//While loop containing mennu
	while (true) {
		cout << endl << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		try {
			cin >> selection;
			//if selection is 0, end program
			if (selection == 0) {
				return 0;
			}
			//throw excpetion if invalid menu selection
			if (selection < 0 ||  selection > 4)
			{
				throw (selection);
			}

			//get the second fraction info
			cout << "Enter numerator: ";
			cin >> newNum;
			cout << "Enter denominator: ";
			cin >> newDenom;
		}
		catch (int desire) { //catch the exception
			cout << desire << " is not a valid menu operation" << endl;
		}

			//perform the operation based on which selection		
			switch (selection) {
			case 1:
				fract.add(newNum, newDenom);
				break;
			case 2:
				fract.sub(newNum, newDenom);
				break;
			case 3:
				fract.mul(newNum, newDenom);
				break;
			case 4:
				fract.div(newNum, newDenom);
				break;
			}
	}
	return 0;
}