#include <iostream>
using namespace std;
#include "rational.h"

Rational::Rational() {}

Rational::Rational(int n, int d) {
	num = n;
	denom = d;
	reduce();
}

/******************
*@name		findGCD
*@brief		finds the greatest common denominator for the fraction to be able
*				to later reduce the fraction, using the Euclidean algorithm
*@param		int n, int d -- the numerator and denominator of the fraction
*@retval	the greatest common denominator
********************/
int Rational::findGCD(int n, int d) {
	//base case 
	if (d == 0) {
		return n;
	}
	//recursive call to find the gcd
	return findGCD(d, n % d);
}

/******************
*@name		reduce	
*@brief		reduces the fraction	
*@param		none	
*@retval	none
********************/
void Rational::reduce() {
	int n = num;
	int d = denom;
	//find gcd
	int gcd = findGCD(num, denom);
	//divide by gcd
	num /= gcd;
	denom /= gcd;

	//If there is a new fraction, print it out
	if (num != n && d != denom) {
		cout << "Fraction reduces to " << num << "/" << denom << endl;
	}
}

/******************
*@name		add
*@brief		adds a fractions to the current fraction
*@param		int n, int d  -- numerator and denominator of fraction to be added
*@retval	none
********************/
void Rational::add(int n, int d) {
	int newNum;
	int newDenom;
	int lcm;

	//Find common denominator
	newDenom = findGCD(d, denom);
	
	//find lcm
	lcm = (d * denom);
	//Find new denominator
	newDenom = lcm / newDenom;

	//find new numerator
	newNum = (n) * (newDenom / d) + (num) * (newDenom / denom);

	num = newNum;
	denom = newDenom;

	//print it out
	cout << num << "/" << denom << endl;
	reduce();
}

/******************
*@name		sub
*@brief		subtracts a fractions to the current fraction
*@param		int n, int d  -- numerator and denominator of fraction to be 
*				subtracted
*@retval	none
********************/
void Rational:: sub(int n, int  d) {
	int newNum;
	int newDenom;
	int lcm;

	//Find common denominator
	newDenom = findGCD(d, denom);

	//find lcm
	lcm = (d * denom);
	//Find new denominator
	newDenom = lcm / newDenom;

	//find new numerator
	newNum = (num) * (newDenom / denom) - (n) * (newDenom / d);

	num = newNum;
	denom = newDenom;

	//print it out
	cout << num << "/" << denom << endl;
	reduce();
}

/******************
*@name		mul
*@brief		multiplies a fractions to the current fraction
*@param		int n, int d  -- numerator and denominator of fraction to be
*				multiplied
*@retval	none
********************/
void Rational::mul(int n, int d) {
	//Do the math
	num = n * num;
	denom = d * denom;
	//print it out
	cout << num << "/" << denom << endl;
	reduce();
}

/******************
*@name		div
*@brief		divides a fractions to the current fraction
*@param		int n, int d  -- numerator and denominator of fraction to be
*				divided
*@retval	none
********************/
void Rational::div(int n, int d) {
	//do the math
	// multiply by the reciprical
	num = num * d;
	denom = denom * n;
	//print it out
	cout << num << "/" << denom << endl;
	reduce();
}