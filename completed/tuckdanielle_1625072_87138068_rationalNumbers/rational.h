class Rational{
private:
	int num;
	int denom;
	
	int findGCD(int n, int d);
public:
	Rational();
	Rational(int, int);
	void reduce();
	void add(int n, int d);
	void sub(int n, int  d);
	void mul(int n, int d);
	void div(int n, int d);
};

