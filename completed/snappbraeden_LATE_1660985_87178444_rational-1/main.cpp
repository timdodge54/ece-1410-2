/******************************************************************************
    @name main
    @brief Take a fractional input and have it be added subtracted multiplied
    @brief or divided based on user input
    @param none
    @retval none

******************************************************************************/

#include "rational.hpp"
#include <iostream>
#include <cmath>
#include <string>
using namespace std;

int main(){
    int numerator, denominator, n, d, e;
    char in;
    //declare the int values
    in = '1'; // set in to 1 so while loop will work
    cout << "Enter Numerator: "; //get first num value
    cin >> numerator; //get first num value
    cout << "Enter Denominator: "; //get first denom value
    cin >> denominator; //get first denom value
   
    Rational r (numerator, denominator); //calls constructor to reduce
    
    while (in != '0'){ //while loop for the entire rest of section
        cout << "1. Add a rational" << endl;
        cout << "2. Subtract a rational" << endl;
        cout << "3. Multiply a rational" << endl;
        cout << "4. Divide a rational" << endl;
        cout << "0. Exit" << endl;
        cout << "Enter Selection: ";
        //print statements based on project
        cin >> in; //get input from user
        //cin to a char or an int
            try{ //try loop to see what the user entered
                if (in > '4' || in < '0'){
                    throw 5;
                    //check to see if its a number listed if not throw 5
                }else if (in == '0'){
                    throw 0; //see if its 0 then throw 0
                }else if (in == '1'){
                    throw 1; //see if its 1 then throw 1
                }else if (in == '2'){
                    throw 2; //see if its 2 then throw 2
                }else if (in == '3'){
                    throw 3; //see if its 3 then throw 3
                }else if (in == '4'){
                    throw 4; //see if its 4 then throw 4
                }
            } catch (int e){ //catch number thrown
                if (e == 0){ //if number is 0 end program
                    return 0; //if 0 end program
                }else if (e == 5){ //if number thrown is 5 tell um to try again
                    cout << "'" << in << "' is not a viable option." << endl;
                }else if (e == 1) { //check to see if number thrown is 1
                    cout << "Enter Numerator: ";
                    //get num to be added to other num
                    cin >> n; //get num to be added to other num
                    cout << "Enter Denominator: ";
                    //get denom to be added to other denom
                    cin >> d; //get denom to be added to other denom
                     
                    r.add (n,d); //calls add functions
                  
                }else if(e == 2) {
                    cout << "Enter Numerator: ";
                    //get num to be subtracted from other num
                    cin >> n; //get num to be subtracted from other num
                    cout << "Enter Denominator: ";
                    //get denom to be subtracted from other denom
                    cin >> d;
                    //get denom to be subtracted from other denom
                    
                    r.sub (n,d); //calls subtract function

                }else if(e == 3) {
                    cout << "Enter Numerator: ";
                    //get num to be multiplied to other num
                    cin >> n; //get num to be multiplied to other num
                    cout << "Enter Denominator: ";
                    //get denom to be multiplied to other denom
                    cin >> d;
                    //get denom to be multiplied to other denom
                   
                    r.mul (n,d); //calls multiply function
                    
                }else if(e == 4) {
                    cout << "Enter Numerator: ";
                    //get num to be divided by other num
                    cin >> n; //get num to be divided by other num
                    cout << "Enter Denominator: ";
                    //get denom to be divided by other denom
                    cin >> d; //get denom to be divided by other denom
                    
                    r.div (n,d); //calls divide funciton
                    
                }
            }
    }
}
