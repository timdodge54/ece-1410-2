#include "rational.hpp"
#include <iostream>
#include <cmath>
#include <string>
using namespace std;
/***
 @name rational
 @brief constructor for the first part
 @param int numerator, denominator, n, d
 @retval none
 */
Rational::Rational (int n, int d)
{ //constructor for the class rational
    numerator = n; //placeholder
    denominator = d; //placeholder
    reduce (); //reduce original fraction
    cout << "Fraction reduces to: " << numerator << "/" << denominator <<endl;
    //1st print statement we need
}
/***
 @name reduce
 @brief reduce the values inputed
 @param int numerator, denominator, n, d
 @retval none
 */
void Rational::reduce ()
{ //function used to reduce the fraction
    int n, d, x, y;
    x=0; //set = 0 will be changed later
    n = numerator; //store the original value so we can simplify
    d = denominator; //store the original value so we can simplify
    while((numerator%2 == 0) && (denominator%2 == 0)){
        //while the num and denom are even
        numerator = numerator/2; //divide num by 2
        denominator = denominator/2; //divide denom by 2
        x = x+1; //add 1 to x
    } while(numerator != denominator){
        //while num and denom arent equal
        if (numerator % 2 == 0){
            //first check to see if num is even
            numerator = numerator/2; //if even divide by 2
        } else if(denominator % 2 == 0){
            //then check to see if denom is even
            denominator = denominator/2; //if even divide by 2
        } else if(numerator>denominator){
            //check to see if num is bigger than denom
            numerator = (numerator-denominator)/2;
            //if greater subtract num from denom and divide by 2
        } else{
            //if none above subtract denom from num and divide by 2
            denominator = (denominator-numerator)/2;
        }
        y = numerator; // set y = to simplified num value
    }
    y = y*(pow(2, x)); //finds the GCF from the num and denom
    //set new y = y*2^x from Binary Method of simplification
    numerator = n; //reset num value to original
    denominator = d; //reset denom value to original
    numerator = numerator/y; //divide num by y
    denominator = denominator/y; //divide denom by y
}
/***
 @name print
 @brief print the reduced value
 @param int numerator, denominator
 @retval none
 */
void Rational::print ()
{ //print the answer to screen
    cout << numerator << "/" << denominator << endl;
    //print the answer to screen after all functions are preformed
}
/***
 @name add
 @brief add the values inputed
 @param int numerator, denominator, n, d
 @retval none
 */
void Rational::add (int n, int d)
{ //add the 2 fractions together
    numerator = (numerator * d) + (n * denominator);
    //need to mulptily each side by denom of other side and then add toghether
    denominator = (denominator * d);
    //multiply the denoms together
    reduce(); //calls reduce to reduce
    print(); //calls print to print
}
/***
 @name sub
 @brief add the values inputed
 @param int numerator, denominator, n, d
 @retval none
 */
void Rational::sub (int n, int d)
{ //subtract the 2 fractions together
    numerator = (numerator * d) - (n * denominator);
    //need to multiply each side by the denom of other side then subtract
    denominator = (denominator * d);
    //multiply the denoms together
    reduce(); //calls reduce to reduce
    print(); //calls print to print
}
/***
 @name mul
 @brief add the values inputed
 @param int numerator, denominator, n, d
 @retval none
 */
void Rational::mul (int n, int d)
{ //multiply the 2 fractions together
    numerator = numerator * n;
    //multipy the numerators
    denominator = denominator * d;
    //multiply the denominators
    reduce(); //calls reduce to reduce
    print(); //calls print to print
}
/***
 @name div
 @brief add the values inputed
 @param int numerator, denominator, n, d
 @retval none
 */
void Rational::div (int n, int d)
{ //divide the 2 fractions together
    numerator = numerator * d;
    //multiply num and denom of other fraction
    denominator = denominator * n;
    //multiply denom and num of other fraction
    reduce(); //calls reduce to reduce
    print(); //calls print to print
}
