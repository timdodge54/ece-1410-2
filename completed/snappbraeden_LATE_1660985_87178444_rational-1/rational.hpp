#ifndef RATIONAL_HPP
#define RATIONAL_HPP


class Rational {
    private:
        int numerator, denominator;
        void reduce ();
        void print ();
    
    public:
        Rational (int,int);
        void add (int n, int d);
        void sub (int n, int d);
        void mul (int n, int d);
        void div (int n, int d);
};



#endif
