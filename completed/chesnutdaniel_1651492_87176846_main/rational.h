#ifndef RATIONAL_H
#define RATIONAL_H


#include <iostream>
#include <string>

using namespace std;

class Rational
{
private: 
	void reduce();
	int iNum, iDenom;

public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void print();
};

#endif
