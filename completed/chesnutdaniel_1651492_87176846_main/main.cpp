/*********************************************************************
* @name main
* @brief Prints menu options for user, scans input and calls         /
*		 functions to perform arithmetic operations with	         /
*		 rational numbers.
* @param none
* @retval  none
*********************************************************************/

#include "rational.h"

int main()
{   //begin

	char select = 49;
	int num, denom;

	//prompt user for numerator and denominator
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> denom;

	//set math object
	Rational Math(num, denom);

	Math.print();

	//loop for menu, and calculations
	while (select != 48)
	{
		//menu options for user selection
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;

		//prompt user for selection, get selection
		cout << "Enter selection: ";
		cin >> select;

		//exception handling
		try
		{
			//check for selection: 0
			if (select == 48) 
			{
				throw 1;
			}

			//check for selection > 4
			if (select > 52 || select < 48)
			{
				throw 2;
			}
		}

		//catch error
		catch (int e)
		{
			//exit program if selection is 0
			if (e == 1)
			{
				return 0;
			}
			//else selection is not valid
			else
			{
				cout << "'" << select << "' is not a valid option!" << endl << endl;
			}
		}

		//if selection is less than 5, continue
		if (select < 53)
		{
			//prompt user for second rational
			cout << "Enter numerator: ";
			cin >> num;
			cout << "Enter denominator: ";
			cin >> denom;

			//switch options for calculation based off of user selection
			switch (select)
			{
			case 49:							//case 1, addition
				Math.add(num, denom);
				break;
			case 50:							//case 2, subtraction
				Math.sub(num, denom);
				break;
			case 51:							//case 3, multiplication
				Math.mul(num, denom);
				break;
			case 52:							//case 4, division
				Math.div(num, denom);
				break;
			}
		}
	}
	return 0;

}	//end