
#include "rational.h"

/*********************************************************************
* @name Rational
* @brief Constructor to initialize the numerator and denominator
* @param values for n and d
* @retval  none
*********************************************************************/

Rational::Rational(int n, int d)
{
	//initialize
	iNum = n;
	iDenom = d;
}

/*********************************************************************
* @name reduce
* @brief Simplifies rationals through evaluating the GCD
* @param none
* @retval  none
*********************************************************************/

void Rational::reduce()
{
	int i;
	bool flag;

	//check for whether denominator is bigger
	if (iNum < iDenom)
	{
		//if true, set i = numerator
		i = iNum;
	}
	//else if numerator is bigger
	else
	{
		//set i = denominator
		i =iDenom;
	}

	//initialize flag to false, and loop while flag is false
	flag = false;
	while (!flag)
	{
		//if both numerator and denominator have no remainders
		if ((iNum % i == 0) && (iDenom % i == 0))
		{
			//set flag to true and break loop
			flag = true;
		}
		//else, decrement
		else
		{
			i--;
		}
	}
	//divide numerator and denominators by i, and initialize variables
	iNum /= i;
	iDenom /= i;
}

/*********************************************************************
* @name print
* @brief Reduces the input numbers, and prints to screen
* @param none
* @retval  none
*********************************************************************/

void Rational::print()
{
	//reduce numerator and denominator, then print to screen
	reduce();
	cout << "Fraction reduces to " << iNum << "/" << iDenom << endl << endl;
}

/*********************************************************************
* @name add
* @brief performs addition of rational numbers
* @param values for n, and d
* @retval  none
*********************************************************************/

void Rational::add(int n, int d)
{
	int num1, num2, nNum, nDenom;

	//calculations for addition
	num1 = iNum * d;
	num2 = n * iDenom;
	nDenom = iDenom * d;
	nNum = num1 + num2;

	//reset initial variables
	iNum = nNum;
	iDenom = nDenom;

	//print results to screen
	reduce();
	cout << iNum << "/" << iDenom << endl << endl;
}

/*********************************************************************
* @name sub
* @brief Performs subtration of rational numbers
* @param values for n and d
* @retval  none
*********************************************************************/

void Rational::sub(int n, int d)
{
	int num1, num2, nNum, nDenom;
	//calculations for subtraction
	num1 = iNum * d;
	num2 = n * iDenom;
	nDenom = iDenom * d;
	nNum = num1 - num2;

	//reset initial variables
	iNum = nNum;
	iDenom = nDenom;

	//print results to screen
	reduce();
	cout << iNum << "/" << iDenom << endl << endl;
}

/*********************************************************************
* @name mul
* @brief Performs multiplication of rational numbers
* @param values for n and d
* @retval  none
*********************************************************************/

void Rational::mul(int n, int d)
{
	int nNum, nDenom;

	//calculations for multiplication
	nNum = iNum * n;
	nDenom = iDenom * d;

	//reset initial variables
	iNum = nNum;
	iDenom = nDenom;

	//print results to screen
	reduce();
	cout << iNum << "/" << iDenom << endl << endl;
}

/*********************************************************************
* @name div
* @brief Performs division of rational numbers
* @param values for n and d
* @retval  none
*********************************************************************/

void Rational::div(int n, int d)
{
	int nNum, nDenom;

	//calculations for multiplication
	nNum = iNum * d;
	nDenom = iDenom * n;

	//reset initial variables
	iNum = nNum;
	iDenom = nDenom;

	//print results to screen
	reduce();
	cout << iNum << "/" << iDenom << endl << endl;
}