
#include "Rational.h"

/*************
@name reduce
@brief reduces a rational to its simplest form
@param none
@retval none
**************/
void Rational::reduce(){
	int x =100000000;
//count down from 100000000
	do {
//when n and d are divisible by x
		if((cd%x)==0 && (cn%x)==0){
//divide both by x
			cd=cd/x;
			cn=cn/x;
		}
		x--;
	}
		while(x>0);
}

/*************
@name add
@brief add two rationals
@param two ints
@retval none
**************/
void Rational::add(int n, int d){
	int tempn, tempd;
//cross multiply
	tempn = cn;
	tempd = cd;
	cn = cn*d;
	cd = cd*d;
	n = tempd*n;
	d = tempd*d;
//add
	cn = cn+n;
//reduce
	Rational::reduce();
}

/*************
@name sub
@brief subtract two rationals
@param two ints
@retval none
**************/
void Rational::sub(int n, int d){
	int tempn, tempd;
//cross multiply
	tempn = cn;
	tempd = cd;
	cn = cn*d;
	cd = cd*d;
	n = tempd*n;
	d = tempd*d;
//subtract
	cn = cn-n;
//reduce
	Rational::reduce();
}

/*************
@name mul
@brief multiply two rationals
@param two ints
@retval none
**************/
void Rational::mul(int n, int d){
//multiply denominators and numerators
	cn = cn*n;
	cd = cd*d;
//reduce
	Rational::reduce();
}

/*************
@name div
@brief divide two rationals
@param two ints
@retval none
**************/
void Rational::div(int n, int d){
//multiply by reciprocal
	cn = cn*d;
	cd = cd*n;
//reduce
	Rational::reduce();
}