
#include "rational.h"


using namespace std;
/*************
@name main
@brief performs basic arithmetic on rational fractions
@param none
@retval none
**************/
int main (void){
	int ind, inn, sel;
	Rational x;
//get starting fraction
	cout << "Enter a numerator:";
	cin >> x.cn;
	cout << "Enter a denominator:" ;
	cin >> x.cd;
//reduce
	x.reduce();
	cout<< "fraction reduces to "<< x.cn << "/" << x.cd << "\n\n\n";
	sel=1;
	while (sel!=0){
//open menu
		cout <<"1. Add a rational\n";
		cout <<"2. Subtract a rational\n";
		cout <<"3. Multiply by a rational\n";
		cout <<"4. Divide by a rational\n";
		cout <<"0. Exit\n";
//get selection
		cout <<"Enter selection: ";
		cin >> sel;
		
		switch (sel){
			case 1:
			//get second fraction
				cout << "Enter numerator: ";
				cin >> inn;
				cout << "Enter denominator: ";
				cin >> ind;
			//add fractions
				x.add(inn,ind);
			//print  result
				cout << x.cn << "/" << x.cd <<"\n\n\n";
				break;
				
			case 2:
			//get second fraction
				cout << "Enter numerator: ";
				cin >> inn;
				cout << "Enter denominator: ";
				cin >> ind;
			//subtract fractions
				x.sub(inn,ind);
			//print  result
				cout << x.cn << "/" << x.cd <<"\n\n\n";
				break;
				
			case 3:
			//get second fraction
				cout << "Enter numerator: ";
				cin >> inn;
				cout << "Enter denominator: ";
				cin >> ind;
			//multiply fractions
				x.mul(inn,ind);
			//print  result
				cout << x.cn << "/" << x.cd <<"\n\n\n";
				break;
				
			case 4:
			//get second fraction
				cout << "Enter numerator: ";
				cin >> inn;
				cout << "Enter denominator: ";
				cin >> ind;
			//divide fractions
				x.div(inn,ind);
			//print  result
				cout << x.cn << "/" << x.cd <<"\n\n\n";
				break;
			//exit
			case 0:
				break;
			//check for other inputs
			default:
				cout << "Invalid selection\n\n\n";
		
		}
	}
}
