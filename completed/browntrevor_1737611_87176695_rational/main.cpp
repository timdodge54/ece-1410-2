#include <iostream>
#include <string>
#include "rational.h"

using namespace std;

int main(void)
{
		//defining variables
	int select, n_in, d_in, a, b;
	get_n_d(n_in, d_in);

		//send to private variables
	Rational rat(n_in, d_in);
	bool good_input = false;
	bool exit = false;

	while (!good_input && !exit)
	{
				//giving them options for selection
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
				//read in number from input
		string str_in;
		cin >> str_in;
				//checking the viablility
		try
		{
			select = stoi(str_in);	//turns string into num and throw
									//invalid inputs
				
			if (select < 0 || select > 4)
			{
					//if select is not a valid number
				cout << "invalid selection: " << select << "  (must be between 0 and 4 inclusive)" << endl;
			}
			else
			{
				if (select == 0)
				{		//quit loop if they entered 0
					exit = true;
					break;
				}

				get_n_d(a, b);
					//switch to access math functions
				switch (select)
				{
				case 1:
					rat.addfrac(a, b);
					break;
				case 2:
					rat.subfrac(a, b);
					break;
				case 3:
					rat.mulfrac(a, b);
					break;
				case 4:
					rat.divfrac(a, b);
					break;
				}
				rat.printfrac();
			}
		}
		catch (invalid_argument const& ex)	//catch statements from stoi
		{
			cout << "invalid text: " << str_in << " " << ex.what() << endl;
		}
		catch (out_of_range const& ex)
		{
			cout << "invalid text: " << str_in << " " << ex.what() << endl;
		}
	}

	return 0;
}