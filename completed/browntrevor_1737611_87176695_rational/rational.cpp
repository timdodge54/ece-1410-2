#include <iostream>
#include <string>
#include "rational.h"

using namespace std;

/****************************************************
*	@name		Rational
*	@brief		send numbers to private data
*	@param		numerator input, denominator input
*	@retval		none
****************************************************/
Rational::Rational(int n_in, int d_in)
{
	n = n_in;	//get n and d into class
	d = d_in;
}

/****************************************************
*	@name		addfrac
*	@brief		add fractions
*	@param		new numerator, new denominator
*	@retval		combined numerator and denominator
****************************************************/
void Rational::addfrac(int a, int b)
{
	n *= b;		//creat common denominators
	a *= d;
	d *= b;
	n += a;		//add numerators

	Rational::reduce();
	cout << "sum is " << n << "/" << d << endl;
}

/****************************************************
*	@name		subfrac
*	@brief		subtract fractions
*	@param		new numerator, new denominator
*	@retval		combined numerator and denominator
****************************************************/
void Rational::subfrac(int a, int b)
{
	n *= b;		//creat common denominators
	a *= d;
	d *= b;
	n -= a;		//subtract numerators

	Rational::reduce();
}

/****************************************************
*	@name		mulfrac
*	@brief		multiply fractions
*	@param		new numerator, new denominator
*	@retval		combined numerator and denominator
****************************************************/
void Rational::mulfrac(int a, int b)
{
	n *= a;		//multiply straight across
	d *= b;

	Rational::reduce();
}

/****************************************************
*	@name		divfrac
*	@brief		divide fractions
*	@param		new numerator, new denominator
*	@retval		combined numerator and denominator
****************************************************/
void Rational::divfrac(int a, int b)
{
	n *= b;		//cross multiply
	d *= a;

	Rational::reduce();
}

/****************************************************
*	@name		reduce
*	@brief		reduce fractions
*	@param		none
*	@retval		reduced numerator and denominator
****************************************************/
void Rational::reduce()
{
	//check if both are negative, then make positive
	if (n < 0 && d < 0)
	{
		n *= -1;
		d *= -1;
	}
	else if (n >= 0 && d < 0)	//if only d is negative, switch n and d
	{
		n *= -1;
		d *= -1;
	}

	for (int i = 2; i <= d; i++)	//loop until i is greater than denomiator
	{
		if ((n % i == 0) && (d % i == 0))	//if they share a common factor
		{
			n = n / i;						//divide both by factor
			d = d / i;
			i = 1;						//reset to lowest common factor
		}
	}
}

/****************************************************
*	@name		printfrac
*	@brief		print fraction
*	@param		none
*	@retval		'n'/'d'
****************************************************/
void Rational::printfrac()
{		//prints...
	cout << n << "/" << d << endl << endl;
}

/****************************************************
*	@name		get_n_d
*	@brief		recieve valid numerator and denominator
*	@param		numerator and denominator by reference
*	@retval		valid numerator and denominator
****************************************************/
void get_n_d(int& n, int& d)
{			//assign more variables
	bool do_numerator = true;
	bool do_denominator = true;
	string prompt = "enter numerator: ";	//define the string like this
											//so it can be changed
	bool loop = true;

				////while both do not have valid input
	while (do_numerator || do_denominator)
	{
		bool good_val = false;
		while (!good_val)	//if we don't have a good number yet
		{
			cout << prompt;	//ui to get number
			string str_in;
			cin >> str_in;
			int val;
			try
			{
							//turn string input into int, throw invalid cases
				val = stoi(str_in);
				if (do_numerator)
				{
					n = val;
					do_numerator = false;	//switch flags and ui prompt
					good_val = true;
					prompt = "enter denominator: ";
				}
				else if (do_denominator)
				{
					if (val == 0)
					{		//check if 0 cause not valid only on denominator
						cout << "0 is an invalid input" << endl;
					}
					else
					{
						d = val;		//switch flags and exit ui loop
						do_denominator = false;
						good_val = true;
					}
				}
			}
			catch (invalid_argument const& ex)	//catches throws from stoi
			{
				cout << "invalid text: " << str_in << " " << ex.what() << endl;
			}
			catch (out_of_range const& ex)
			{
				cout << "invalid text: " << str_in << " " << ex.what() << endl;
			}
		}
	}
}