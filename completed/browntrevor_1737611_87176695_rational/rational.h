#pragma once
void get_n_d(int& n, int& d);

class Rational
{
	int n, d;
	void reduce();
public:
	Rational(int n_in, int d_in);
	void addfrac(int a, int b);
	void subfrac(int a, int b);
	void mulfrac(int a, int b);
	void divfrac(int a, int b);
	void printfrac();
};