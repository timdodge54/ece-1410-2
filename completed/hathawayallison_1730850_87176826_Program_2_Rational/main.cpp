/*****************************************************************************
* @name main
* @brief does math operations with rational numbers
* @param none
* @retval none
*****************************************************************************/
#include "rational1.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(void)
{
	//begin
	int numerator, denominator, n, d, selection = 5;
	bool flag;

	cout << "Enter numerator: ";
	//prompt user to enter numerator
	cin >> numerator;
	//get numerator
	cout << "Enter denominator: ";
	//prompt user to enter denominator
	cin >> denominator;
	//get denominator
	Rational rat(numerator, denominator);
	cout << "Fraction reduces to " << rat.get_n() << "/" << rat.get_d() << endl << endl << endl;
	//reduce fraction and print to screen

	while (selection != 0)
	//while selection is not 0
	{
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		//menu options

		cout << "Enter selection: ";
		//prompt user to enter selection
		cin >> selection;
		//get selection

		if (selection == 1)
		//if selection is 1
		{
			cout << "Enter numerator: ";
			//prompt user to enter numerator
			cin >> n;
			//get numerator
			cout << "Enter denominator: ";
			//prompt user to enter denominator
			cin >> d;
			//get denominator
			rat.add(n, d);
			//add and reduce
		}
		if (selection == 2)
		//if selection is 2
		{
			cout << "Enter numerator: ";
			//prompt user to enter numerator
			cin >> n;
			//get numerator
			cout << "Enter denominator: ";
			//prompt user to enter denominator
			cin >> d;
			//get denominator
			rat.sub(n, d);
			//subtract and reduce
		}
		if (selection == 3)
		//if selection is 3
		{
			cout << "Enter numerator: ";
			//prompt user to enter numerator
			cin >> n;
			//get numerator
			cout << "Enter denominator: ";
			//prompt user to enter denominator
			cin >> d;
			//get denominator
			rat.mul(n, d);
			//multiply and reduce
		}
		if (selection == 4)
		//if selection is 4
		{
			cout << "Enter numerator: ";
			//prompt user to enter numerator
			cin >> n;
			//get numerator
			cout << "Enter denominator: ";
			//prompt user to enter denominator
			cin >> d;
			//get denominator
			rat.div(n, d);
			//divide and reduce
		}
		if (selection == 0)
		//if selection is 0
		{
			return 0;
			//quit program
		}
		try
		//try
		{
			if (selection > 4)
			//if selection is more than 4
			{
				throw 1;
				//throw 1
			}
		}
		catch (int exception)
		//cath exception
		{
			if (exception == 1)
			//if exception is 1
			{
				cout << "'" << selection << "' is not a valid option!" << endl << endl << endl;
				//print error message to screen
				selection = 5;
				//set selection equal to 5
			}
		}
	}

	return 0;
}
//end