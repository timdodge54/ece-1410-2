#ifndef RATIONAL1_H
#define RATIONAL1_H

class Rational
{
	public:
		Rational(int n, int d)
		{
			numerator = n;
			denominator = d;
			reduce();
		}
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void print();
		int get_d()
		{
			return denominator;
		}
		int get_n()
		{
			return numerator;
		}

	private:
		void reduce();
		int numerator, denominator;
};

#endif 