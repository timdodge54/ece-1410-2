#include <iostream>
#include "rational1.h"
using namespace std;

void Rational::add(int n, int d)
{ //begin
	numerator = (numerator * d) + (n * denominator);
	//add numerators
	denominator *= d;
	//get common denominator
	reduce();
	//reduce
	print();
	//print
} //end

void Rational::sub(int n, int d)
{ //begin
	numerator = (numerator * d) - (n * denominator);
	//subtract numerators
	denominator *= d;
	//get common denominator
	reduce();
	//reduce
	print();
	//print
} //end

void Rational::mul(int n, int d)
{ //begin
	numerator *= n;
	//multiply numerators
	denominator *= d;
	//get common denominator
	reduce();
	//reduce
	print();
	//print
} //end

void Rational::div(int n, int d)
{ //begin
	numerator *= d;
	//multiply numerator and denominator
	denominator *= n;
	//multiply denominator and numerator
	reduce();
	//reduce
	print();
	//print
} //end

void Rational::reduce()
{ //begin
	int i;
	for (i = 1; i <= denominator; i++)
	{
		if (denominator % i == 0 && numerator % i == 0)
		//if denominator % i is 0 and numerator % i is 0
		{
			denominator /= i;
			//denominator /= i
			numerator /= i;
			//numerator /= i
			i = 1;
			//reset i to 1
		}
	}
} //end

void Rational::print()
{ //begin
	cout << numerator << "/" << denominator << endl << endl;
	//print
} //end