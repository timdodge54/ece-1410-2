#ifndef rational
#define rational

#include <iostream>
class Rational
{
	int numerator, denominator;
	public:
	Rational(int, int);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
};

#endif