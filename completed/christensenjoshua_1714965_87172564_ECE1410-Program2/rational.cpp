
#include "rational.h"
void Reduce(int& n, int&d);

/**
@name Rational::Rational
@brief Sets initial values for class Rational and reduces
@param a, b
@retval none
**/ 
Rational::Rational(int a, int b)
{
	numerator = a;
	denominator = b;
	Reduce(numerator, denominator);
}

/**
@name Rational::add
@brief adds 2 fractions together and calls the reduce function
@param n, d
@retval none
**/ 

void Rational::add(int n, int d)
{
	numerator = (numerator * d) + (n * denominator);
	denominator *= d;
	Reduce(numerator, denominator);
}

/**
@name Rational::sub
@brief subtracts 2 fractions together and calls the reduce function
@param n, d
@retval none
**/ 

void Rational::sub(int n, int d)
{
	numerator = (numerator * d) - (n * denominator);
	denominator *= d;
	Reduce(numerator, denominator);
}

/**
@name Rational::mul
@brief multiplies 2 fractions together and calls the reduce function
@param n, d
@retval none
**/ 

void Rational::mul(int n, int d)
{
	numerator *= n;
	denominator *= d;
	Reduce(numerator, denominator);
}

/**
@name Rational::div
@brief divides 2 fractions together and calls the reduce function
@param n, d
@retval none
**/ 

void Rational::div(int n, int d)
{
	numerator *= d;
	denominator *= n;
	Reduce(numerator, denominator);
}

/**
@name Reduce
@brief Reduces given fraction by finding Greatest Common Factor
@param n, d
@retval none
**/ 

void Reduce(int& n, int&d)
{	// Begin
	int i, negative = 1, flag = 0;
		// if numerator is negative, flip sign of variable
	if (n < 0)
	{
		n *= -1;
		negative *= -1;
	}
		// if denominator is negative, flip sign of variable
	if (d < 0)
	{
		d *= -1;
		negative *= -1;
	}	// if numerator is smaller, set i to n
	if (n < d)
	{
		i = n;
	}	// else set i to d
	else
	{
		i = d;
	}	// loop until greatest common factor is found
	while (flag == 0)
	{	// if both numerator and denominator are divisible by i
		if ((n % i) == 0 && (d % i) == 0)
		{	// then raise the flag
			flag++;
		}	// else decrease i by 1
		else
		{
			i--;
		}
	}	// divide fraction by greatest common factor
	n /= i;
	d /= i;
		// multiply by negative
	n *= negative;

		// print fraction
	std::cout << n << "/" << d << std::endl << std::endl << std::endl;
}