
/**
@name main
@brief performs arithmetic on fractions
@param none 
@retval  none 
**/ 

#include "rational.h"
using namespace std;

int main(void)
{											// Begin
	int numerator, denominator, input, flag;		// Recieve initial inputs
	cout << "Enter numerator: ";
	cin >> numerator;
	cout << "Enter denominator: ";
	cin >> denominator;
	cout << "fraction reduces to ";
	Rational frac (numerator, denominator);	// Create fraction
	
	while (flag != 1)						// Loop until flag is raised
	{										// Print selection menu
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> input;						// Get input
		try
		{
			if (input < 0 || input > 4)		// if invalid, throw
			{
				throw input;
			}
			else
			{
				switch(input)				// Else, switch inputs		
				{
					case 4:					// if 4, divide by new fraction
						cout << "Enter numerator: ";
						cin >> numerator;
						cout << "Enter denominator: ";
						cin >> denominator;
						frac.div(numerator, denominator);
					break;
					case 3:					// if 3, multiply by new fraction
						cout << "Enter numerator: ";
						cin >> numerator;
						cout << "Enter denominator: ";
						cin >> denominator;
						frac.mul(numerator, denominator);
					break;
					case 2:					// if 2, subtract by new fraction
						cout << "Enter numerator: ";
						cin >> numerator;
						cout << "Enter denominator: ";
						cin >> denominator;
						frac.sub(numerator, denominator);
					break;
					case 1:					// if 1, add by new fraction
						cout << "Enter numerator: ";
						cin >> numerator;
						cout << "Enter denominator: ";
						cin >> denominator;
						frac.add(numerator, denominator);
					break;
					case 0:					// if 0, increase flag
						flag++;
					break;
				}
			}
		}
		catch (int i)						// print if invalid
		{
			cout << i << " is an invalid input" << endl << endl << endl;
		}
	}
	return 0;								// end
}
