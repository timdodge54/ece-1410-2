#include <iostream>
#include "rational.h"
#include <algorithm>
using namespace std;

/******************************************************************************
* @name rational
* @brief constructor that declares the numerator and denominator.
*           it also reduces them
* @param none
* @retval none
******************************************************************************/
Rational::Rational(int n, int d)
{
    numerator = n;
    denominator = d;
    reduce();
}

/******************************************************************************
* @name add
* @brief adds the new rational and old rational together and reduces them
* @param none
* @retval none
******************************************************************************/
void Rational::add(int n, int d)
{
    numerator = numerator * d + denominator * n;
    denominator *= d;
    reduce();
}

/******************************************************************************
* @name sub
* @brief subtracts the new rational and old rational together and reduces them
* @param none
* @retval none
******************************************************************************/
void Rational::sub(int n, int d)
{
    numerator = numerator * d - denominator * n;
    denominator *= d;
    reduce();
}

/******************************************************************************
* @name mul
* @brief multiplies the new new rational and old rational 
            together and reduces them
* @param none
* @retval none
******************************************************************************/
void Rational::mul(int n, int d)
{
    numerator *= n;
    denominator *= d;
    reduce();
}

/******************************************************************************
* @name main
* @brief performs arithmetic on fractions that are supplied by the user
* @param none
* @retval none
******************************************************************************/
void Rational::div(int n, int d)
{
    numerator *= d;
    denominator *= n;
    reduce();
}

/******************************************************************************
* @name reduce
* @brief reduces a rational after arithmetic is performed on it
* @param none
* @retval none
******************************************************************************/
void Rational::reduce()
{
    int divisor = min(numerator, denominator);
    while (divisor > 1) {
        if (numerator % divisor == 0 && denominator % divisor == 0) {
            numerator /= divisor;
            denominator /= divisor;
            break;
        }
        divisor--;
    }
}

/******************************************************************************
* @name print
* @brief prints the new rational to screen
* @param none
* @retval none
******************************************************************************/
void Rational::print()
{
    cout << numerator << "/" << denominator << endl;
}
