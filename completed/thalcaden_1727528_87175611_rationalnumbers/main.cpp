/******************************************************************************
* @name main
* @brief performs arithmetic on fractions that are supplied by the user
* @param none
* @retval none
******************************************************************************/

#include <iostream>
#include "rational.h"
using namespace std;

int main() 
{
    //begin

    //declare variables
    int num, den;
    //ask user for numerator for the first fraction
    cout << "Enter numerator: ";
    //get numerator for the first fraction
    cin >> num;
    //ask user for denominator for the first fraction
    cout << "Enter denominator : ";
    //get denominator for the first fraction
    cin >> den;
    //call the rational class
    Rational r(num, den);
    //reduce the first fraction, if applicable
    cout << "Fraction reduces to ";
    r.print();
    cout << endl;
    cout << endl;
    
    int choice;
    //do while Loop
    do 
    {
        //print the menu to screen
        cout << "1. Add a rational" << endl;
        cout << "2. Subtract a rational" << endl;
        cout << "3. Multiply by a rational" << endl;
        cout << "4. Divide by a rational" << endl;
        cout << "0. Exit" << endl;
        cout << "Enter selection: ";

        //get choice from user
        cin >> choice;

        //try function
        try 
        {
            //if choice equals 1
            if (choice == 1) 
            {
                //ask user for numerator
                cout << "Enter numerator: ";
                //get numerator
                cin >> num;
                //check if numerator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //ask user for denominator
                cout <<  "Enter denominator: ";
                //get denominator
                cin >> den;
                //check if denominator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }                
                //call the addition function
                r.add(num, den);
                //print the new rational to screen
                r.print();
                cout << endl;
                cout << endl;
            }
            //if choice equals 2
            else if (choice == 2) 
            {
                //ask user for numerator
                cout << "Enter numerator: ";
                //get numerator
                cin >> num;
                //check if numerator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //ask user for denominator
                cout << "Enter denominator: ";
                //get denominator
                cin >> den;
                //check if denominator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //call the subtraction function
                r.sub(num, den);
                //print the new rational to the screen
                r.print();
                cout << endl;
                cout << endl;
            }
            //if choice equals 3
            else if (choice == 3) 
            {
                //ask user for numerator
                cout << "Enter numerator: ";
                //get numerator
                cin >> num;
                //check if numerator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //ask user for denominator
                cout << "Enter denominator: ";
                //get denominator
                cin >> den;
                //check if denominator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //call the multiplication function
                r.mul(num, den);
                //print the new rational to screen
                r.print();
                cout << endl;
                cout << endl;
            }
            //if choice equals 4
            else if (choice == 4) 
            {
                //ask user for numerator
                cout << "Enter numerator: ";
                //get numerator
                cin >> num;
                //check if numerator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
                //ask user for denominator
                cout << "Enter denominator: ";
                //get denominator
                cin >> den;
                //check if denominator is a valid option
                if (cin.fail())
                {
                    throw "Invalid input. Please enter a valid option.\n\n";
                }
            
                //call the division function
                r.div(num, den);
                //print the new rational to screen
                r.print();
                cout << endl;
                cout << endl;
            }
            //if choice does not equal 0, 1, 2, 3, or 4
            else if (choice != 0)
            {
                //print that that is not a valid selection 
                //prompt user for a new, valid selection
                throw "Invalid input. Please enter a valid option.\n\n";
            }
        }
        //catch funciton
        catch (const char *e ) 
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cerr << e << endl;
            continue;
        }
    } 
    //while choice does not equal 0
    while (choice != 0);

    //end
    return 0;
}
