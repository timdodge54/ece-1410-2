/******************************************************************************
* @name		main
* @brief	calls the "rational" function to calculate the reduced form of a
			fraction that is specified by the user after a mathematical
			operation is performed on the provided fraction, selected
			through the usage of a selection menu.
* @param	none
* @retval	none
******************************************************************************/

#include <iomanip>
#include <cmath>
#include "rational.h"

int main()
{
	int n, d; //initialize local variables n and d as standins for private vars
	char sel = 1; //initialize selection to 1 to allow for initialized memory
	Rational r(n, d); //create class Rational named r

	cout << "Enter numerator: "; //prompt for original numerator
	cin >> n; //get value for numerator
	cout << "Enter denominator: "; //prompt for original denominator
	cin >> d; //get valule for numerator
	r.Rational::Rational(n, d); /*pass numerator and denominator values into
	Rational class*/

	while (sel != 0) //loop while user does not enter selection to exit
	{
		/* The following 6 lines of code provide the user with selections
		on what operation to perform, as well as prompting for a selection*/
		cout << "1. Add a rational" << endl; 
		cout << "2. Suubtract a rational" << endl;
		cout << "3. Mulitply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> sel; //take in value for selection, represented as a char
		
		try
		{
			/*the following if statements residing within the try block
			are meant as corresponding actions for the catch block to 
			perform, given a specific thrown value */
			if (sel == 1)
			{
				throw 1;
			}

			if (sel == 2)
			{
				throw 2;
			}

			if (sel == 3)
			{
				throw 3;
			}

			if (sel == 4)
			{
				throw 4;
			}

			if (sel == 0)
			{
				throw 0;
			}
			else
			{
				throw 5; //5 is thrown if user enters an invalid selection
			}
		}
		catch (char e) //the value thrown is caught and stored in char e
		{
			if (e == 1)
			{
				/*given that the first selection is chosen, code is executed
				to take in a new numerator and denominator to add to the 
				current fractional, and print the reduced result*/
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				r.add(n, d);
				r.print();
			}

			if (e == 2)
			{
				/*given that the second selection is chosen, code is executed
				to take in a new numerator and denominator to subtract from the
				current fractional, and print the reduced result*/
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				r.sub(n, d);
				r.print();

			}

			if (e == 3)
			{
				/*given that the third selection is chosen, code is executed
				to take in a new numerator and denominator to multiply to the
				current fractional, and print the reduced result*/
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				r.mul(n, d);
				r.print();
			}

			if (e == 4)
			{
				/*given that the fourth selection is chosen, code is executed
				to take in a new numerator and denominator to divide the
				current fractional, and print the reduced result*/
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				r.div(n, d);
				r.print();
			}

			if (e == 0)
			{
				/*given that the fifth option - 0 - is selected, the program
				will quit as stated to the user at the beginning of the 
				program execution*/
				return 0;
			}

			if (e == 5)
			{
				/*given that a user tries to enter an invalid selection
				from the list provided, an error statement is printed,
				and the loop continues until a valid selection is either
				made or found within what is entered by the user*/
				cout << sel << " is not a valid selection!" << endl;
			}
		}
	}
	return 0;
}