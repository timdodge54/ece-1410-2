/******************************************************************************
* Class Rational
*
* Summary: 
* A class for working with rational numbers represented as fractions.
* Methods exist for adding, subtracting, multiplying, dividing, reducing,
* and printing fractions.
******************************************************************************/

#ifndef RATIONAL_H
#define RATIONAL_H
#include <iostream>
using namespace std;

class Rational {
public:
	Rational(int n, int d); //constructor used to in main to call public funcs
	void add(int n, int d); //adder function
	void sub(int n, int d); //subtraction function
	void mul(int n, int d); //multiplication function
	void div(int n, int d); //division function
	void print(); //printing function
private:
	void reduce(); //reduction function to simplify fractional
	int numerator, denominator; //private variables to store init. num and den
};

#endif