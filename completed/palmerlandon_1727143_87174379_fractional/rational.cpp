#include "rational.h"

Rational::Rational(int n, int d)
{//numerator is given the first num value provided by the user
 //denominator is given the first denom value provided by the user
	numerator = n;
	denominator = d;
}

void Rational::add(int n, int d)
{/*addition is performed on the original fraction with the
 new fraction provided by the user. The fraction is then
	reduced to its simplest form*/
	numerator = n*denominator + numerator*d;
	denominator = d*denominator;
	reduce();
}

void Rational::sub(int n, int d)
{
	/*subtraction is performed on the original fraction with the
 new fraction provided by the user. The fraction is then
	reduced to its simplest form*/
	numerator = n*denominator - numerator*d;
	denominator = d*denominator;
	reduce();
}

void Rational::mul(int n, int d)
{
	/*mulitplication is performed on the original fraction with the
 new fraction provided by the user. The fraction is then
	reduced to its simplest form*/
	numerator *= n;
	denominator *= d;
	reduce();
}

void Rational::div(int n, int d)
{
	/*division is performed on the original fraction with the
 new fraction provided by the user. The fraction is then
	reduced to its simplest form*/
	numerator /= n;
	denominator /= d;
	reduce();
}

void Rational::print()
{/*this is a function to print the new reduced fuction that
has a mathematical operation performed on it*/
	cout << numerator << "/" << denominator << std::endl;
}

void Rational::reduce()
{
	/*The following lines of code are provided to reduce
	the fractional to its simplest form through the usage
	of a GCF function. Local "dummy" variables are used
	to protect the actual values of the numerator and denominator*/
	int a, b, c, g;
	a = 0;
	b = 0;
	g = 0;
	c = 0;

	while (a % 2 == 0 && b % 2 == 0)
	{
		//check if both a and b are even
		//if both are even, divide them both by 2 and incriment c by 1
		a = a / 2;
		b = b / 2;
		c++;
	}	
	while (a != b)
	{
		//while a and b are not equal, check if b and a are even
		if (a % 2 == 0)
		{
			//divide a by 2 if it is even
			a = a / 2;
		}

		if (b % 2 == 0)
		{
			//divide b by 2 if it is even
			b = b / 2;
		}

		if (a > b)
		{
			//if a > b, subtract b from a and divide result by 2,
			//and assign to a.
			a = (a - b) / 2;
		}

		if (b > a)
		{
			//if b > a, subtract a from b and divide the result by 2,
			//and assign to b.
			b = (b - a) / 2;

		}
	}
	//set a third variable, g, to the value of a.
	//This will allow a to be used in providing a GCF to divide by
	//without altering a outside the scope of the function,
	//and thereby affecting other operations performed while the program runs.
	g = a;
	//divide the current numerator by the newfound GCF
	numerator = numerator / (g * pow(2, c));
	//divide the current denominator by the newfound GCF
	denominator = denominator / (g * pow(2, c));

}
