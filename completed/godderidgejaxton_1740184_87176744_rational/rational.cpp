/**
@name: rational
@brief: stores two numbers
@param: inputs from user
@retval: none
**/

#include "rational.h"
#include <iostream>
rational::rational(int a, int b)//default constructor function to store fraction
{
	numerator = a;
	denominator = b;
}

/**
@name: add
@brief: adds two fractions together
@param: inputs from user
@retval: none
**/

void rational::add(int a, int b)
{
	if (denominator == b)//if denominators are the same
	{
		numerator = numerator + a;//add numerators
		denominator = denominator;
	}
	else
	{
		int lcm = (denominator * b);//makes the denominators the same
		numerator = ( b * numerator) + (denominator * a);
		denominator = lcm;
	}
}

/**
@name: sub
@brief: subtracts two fractions
@param: inputs from the user
@retval: none
**/

void rational::sub(int a, int b)
{
	if (denominator == b)//if denominators are the same
	{
		numerator = numerator - a;//subtract numerators
		denominator = denominator;
	}
	else
	{
		int lcm = (denominator * b);//makes the denominators the same
		numerator = (b * numerator) + (denominator * a);
		denominator = lcm;
	}
}

/**
@name: mul
@brief: multiplies two fractions together
@param: inputs from the user
@retval: none
**/

void rational::mul(int a, int b)
{
	 numerator = numerator * a;//multiplies the numerators together
	 denominator = denominator * b;//multiplies the denominators together
}

/**
@name: div
@brief: divides two fractions
@param: inputs from the user
@retval: none
**/

void rational::div(int a, int b)
{
	 numerator = numerator * b;//multiplies recipricol of the second fraction
	 denominator = numerator * a;
}

/**
@name: reduce
@brief: reduces the fraction
@param: numbers from the user
@retval: none
**/

void rational::reduce()
{
	
	int a, b;
	a = numerator;
	b = denominator;

	while(a != 0 && b != 0)//while 
	{
		if (a > b)//if numerator > denominator
		{
			a = a % b;//set remainder = a
		}
		else if (b > a)//if denominator > numerator
		{
			b = b % a;//set remainder = b
		}
		else 
		{
			a = 0;//set a = 0
		}
	}

	if (a == 0)//if a is equal to 0
	{
		numerator = numerator / b;
		denominator = denominator / b;
	}
	else if (b == 0)//if b is equal to 0
	{
		numerator = numerator / a;
		denominator = denominator / a;
	}
}

/**
@name: print
@brief: prints numbers in fraction form to screen
@param: inputs from user
@retval: none
**/

void rational::print()
{
	std::cout << numerator << "/" << denominator << std::endl;
	//prints results to the screen
}