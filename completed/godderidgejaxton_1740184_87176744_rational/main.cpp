/**
@name: main
@brief: asks user for numbers of a fraction and what expression
they woud like done
@param: inputs from user
@retval: none
**/

#include "rational.h"
using namespace std;

int main(void)
{
	int a, b, numerator, denominator;
	char  t, input = '1';

	
	cout << "Enter numerator: ";//asks user for numerator
	cin >> numerator;
	cout << "Enter denominator: ";//asks user for denominator
	cin >> denominator;
	rational fract(numerator, denominator);//stores fraction in class
	cout << "Fraction Reduces to: ";
	fract.rational::reduce();
	fract.rational::print();

	try//checks if user enters not a number
	{
		if (numerator > 9)
		{
			throw numerator;
		}
		else if (denominator > 9)
		{
			throw denominator;
		}
	}
	catch (...)
	{
		cout << "Invalid seslection" << endl;
	}

	while (input != '0')
	{
	//displays which expressions are availaable for the user to use
	cout << "1 add a rational" << endl;
	cout << "2 subtract a rational" << endl;
	cout << "3 multiply by a rational" << endl;
	cout << "4 divide by a rational" << endl;
	cout << "0 exit" << endl;
	cout << "Enter selection: ";
	cin >> t;
	cout << endl;

	switch(t)//switch to determine which expression needs to be preformed 
	{
	case '0': break;
	case '1': //addition
		cout << "Enter numerator: ";
		cin >> a;
		cout << endl;
		cout << "Enter denominator: ";
		cin >> b;
		cout << endl;
		fract.rational::add(a, b);//sends ints to add function
		fract.rational::reduce();
		fract.rational::print();
		break;
	case '2': //subtraction
		cout << "Enter numerator: ";
		cin >> a;
		cout << endl;
		cout << "Enter denominator: ";
		 cin >> b;
		 cout << endl;
		fract.rational::sub(a, b);//sends ints to sub function
		fract.rational::reduce();
		fract.rational::print();
		break;
	case '3': //multiplication
		cout << "Enter numerator: ";
		cin >> a;
		cout << endl;
		cout << "Enter denominator: ";
		cin >> b;
		cout << endl;
		fract.rational::mul(a, b);//sends ints to mul function
		fract.rational::reduce();
		fract.rational::print();
		break;
	case '4': //division
		cout << "Enter numerator: ";
		cin >> a;
		cout << endl;
		cout << "Enter denominator: ";
		cin >> b;
		cout << endl;
		fract.rational::div(a, b);//sends ints to div function
		fract.rational::reduce();
		fract.rational::print();
		break;
	default: cout << "Enter a valid command" << endl; break;
	//if anything other than 0-4 is entered asks user to enter a proper command
	}

	}

	cout << "Program has ended" << endl;

	return 0;
}