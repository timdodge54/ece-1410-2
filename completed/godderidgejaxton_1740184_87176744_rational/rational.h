#ifndef RATIONAL_H
#define RATIONAL_H
#endif
#include <iostream>

class rational {
private:
	int numerator, denominator;

public:
	rational(int a, int b);
	void add(int a, int b);
	void sub(int a, int b);
	void mul(int a, int b);
	void div(int a, int b);
	void reduce();
	void print();
};

