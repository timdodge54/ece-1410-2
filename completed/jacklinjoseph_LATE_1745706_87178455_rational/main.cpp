#include "rational.h"
#include <iostream>

using namespace std;
char printmenu(void);

/******************************************************************************
* @name		main
* @brief	Prints a menu to do arithmetic on fractions
* @param	none
* @retval	none
******************************************************************************/

int main(void)
{
	int num = 0, den = 0;
	char sel = '0';
	Rational a;
	//Loops if users entered denominator is 0
	while (den == 0)
	{
		//Asks user for numerator and denominator
		cout << "Enter numerator: ";
		cin >> num;

		cout << "Enter denominator: ";
		cin >> den;

		// Prints error message is denominator is still 0
		if (den == 0)
		{
			cout << den << " is an invalid denominator." << endl << endl << endl;
		}
	}

	//Sets the values of denominator and numerator then prints then.
	a.setValues(num, den);
	cout << "Fraction reduces to : ";
	a.print();
	sel = printmenu();

	//Loops as long as the user didnt select the exit option
	while (sel != '0')
	{
		//test if the user entered a valid selection
		try
		{ 
			if(sel<'0'||sel>'5')
			{
				throw 1;
			}

			//else prompts user to enter a numerator and denominator
			else
			{
				cout << "Enter numerator: ";
				cin >> num;
				cout << "Enter denominator: ";
				cin >> den;
			}

			//tests if the user entered a valid denominator
			if (den == 0)
			{
				throw 2;
			}

			else
			{
				//calls the appropriate function for the users selection
				switch (sel)
				{
				//adds the two fractions then prints the fraction
				case '1':
					a.add(num, den);
					a.print();
					break;
				//subtracts the two fractions then prints the fraction
				case '2':
					a.sub(num, den);
					a.print();
					break;
				//multiplies the two fractions then prints the fraction
				case '3':
					a.mul(num, den);
					a.print();
					break;
				//divides the two fractions then prints the fraction
				case '4':
					a.div(num, den);
					a.print();
					break;
				default:
					break;
				}
			}
		}
		catch(int e)
		{
			switch (e)
			{
			// prints error message if users selection was invalid
			case 1:
				cout << sel << " is an invalid selection." << endl
					<< endl << endl;
				break;
			// prints error message if users denominator was invalid
			case 2:
				cout << den << " is an invalid denominator." << endl
					<< endl << endl;
				break;
			}
		}
		//prints menu for selection
		sel = printmenu();
	}

	//exits program if user selected 0
	return 0;
}
/******************************************************************************
* @name		printmenu
* @brief	prints the menu for selection
* @param	none
* @retval	char
******************************************************************************/
char printmenu(void)
{
	char s;

	cout << "1. Add a rational" << endl << "2. Subtract a rational" << endl;
	cout << "3. Multiply by a rational" << endl << "4. Divide by a rational";
	cout << endl << "0. Exit" << endl << "Enter selection: ";

	cin >> s;

	return s;
}