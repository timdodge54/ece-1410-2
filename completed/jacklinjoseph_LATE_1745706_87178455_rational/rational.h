#ifndef RATIONAL_H
#define RATIONAL_H

class Rational {
	private:
		void reduce(int n, int d);
		int num, den;

	public:

		Rational();

		void setValues(int n, int d);
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void print(void);

};

#endif
