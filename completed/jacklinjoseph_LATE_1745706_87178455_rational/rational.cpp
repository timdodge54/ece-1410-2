#include "rational.h"
#include <iostream>
#define DEBUG

/******************************************************************************
* @name		Rational
* @brief	innitializes fraction to 1/1
* @param	none
* @retval	none
******************************************************************************/
Rational::Rational()
{
	num = 1;
	den = 1;
}
/******************************************************************************
* @name		reduce
* @brief	reduces final fraction to simplist form
* @param	int n and int d
* @retval	none
******************************************************************************/
void Rational::reduce(int n, int d)
{
	int g, f = 0;
	n = abs(n);
	d = abs(d);
	if (n != 0)
	{
		while (((n % 2) == 0) && ((d % 2) == 0))
		{
			n = n / 2;

			d = d / 2;

			f = f++;
		}
		while (n != d)
		{
			if ((n % 2) == 0)
			{
				n = (n / 2);
			}
			else
			{
				if ((d % 2) == 0)
				{
					d = (d / 2);
				}
				else
				{
					if (n > d)
					{
						n = ((n - d) / 2);
					}
					else
					{
						d = ((d - n) / 2);
					}
				}
			}
		}
		g = n;

		num = num / (g * (pow(2, f)));
		den = den / (g * (pow(2, f)));
	}
	else
	{
		num = 0;
		den = 1;
	}
}
/******************************************************************************
* @name		setValues
* @brief	sets the values after user puts them in
* @param	int n int d
* @retval	none
******************************************************************************/
void Rational::setValues(int n, int d)
{
	num = n;
	den = d;
	reduce(num, den);
}
/******************************************************************************
* @name		add
* @brief	adds two fractions
* @param	int n int d
* @retval	none
******************************************************************************/
void Rational::add(int n, int d)
{
	if ((num != 0) && (den != 0))
	{
		num = ((d * num) + (den * n));
		den = (d * den);
	}
	else
	{
		if (num == 0)
		{
			num += n;
			den = d;
		}
	}
	reduce(num, den);
}
/******************************************************************************
* @name		sub
* @brief	subtracts two fractions
* @param	int n, int d
* @retval	none
******************************************************************************/
void Rational::sub(int n, int d)
{
	if ((num != 0) && (den != 0))
	{
		num = ((d * num) - (den * n));
		den = (d * den);
	}
	else
	{
		if (num == 0)
		{
			num -= n;
			den = d;
		}
	}
	reduce(num, den);
}
/******************************************************************************
* @name		mul
* @brief	multiplies two fractions
* @param	int n, int d
* @retval	none
******************************************************************************/
void Rational::mul(int n, int d)
{
	num *= n;
	den *= d;
	reduce(num, den);
}
/******************************************************************************
* @name		div
* @brief	dvides two fractions
* @param	int n, int d
* @retval	none
******************************************************************************/
void Rational::div(int n, int d)
{
	if ((n != 0) && (d != 0))
	{
		num = num * d;
		den = den * n;
		reduce(num, den);
	}
}
/******************************************************************************
* @name		print
* @brief	prints fraction after arithmetic has been preformed
* @param	none
* @retval	none
******************************************************************************/
void Rational::print(void)
{
	std::cout << num << "/" << den << std::endl << std::endl << std:: endl;
}