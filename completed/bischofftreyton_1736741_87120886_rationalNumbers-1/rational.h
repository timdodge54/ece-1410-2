#ifndef _RATNUMBERS_
#define _RATNUMBERS_

class Rational {
	int num, den;
public:
	void set_values(int n, int d);
	void print();
	void reduce();
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
};

int gcd(int n, int d);

#endif