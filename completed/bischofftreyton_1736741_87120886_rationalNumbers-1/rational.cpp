#include <iostream>

#include "rational.h"

/**
* @name set_values
* @brief sets entered fraction numbers into the num and den,
										which is inside the class
* @param numerator and denominator
* @retval none
**/

void Rational::set_values(int n, int d)
{
	// set make class fraction equal to entered fraction
	num = n;
	den = d;
}

/**
* @name print
* @brief prints fraction to the screen
* @param none
* @retval none
**/

void Rational::print()
{
	// prints fraction
	std::cout << num << "/" << den << std::endl;
}

/**
* @name reduce
* @brief reduces the fraction inside the class using its gcd
* @param none
* @retval none
**/

void Rational::reduce() {
	int div = gcd(num, den);
	// reduce fractions using their gcd
	num /= div;
	den /= div;
}

/**
* @name add
* @brief adds two fractions
* @param new fraction
* @retval none
**/

void Rational::add(int n, int d)
{
	// add fraction
	num = (num * d) + (n * den);
	den = den * d;
}

/**
* @name sub
* @brief subtracts two fractions
* @param new fraction
* @retval none
**/

void Rational::sub(int n, int d)
{
	// subtract fractions
	num = (num * d) - (n * den);
	den = den * d;
}

/**
* @name mul
* @brief multiplies two fractions
* @param new fraction
* @retval none
**/

void Rational::mul(int n, int d)
{
	// multiply fractions
	num *= n;
	den *= d;
}

/**
* @name div
* @brief divides two fractions
* @param new fraction
* @retval none
**/

void Rational::div(int n, int d)
{
	// divide fractions
	num *= d;
	den *= n;
}

/**
* @name gcd
* @brief uses recursion to find a fractions greatest common divisor
* @param numerator and denominator
* @retval greatest common divisor
**/

int gcd(int n, int d)
{
	// solve for greatest common divisor
	if (d == 0)
	{
		return n;
	}
	return gcd(d, n % d);
}
