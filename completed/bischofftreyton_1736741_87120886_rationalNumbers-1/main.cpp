/**
* @name main
* @brief perfroms basic arithmetic on fractions
* @param none
* @retval none
**/

#include <iostream>
#include <fstream>
#include <istream>
#include <string>

#include "rational.h"

using namespace std;

int main(void)
{
	Rational frac;
	int n, d, selec;

	// ask for and get fraction
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;

	// make class fraction entered fraction and reduce
	frac.set_values(n, d);
	frac.reduce();

	// print reduced fraction
	cout << "Fraction reduces to ";
	frac.print();

	// loop
	while (true)
	{
		cout << endl << endl;
		// print instructions
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter Selection: ";
		cin >> selec;
		
		// if the user wants to add
		if (selec == 1)
		{
			// get new fraction
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// add fractions
			frac.add(n, d);
		}
		// if user wants to subtrac
		else if (selec == 2)
		{
			// get new fraction
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// subtract fractions
			frac.sub(n, d);
		}
		// if user wants to multiply
		else if (selec == 3)
		{
			// get new fraction
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// multiply fractions
			frac.mul(n, d);
		}
		// if user wants to divide
		else if (selec == 4)
		{
			// get new fraction
			cout << "Enter numerator: ";
			cin >> n;
			cout << "Enter denominator: ";
			cin >> d;
			// divide fractions
			frac.div(n, d);
		}
		// if user wants to exit
		else if (selec == 0)
		{
			// exit
			return 0;
		}
		// if user enters invalid command
		else if (selec < 0 || selec > 4)
		{
			try
			{
				throw 1;
			}
			catch (int wrong)
			{
				cout << "Illegal menu selection!" << endl;
			}
		}
		// reduce fractions
		frac.reduce();
		// print fraction
		frac.print();
	}
	// end loop
	return 0;
}