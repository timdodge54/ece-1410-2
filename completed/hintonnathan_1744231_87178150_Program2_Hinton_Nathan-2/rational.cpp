//Nathan Hinton

#include <iostream>
#include "rational.h"

//public functions:

/// @name Rational
/// @brief Overrides the default constructor for the Rational class
/// @param int n, int d
/// @retval None
Rational::Rational(int n, int d) {
    Rational::num = n;
    Rational::den = d;
    Rational::reduce();
    Rational::print();
}

/// @name Rational::add
/// @brief Adds two rational numbers together
/// @param int n, int d
/// @retval None
void Rational::add(int n, int d) {
    Rational::num = (Rational::num*d)+(n*Rational::den);
    Rational::den = Rational::den*d;
    Rational::print();
    Rational::reduce();
    Rational::print();
}

/// @name Rational::sub
/// @brief Subtracts two rational numbers
/// @param int n, int d
/// @retval None
void Rational::sub(int n, int d) {
    Rational::num = (Rational::num*d)-(n*Rational::den);
    Rational::den = Rational::den*d;
    Rational::reduce();
    Rational::print();
}

/// @name Rational::mul
/// @brief Multiplies two rational numbers
/// @param int n, int d
/// @retval None
void Rational::mul(int n, int d) {
    Rational::den = Rational::den*d;
    Rational::num = Rational::num*n;
    Rational::reduce();
    Rational::print();
}

/// @name Rational::div
/// @brief Inverts a rational fraction and calls Rational::mul
/// @param int n, int d
/// @retval None
void Rational::div(int n, int d) {
    Rational::mul(d, n);
}

//Private functions:

/// @name Rational::print
/// @brief Prints the value of the fraction
/// @param None
/// @retval None
void Rational::print() {
    std::cout << Rational::num << "/" << Rational::den << std::endl;
}

/// @name Rational::reduce
/// @brief Reduces a fraction to the simplest form
/// @param None
/// @retval None
void Rational::reduce() {
    int i = 2;
    while (i < (Rational::den/2)+1) {
        if (0 == Rational::den % i && 0 == Rational::num % i) {
            Rational::den = Rational::den / i;
            Rational::num = Rational::num / i;
            i = 2;
        } else {
            i ++;
        }
    }
}