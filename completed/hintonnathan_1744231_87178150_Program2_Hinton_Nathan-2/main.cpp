//Nathan Hinton

#include <iostream>
#include "rational.h"

using namespace std;

/// @name main
/// @brief The entry point for the program
/// @param None
/// @retval None
int main() {
    int n, d;
    char task;
    bool exit = false;
    cout << "Enter numerator: ";
    cin >> n;
    cout << "Enter denominator: ";
    cin >> d;
    Rational r(n, d);
    //Begin loop:
    while (false == exit){
        cout << endl << endl << "1. Add a rational" << endl;
        cout << "2. Subtract a rational" << endl <<"3. Multiply by a rational";
        cout << endl << "4. Divide by a rational" << endl << "0. Exit" << endl;
        cout << "Enter selection: ";
        cin >> task;
        try {
            if ('0' == task) { //Do nothing and exit loop
                exit = true;
            } else if (49 <= task && task <= 52) {
                cout << "Enter numerator: ";
                cin >> n;
                cout << "Enter denominator: ";
                cin >> d;
            }
            if ('1' == task) {
                //add a rational
                r.add(n, d);
            } else if ('2' == task) {
                //subtract a rational
                r.sub(n, d);
            } else if ('3' == task) {
                //multiply by a rational
                r.mul(n, d);
            } else if ('4' == task) {
                //divide by a rational
                r.div(n, d);
            } else {//This is redundant. You never should reach this code
                //cout << "Throwing from specific";
                throw task;
            }
        } catch(char e) {
            cout << e << " is not a valid menu option";
        }
    }
    return 0;
}
