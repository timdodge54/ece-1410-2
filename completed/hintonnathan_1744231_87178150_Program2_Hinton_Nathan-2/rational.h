//Nathan Hinton

#pragma once
class Rational {
    private:
        int num, den;
        void print();
        void reduce();
    public:
        Rational(int n, int d);
        void add(int n, int d);
        void sub(int n, int d);
        void mul(int n, int d);
        void div(int n, int d); 
};