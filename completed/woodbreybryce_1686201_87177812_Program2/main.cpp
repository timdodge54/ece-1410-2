#include "rational.h"
#include <iostream>
using namespace std;

/**
@name	main
@brief	Computes math operations on rational numbers
@param	none
@retval	none
**/

int main ()
{
	int e = 5;
	int numerator, denominator;
	
	//ask for first rational
	cout << "Enter a numerator: ";
	cin >> numerator;
	cout << "Enter a denominator: ";
	cin >> denominator;
	
	//input rational through constructor
	Rational frac (numerator, denominator);
	
	//print fractions
	cout << "Fraction entered is: ";
	frac.print();
	cout << endl << endl;
	
	//Ask for numerator and denominator
	while (e != 0)
	{
		//display options menu with try block
		try
		{
			cout << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a rational" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;
			cout << "Enter a selection: ";
			
			//get selection and throw it
			cin >> e;
			throw e;
		}
		
		//catch menu selection
		catch (int e)
		{
			//Add if e is 1
			if (e == 1)
			{
				cout << "Enter a numerator: ";
				cin >> numerator;
				cout << "Enter a denominator: ";
				cin >> denominator;
				frac.add(numerator, denominator);
			}
			
			//Subtract if e is 2
			else if (e == 2)
			{
				cout << "Enter a numerator: ";
				cin >> numerator;
				cout << "Enter a denominator: ";
				cin >> denominator;
				frac.sub(numerator, denominator);
			}
			
			//Multiply if e is 3
			else if (e == 3)
			{
				cout << "Enter a numerator: ";
				cin >> numerator;
				cout << "Enter a denominator: ";
				cin >> denominator;
				frac.mul(numerator, denominator);
			}
			
			//Divide if e is 4
			else if (e == 4)
			{
				cout << "Enter a numerator: ";
				cin >> numerator;
				cout << "Enter a denominator: ";
				cin >> denominator;
				frac.div(numerator, denominator);
			}
			
			//Exit if e is 0
			else if (e == 0)
			{
				return 0;
			}
			
			//Error for non menu available selections
			else
			{
				cout << "'" << e << "'" << "is not a valid menu option!";
				cout << endl;
			}
			
			//Print new fraction after reducing
			frac.reduce();
			frac.print();
			cout << endl << endl;
		}
		
		//Default error exception
		catch (...)
		{
			cout << "'" << e << "'" << "is not a valid menu option!" << endl;
		}
	}
	
	return 0;
}