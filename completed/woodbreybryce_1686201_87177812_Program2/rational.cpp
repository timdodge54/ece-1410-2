#include "rational.h"
#include <iostream>
using namespace std;

/**
@name	Rational
@brief	Constructor for the Rational class. sets rational and reduces rational
@param	integer for numerator, and integer for denominator
@retval	none
**/
Rational::Rational(int n, int d)
{
	numerator = n;
	denominator = d;
	reduce();
}

/**
@name	add
@brief	Adds two rational numbers together
@param	integer for numerator and denominator to be added
@retval	none
**/
void Rational::add(int n, int d)
{
	tempNumerator = (numerator * d) + (n * denominator);
	denominator = denominator * d;
	numerator = tempNumerator;
}

/**
@name	sub
@brief	Computes math operations on rational numbers
@param	integer for numerator and denominator to be subtracted
@retval	none
**/
void Rational::sub(int n, int d)
{
	tempNumerator = (numerator * d) - (n * denominator);
	denominator = denominator * d;
	numerator = tempNumerator;
}

/**
@name	mul
@brief	Multiplies two rational numbers
@param	integer for numerator and denominator to divide
@retval	none
**/
void Rational::mul(int n, int d)
{
	numerator *= n;
	denominator *= d;
}

/**
@name	div
@brief	divides two rational numbers
@param	integer for numerator and denominator to be multiplied
@retval	none
**/
void Rational::div(int n, int d)
{
	numerator *= d;
	denominator *= n;
}

/**
@name	print
@brief	Uses private class variables to print rational number to screen
@param	none
@retval	none
**/
void Rational::print()
{
	cout << numerator << "/" << denominator << endl;
}

/**
@name	reduce
@brief	Reduces a rational number using the gcf
@param	none
@retval	none
**/
void Rational::reduce()
{
	int factor = 1;
	int num = numerator, denom = denominator;
	
	//reduce numbers equal to 1 to 1/1
	if (num == denom)
	{
		factor = num;
		numerator = numerator / factor;
		denominator = denominator / factor;
	}
	
	//find gcf then divide original numbers by gcf
	while (num != denom)
	{
		if (num > denom)
		{
			num -= denom;
		}
		if (num < denom)
		{
			denom -= num;
		}
		if (num == denom)
		{
			factor = num;
			numerator = numerator / factor;
			denominator = denominator / factor;
		}
	}
}