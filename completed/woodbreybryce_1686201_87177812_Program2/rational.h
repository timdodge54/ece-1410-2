
#ifndef RATIONAL_H
#define RATIONAL_H

class Rational
{
	private:
		int numerator, denominator;
		int tempNumerator;
	public:
		Rational(int n, int d);
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void print();
		void reduce();
};

#endif