#include <iostream>
#include "rational.h"

using namespace std;
int selector (void);
int gcf(int x, int y);

/******************************************************************************
@name	main
@brief	to run a program that allows to user to do operations on fractions
@praam	none	
@retval	none
******************************************************************************/
int main()
{
	int numerator, denom, input; 
	cout << "Numerator: ";	//asks for input
	cin >> numerator;		//gets input
	cout << "Denominator: ";
	cin >> denom;
	
	Rational old (numerator,denom); //sets values for old through constructor
	old.simplify();			//simplifys fraction through my function
	cout << "Fraction reduces to ";
	old.print();	//prints results using correlating function
	
	
	while(input== 0||1||2||3||4||5)//loop security statement. 
	{
		input = selector();			//recieve input through function
		if (input == 0){
			cout << "Exiting...";
			return 0;				//exits if input is 0
		}
	//in my selector loop. if any invalid input was detected. it returns 5
		else if (input == 5)
		{	
			cout << "INVALID INPUT"<<endl<<endl;//print fail
		}
		else
		{
			int nn, nd;
			cout << "Numerator:";
			cin >> nn;				//recieves new fraction operand
			cout << "Denominator:";
			cin >> nd;
			if (input == 1){	//if user selects 1...addition
				old.add(nn,nd);	//runs function adjusting private variables
			}
			else if (input == 2){//same thing as one
				old.sub(nn,nd);	 //just a different function
			}
			else if (input == 3){//and so on...
				old.mul(nn,nd);
			}
			else if (input == 4){
				old.div(nn,nd);
			}
			old.simplify();//after doing calculations the program simplifies...
			old.print();	//as well as prints values
		}
	}
	
	return 0;	//ends program... just being safe
}