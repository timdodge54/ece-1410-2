class Rational {
	private:
		int numerator, denom;
	public:
		Rational(int, int);
		void print();
		void add(int n, int d);
		void sub(int n, int d);
		void mul(int n, int d);
		void div(int n, int d);
		void simplify();
};