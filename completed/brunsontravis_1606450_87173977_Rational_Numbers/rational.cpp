#include "rational.h"
#include <cmath>
#include <iostream>
using namespace std;

/******************************************************************************
@name	gcf
@brief	find the greatest common factor
@praam	x and y	
@retval	a greatest common factor
******************************************************************************/
int gcf(int x, int y)
{ 
 if (x == y){ // if the values are the same it is the gcf
  return x; 
 } 
 else { 	
  if (x < y){ 		//if y is bigger subtract x from y
   return gcf(x, y-x); //send adjusted values back through the function
  } 
  else { 			//else x is bigger than y
   return gcf(x-y, y); //send adjusted values through program
  }   
 } 
} 
/******************************************************************************
@name	sub
@brief	subtract given fraction from private variables
@praam	n, d (the users operand)	
@retval	none
******************************************************************************/
void Rational::sub(int n, int d){
	numerator = (numerator * d)-(n * denom);	//upper subtraction
	denom = (denom * d);						//lower adjustment
}
/******************************************************************************
@name	add
@brief	add given fraction to private variables
@praam	n, d (the users operand)	
@retval	none
******************************************************************************/
void Rational::add(int n, int d){
	numerator = (numerator * d)+(n * denom);//upper addition
	denom = (denom * d);					//lower adjustment
}
/******************************************************************************
@name	mul
@brief	multiply given fraction to private variables
@praam	n, d (the users operand)	
@retval	none
******************************************************************************/
void Rational::mul(int n, int d){
	numerator = numerator * n;		//simple multiplication
	denom = denom * d;
}
/******************************************************************************
@name	div
@brief	divide given fraction from private variables
@praam	n, d (the users operand)	
@retval	none
******************************************************************************/
void Rational::div(int n, int d){
	numerator = numerator * d;	//simple division
	denom = denom * n;
}
/******************************************************************************
@name	print
@brief	to print the current fraction
@praam	none	
@retval	none
******************************************************************************/
void Rational::print()
{
	cout << numerator << "/" << denom<<endl<< endl;	//print to screen
}

/******************************************************************************
@name	Rational
@brief	Constructor for my private variable class, sets values
@praam	a, b	
@retval	none
******************************************************************************/
Rational::Rational (int a, int b) 
{
  numerator = a;		//sets internal private values
  denom = b;
}
/******************************************************************************
@name	simplify
@brief	simplify the private classes fractions
@praam	none	
@retval	none
******************************************************************************/
void Rational::simplify()
{
	int multi = gcf(numerator, denom);	//finds gcf and sets its value to multi
	numerator = numerator / multi;	// divides numerator by gcf
	denom = denom / multi;			//divides denom by multi
}

/******************************************************************************
@name	selector
@brief	to print options for the user and return their pick
@praam	none	
@retval	the users pick
******************************************************************************/
int selector (void)
{
	char e = 1; char input;		//ensures e is safe to loop by initializing
	while (e > 0 & e < 4) 		//while is is a valid input
	{
		cout << "1. Add a rational" << endl
		<< "2. Subtract a rational" << endl
		<< "3. Multiply by a rational" << endl	//this all is just printing
		<< "4. Divide by a rational"<<endl		//seperate options
		<< "0. Exit"<<endl<<"Enter selection: ";
	
		cin >> input; //recieve input
	
		try
		{
			if (input == '1'){throw 1;}//this section reads what the user
			else if (input == '2'){throw 2;}//has entered and throws the
			else if (input == '3'){throw 3;}//corresponding value
			else if (input == '4'){throw 4;}
			else if (input == '0'){throw 0;}// EG if the user entered 0.throw 0
			else if (input != '4'|| '3'|| '2'|| '1'|| '0'){throw 5;}
			}
		catch(int e){
			if (e == 1){		//this section returns the value the user
				return 1;		//entered. I could've just built an if else
			}					//block and returned instead of thrown
			else if (e == 2){	//but this shows that I understand how 
				return 2;		//try and catch blocks work.
			}
			else if (e == 3){
				return 3;
			}
			else if (e == 4){	//if it catches 4. it returns 4.
				return 4;
			}
			else if (e == 0){
				return 0;
			}
			else if (e == 5){
				return 5;
			}
		}	
	}
			
	return 0;
}
