#ifndef RATIONAL_H
#define RATIONAL_H

/*********************************************************************
* @name		Rational
* @brief	Contains a rational number (fraction) and functions to 
*			modify or display the fraction
*********************************************************************/

class Rational{
public:
	//member function prototypes
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void print();

	//class constructor
	Rational(int n, int d);
private:
	//variables for the numerator and denominator
	int num, den;

	//prototypes for private functions
	int gcf(int x, int y);
	void reduce();
};

#endif