#include "rational.h"
#include <iostream>

/*********************************************************************
* @name		Rational
* @brief	A constructor for the Rational class that defines its
*			numerator and denominator in reduced form
* @param	numerator
* @param	denominator
* @retval	none
*********************************************************************/

Rational::Rational(int n, int d) {
	//assign the numerator and denominator values
	num = n;
	den = d;

	//reduce the fraction
	reduce();
}

/*********************************************************************
* @name		gcf
* @brief	Finds the greatest common factor between two numbers
* @param	first number
* @param	second number
* @retval	greatest common factor
*********************************************************************/

int Rational::gcf(int x, int y) {
	//make sure the values aren't negative or zero
	x = abs(x);
	y = abs(y);
	//
	//if the two numbers are the same, that is the gcf
	if (x == y) {
		return x;
	}
	//if the first number is greater, subtract the second number from it
	//and start over
	else if (x > y) {
		return gcf(x - y, y);
	}
	//if the second number is greater, subtract the first number from it
	//and start over
	else {
		return gcf(x, y - x);
	}
}

/*********************************************************************
* @name		print
* @brief	prints the fraction
* @param	none
* @retval	none
*********************************************************************/

void Rational::print() {
	//prints the fraction
	std::cout << num << "/" << den << std::endl << std::endl << std::endl;
}

/*********************************************************************
* @name		add
* @brief	adds a new fraction to the existing one and reduces the sum
* @param	new numerator
* @param	new denominator
* @retval	none
*********************************************************************/

void Rational::add(int n, int d) {
	//get the numerator and denominator of the sum
	num =  (num * d) + (n * den);
	den = d * den;

	//reduce the answer
	reduce();
}

/*********************************************************************
* @name		sub
* @brief	subtracts a new fraction from the existing one and reduces 
*			the difference
* @param	new numerator
* @param	new denominator
* @retval	none
*********************************************************************/

void Rational::sub(int n, int d) {
	//get the numerator and denominator of the difference
	num = (num * d) - (n * den);
	den = d * den;

	//reduce the answer
	reduce();
}

/*********************************************************************
* @name		mul
* @brief	multiplies a new fraction by the existing one and reduces
*			the product
* @param	new numerator
* @param	new denominator
* @retval	none
*********************************************************************/

void Rational::mul(int n, int d) {
	//get the numerator and denominator of the product
	num = num * n;
	den = den * d;

	//reduce the answer
	reduce();
}

/*********************************************************************
* @name		div
* @brief	divides the existing fraction by a new one and reduces
*			the quotient
* @param	new numerator
* @param	new denominator
* @retval	none
*********************************************************************/

void Rational::div(int n, int d) {
	//get the numerator and denominator of the quotient
	num = num * d;
	den = den * n;

	//reduce the answer
	reduce();
}

/*********************************************************************
* @name		reduce
* @brief	reduces the stored fraction and ensures proper negative
*			sign placement
* @param	none
* @retval	none
*********************************************************************/

void Rational::reduce() {
	int f;

	//get the gcf of the two numbers
	f = gcf(num, den);

	//divide both by their gcf
	num /= f;
	den /= f;

	if (den < 0) {
		//if the numerator and denominator are negative
		if (num < 0) {
			//get rid of the negative signs
			num = abs(num);
			den = abs(den);
		}
		//if only the denominator is negative
		else {
			//put the negative sign on the numerator
			num = -num;
			den = abs(den);
		}
	}
}