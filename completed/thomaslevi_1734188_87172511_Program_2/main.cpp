#include "rational.h"
#include <iostream>
using namespace std;

/*********************************************************************
* @name		main
* @brief	Asks the user to input a fraction, then enters a menu
*			driven loop where the user can modify the fraction.
* @param	none
* @retval	none
*********************************************************************/

int main(void) {
	int n, d, quit;
	char selection;

	//ask for and collect fraction
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;

	//create the fraction object
	Rational frac(n, d);

	//print the reduced form
	cout << "Fraction reduces to ";
	frac.print();

	//enter the menu loop
	quit = 0;
	while (!quit) {
		try {
			//list options
			cout << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a rational" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;

			//get selection
			cout << "Enter selection: ";
			cin >> selection;

			//if addition is chosen
			if (selection == '1') {
				//get a fraction to add
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				//add it to the existing fraction
				frac.add(n, d);

				//print the result
				frac.print();
			}
			//if subtraction is chosen
			else if (selection == '2') {
				//get a fraction to subtract
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				//subtract it from the existing fraction
				frac.sub(n, d);

				//print the result
				frac.print();
			}
			//if multiplication is chosen
			else if (selection == '3') {
				//get a fraction to multiply
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				//multiply it by the existing fraction
				frac.mul(n, d);

				//print the result
				frac.print();
			}
			//if division is chosen
			else if (selection == '4') {
				//get a fraction to divide
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				//divide the existing fraction by it
				frac.div(n, d);

				//print the result
				frac.print();
			}
			else if (selection == '0') {
				return 0;
			}
			else {
				throw selection;
			}
		}
		catch (char c) {
			cout << "'" << c << "' is not a valid selection" << endl << endl << endl;
		}
	}

	return 0;
}