/*********************************************************************
* @name rational
* @brief location of public/private member functions
* @param none
* @retval  none
*********************************************************************/
#include <iostream>
#include <fstream>
#include <cmath>
#include "rational.h"

using namespace std;

/*********************************************************************
* @name add
* @brief adds two fractions
* @param n,d
* @retval  n,d
*********************************************************************/
	void rational::add(int n, int d)
	{
		num = n + num;
		denom = d + denom;
		rational::reduce(n, d);
	}
/*********************************************************************
* @name sub
* @brief subtracts two fractions
* @param n,d
* @retval  n,d
*********************************************************************/
	void rational::sub(int n, int d)
	{
		num = num - n;
		denom = denom - d;
	}
/*********************************************************************
* @name mul
* @brief multiplies two fractions
* @param n,d
* @retval  n,d
*********************************************************************/
	void rational::mul(int n, int d)
	{
		num = n * num;
		denom = d * denom;
	}
/*********************************************************************
* @name div
* @brief divides two fractions
* @param n,d
* @retval  n,d
*********************************************************************/
	void rational::div(int n, int d)
	{
		num = num * d;
		denom = denom * n;
	}

/*********************************************************************
* @name reduce
* @brief simplifies the fraction
* @param n,d
* @retval n,d
*********************************************************************/
	void rational::reduce(int n, int d)
	{
		

		int i;
		bool flag;
		if (n < d) {
			i = n;
		}
		else {
			i = d;
		}
		flag = false;
		while (!flag) {
			if ((n % i == 0) && (d % i == 0)) {
				flag = true;
			}
			else {
				i--;
			}
		}
		num = n / i;
		denom = d / i;
	}
