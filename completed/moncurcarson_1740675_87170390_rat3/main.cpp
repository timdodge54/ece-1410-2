/*********************************************************************
* @name main
* @brief
* @param none
* @retval  none
*********************************************************************/
#include <iostream>
#include <fstream>
#include <cmath>
#include "rational.h"

using namespace std;
int main()
{
	int num, denom, n, d;
	unsigned int input;
	bool exit = false;
	

	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> denom;
	rational rat(num, denom);


	cout << "Fraction reduces to " << rat.getN()<< "/" << rat.getD() << endl;

	while (!exit)
	{
		cout << "1. Add a rational" << endl << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl << endl;

		cout << "Enter selection: ";
		try
		{
			cin >> input;
			if (input > 4)
			{
				throw input;
			}
			switch (input)
				{
				case 1:
					cout << "Enter numerator: ";
					cin >> n;
					cout << "Enter denominator: ";
					cin >> d;
					rat.add(n, d);
					cout << "Fraction reduces to " << rat.getN() << "/" << rat.getD() << endl << endl;

					break;

					case 2:
						cout << "Enter numerator: ";
						cin >> n;
						cout << "Enter denominator: ";
						cin >> d;
						rat.sub(n, d);
						cout << "Fraction reduces to " << rat.getN() << "/" << rat.getD() << endl << endl;

						break;

					case 3:
						cout << "Enter numerator: ";
						cin >> n;
						cout << "Enter denominator: ";
						cin >> d;
						rat.mul(n, d);
						cout << "Fraction reduces to " << rat.getN() << "/" << rat.getD() << endl << endl;

						break;

					case 4:
						cout << "Enter numerator: ";
						cin >> n;
						cout << "Enter denominator: ";
						cin >> d;
						rat.div(n, d);
						cout << "Fraction reduces to " << rat.getN() << "/" << rat.getD() << endl << endl;

						break;

					case 0:
						exit = true;
						break;

				}

			



			
		}
		catch (unsigned int E)
		{
			cout << E << " is an illegal menu option" << endl << endl;
		}
	}
}