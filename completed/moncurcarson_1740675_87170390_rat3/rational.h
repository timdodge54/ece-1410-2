#ifndef RATIONAL_H
#define RATIONAL_H

class rational {
public:
	rational(int n, int d) { rational::reduce(n, d); }
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	int getN() { return num; }
	int getD() { return denom; }

private:
	void reduce(int n, int d);
	int num, denom;
};
#endif