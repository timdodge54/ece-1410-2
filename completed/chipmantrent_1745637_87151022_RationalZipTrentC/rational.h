#ifndef Rationalh
#define Rationalh
//things to include and declare
#include <iostream>
using namespace std;

//class for Rational numbers
class Rational {
private:
    int num, den; 
    int GCF;
    int gcf(int n, int d);
public:
    Rational(int n, int d);
    void add(int n, int d);
    void sub(int n, int d);
    void mul(int n, int d);
    void div(int n, int d);
};
#endif