//include rational class and necesary library and namespace ceclarations
#include "rational.h"
/**************************************************
* @name Rational
* @brief sets original fraction stored in class
* @param integers for numerator and denominator
* @retval None
*************************************************/
Rational::Rational(int n, int d) {
	//set fraction numbers to those given
    num = n;
    den = d;
	//reduce fraction
	//find greatest common factor and divide both numbers by it
	GCF = gcf(num, den);
	num = (num / GCF);
	den = (den / GCF);
	//print reduced fraction
	cout << "Fraction reduces to " << num << "/" << den << endl;
}
/**************************************************
* @name Rational
* @brief sets original fraction stored in class
* @param integers for numerator and denominator
* @retval returns the greatest common factor of two numbers
*************************************************/
int Rational::gcf(int n, int d) {
	//when not dividing by zero, recursively get remainder to find GCF
	if (d != 0) {
		return gcf(d, (n % d));
	}
	else {
		return n;
	}
}
/**************************************************
* @name add
* @brief adds new fraction to fraction stored
* @param integers for numerator and denominator
* @retval None
*************************************************/
void Rational::add(int n, int d) {
	//get same denominator and add together
	num = (num * d) + (n * den);
	den = (den * d);
	//find greatest common factor and divide both numbers by it
	GCF = gcf(num, den);
	num = (num / GCF);
	den = (den / GCF);
	//print to screen
	cout << num << "/" << den << endl;
}
/**************************************************
* @name sub
* @brief subtracts new fraction from fraction stored
* @param integers for numerator and denominator
* @retval None
*************************************************/
void Rational::sub(int n, int d) {
	//get same denominator and minus
	num = (num * d) - (n * den);
	den = (den * d);
	//find greatest common factor and divide both numbers by it
	GCF = gcf(num, den);
	num = (num / GCF);
	den = (den / GCF);
	//print to screen
	cout << num << "/" << den << endl;
}
/**************************************************
* @name mul
* @brief multiplies new fraction with fraction stored
* @param integers for numerator and denominator
* @retval None
*************************************************/
void Rational::mul(int n, int d) {
	//multiply across
	num = (num * n);
	den = (den * d);
	//find greatest common factor and divide both numbers by it
	GCF = gcf(num, den);
	num = (num / GCF);
	den = (den / GCF);
	//print to screen
	cout << num << "/" << den << endl;
}
/**************************************************
* @name div
* @brief divides fraction stored by new fraction
* @param integers for numerator and denominator
* @retval None
*************************************************/
void Rational::div(int n, int d) {
	//multiply inversely across
	num = (num * d);
	den = (den * n);
	//find greatest common factor and divide both numbers by it
	GCF = gcf(num, den);
	num = (num / GCF);
	den = (den / GCF);
	//print to screen
	cout << num << "/" << den << endl;
}


