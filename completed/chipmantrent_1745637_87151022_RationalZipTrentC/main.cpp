/*************************************************
* Function Title: main
*
* Summary: Uses rational class to ask and compute user selected operations
*
* Inputs: None
* Outputs: None
*
**************************************************
* @name main
* @brief Uses rational class to ask and compute user selected operations
* @param None
* @retval None
*************************************************/
//include rational class and necesary library and namespace ceclarations
#include "rational.h"

int main(){
	//declare numerator and denominator variables, and selection variables
	int numer = 0, denom = 0, sel = 1;
	char c;
	//ask for numerator and denominator
	cout << "Enter numerator: ";
	cin >> numer;
	cout << "Enter denominator: ";
	cin >> denom;
	//set first fraction and print reduced fraction
	Rational fract(numer, denom);

	while (sel != 0) {
		//give user selection options
		cout << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		//get character
		cin >> c;
		//convert if integer
		sel = (c - 48);
		//do operation specified by selection
		if (sel == 0) {
			//end
			return 0;
		}
		else if (sel == 1) {
			//ask for numerator and denominator
			cout << "Enter numerator: ";
			cin >> numer;
			cout << "Enter denominator: ";
			cin >> denom;
			//add
			fract.Rational::add(numer, denom);
		}
		else if (sel == 2) {
			//ask for numerator and denominator
			cout << "Enter numerator: ";
			cin >> numer;
			cout << "Enter denominator: ";
			cin >> denom;
			//subtract
			fract.Rational::sub(numer, denom);
		}
		else if (sel == 3) {
			//ask for numerator and denominator
			cout << "Enter numerator: ";
			cin >> numer;
			cout << "Enter denominator: ";
			cin >> denom;
			//multiply
			fract.Rational::mul(numer, denom);
		}
		else if (sel == 4) {
			//ask for numerator and denominator
			cout << "Enter numerator: ";
			cin >> numer;
			cout << "Enter denominator: ";
			cin >> denom;
			//divide
			fract.Rational::div(numer, denom);
		}
		else {
			try
			{
				throw sel;
			}
			catch (int sel)
			{
				cout << "ASCII #: '" << sel << "' " << "is not a valid selection" << endl;
			}
		}
	}
	return 0;
}