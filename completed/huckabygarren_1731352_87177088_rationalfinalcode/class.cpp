
#include "class.h"

/*************************************************************************
*	@name	Rational
*	@brief  Constructor for class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
Rational::Rational(int n, int d)
{
	numerator = n;
	denominator = d;
	reduce();
	print();
}
//establish constructor for class Rational
/*************************************************************************
*	@name	print
*	@brief  Private function called to print members of class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::print()
{
	cout << "Fraction reduces to: ";
	cout << numerator << '/' << denominator;
}
//print reduced numerator and denominator value of other functions
/*************************************************************************
*	@name	reduce
*	@brief  Private function called to reduce members of class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::reduce()
{
	int num = numerator;
	int den = denominator;
	//create temporary variables for denominator and numerator
	while (numerator != denominator)
	{
		//loop while numerator != denominator
		if (numerator == denominator)
		{
		}
		//if equal, exit loop
		if (numerator > denominator)
		{
			numerator = numerator - denominator;
		}
		//if numerator is greater, numerator become numerator subtract denominator
		if (numerator < denominator)
		{
			denominator = denominator - numerator;
		}
		//if denominator is greater, denominator equals denominator subtract numerator
	}
	numerator = num / numerator;
	denominator = den / denominator;
}
//divide numerator and denominator by GCF


/*************************************************************************
*	@name	add
*	@brief  Addition function for class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::add(int n, int d)
{
	numerator = (n * denominator) + (numerator * d);
	denominator = (d * denominator);
	reduce();
	print();
}
//find common denominator and add together	
/*************************************************************************
*	@name	sub
*	@brief  Subtraction function for class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::sub(int n, int d)
{
	numerator = ((numerator * d) - (n * denominator));
	denominator = (d * denominator);
	reduce();
	print();
}
//find common denominator and subtract
/*************************************************************************
*	@name	mul
*	@brief  Multiplication function for class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::mul(int n, int d)
{
	numerator = (n * numerator);
	denominator = (d * denominator);
	reduce();
	print();
}
//multiply straight across
/*************************************************************************
*	@name	div
*	@brief  Division function for class Rational
*	@param	n, d
*	@retval	none
*************************************************************************/
void Rational::div(int n, int d)
{
	denominator = (n * denominator);
	numerator = (d * numerator);
	reduce();
	print();
}
//cross multiply