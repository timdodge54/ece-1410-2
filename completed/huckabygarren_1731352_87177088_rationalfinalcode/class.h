/*************************************************************************
*	@name	class
*	@brief Defines class Rational
*	@param	none
*	@retval	none
*************************************************************************/

#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>

#ifndef RATIONAL_H
#define RATIONAL_H
using namespace std;

class Rational {
public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);

private:
	void reduce();
	void print();
	int numerator, denominator;
};
//define public and private sections of class
#endif#pragma once
