/*************************************************************************
*	@name	main
*	@brief Takes a fraction, reduces it to simplest form, and then
* provides a list of possible algorithmic functions that can be employed.
* it then employs the use of functions to compute and reduce functions
* until the user inputs zero.
*	@param	none
*	@retval	none
*************************************************************************/

#include <fstream>
#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include "class.h"

using namespace std;

int main()
{
	int math, numerator, denominator;

	cout << "Enter a numerator: ";
	cin >> numerator;
	cout << "Enter denominator: ";
	cin >> denominator;
	//get initial fraction values	
	Rational rational(numerator, denominator);
	//run through constructor, and reduce	
	while (math != 0)
	{
		cout << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> math;
		//provide operation option display		
		try
		{
			if (math < 0 || math > 4)
				throw 1;
		}
		catch (int e)
		{
			if (e == 1)
				cout << "Invalid input" << endl;
		}
		//filter for incorrect input values
		switch (math)
		{
		case 1:
		{
			cout << "Enter numerator: ";
			cin >> numerator;
			cout << "Enter denominator: ";
			cin >> denominator;
			rational.add(numerator, denominator);
			break;
		}
		//call addition function
		case 2:
		{
			cout << "Enter numerator: ";
			cin >> numerator;
			cout << "Enter denominator: ";
			cin >> denominator;
			rational.sub(numerator, denominator);
			break;
		}
		//call subtraction function
		case 3:
		{
			cout << "Enter numerator: ";
			cin >> numerator;
			cout << "Enter denominator: ";
			cin >> denominator;
			rational.mul(numerator, denominator);
			break;
		}
		//call multiplication function
		case 4:
		{
			cout << "Enter numerator: ";
			cin >> numerator;
			cout << "Enter denominator: ";
			cin >> denominator;
			rational.div(numerator, denominator);
			break;
		}
		//call division function
		default:
		{
			break;
		}
		//call default
		}
	}

	return 0;
}