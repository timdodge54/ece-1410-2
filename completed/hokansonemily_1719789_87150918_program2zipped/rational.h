#pragma once

class Rational {
	int num, denom;
	void reduce();
public:
	Rational(int n, int d);
	void add(int n, int d);
	void sub(int n, int d);
	void mul(int n, int d);
	void div(int n, int d);
	void print();
};