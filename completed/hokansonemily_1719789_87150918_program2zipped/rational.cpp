#include <iostream>
#include "rational.h"

using namespace std;

/***********************************************
* @name		Rational
* @brief	constructor for class Rational, initiates variables with inputs from user
* @param	int numerator, int denominator
* @retval	none
***********************************************/
Rational::Rational(int n, int d) {
	//set values of num, denom
	num = n;
	denom = d;

	//print reduced fraction
	cout << "Fraction reduces to ";
	print();
}

/***********************************************
* @name		add
* @brief	adds two fractions
* @param	int numerator, int denominator
* @retval	none
***********************************************/
void Rational::add(int n, int d) {
	//cross multiply and add numerators
	n *= denom;
	num *= d;
	num += n;

	//cross multiply denominators
	denom *= d;
}


/***********************************************
* @name		sub
* @brief	subtracts two fractions
* @param	int numerator, int denominator
* @retval	none
***********************************************/
void Rational::sub(int n, int d) {
	//cross multiply and subtract numerators
	n *= denom;
	num *= d;
	num -= n;

	//cross multiply denominators
	denom *= d;
}


/***********************************************
* @name		mul
* @brief	multiplies two fractions
* @param	int numerator, int denominator
* @retval	none
***********************************************/
void Rational::mul(int n, int d) {
	//multiply numerators
	num *= n;

	//multiply denominators
	denom *= d;
}


/***********************************************
* @name		div
* @brief	divides two fractions
* @param	int numerator, int denominator
* @retval	none
***********************************************/
void Rational::div(int n, int d) {
	//multiply by reciprocal
	num *= d;
	denom *= n;
}



/***********************************************
* @name		print
* @brief	reduces and prints fraction to screen
* @param	none
* @retval	none
***********************************************/
void Rational::print() {
	//reduce fraction
	reduce();

	//print to screen
	cout << num << "/" << denom << endl;
}


/***********************************************
* @name		reduce
* @brief	reduces fraction using the euclidian algorithm
* @param	none
* @retval	none
***********************************************/
void Rational::reduce() {
	//use euclidian algorithm to find gfc

	//set temp variables
	int tempn = num, tempd = denom;

	//loop until a = b
	while (tempn != tempd) {

		//if a > b, (a-b, b)
		if (tempn > tempd) {
			tempn -= tempd;
		}
		else {
			//if b > a, (a, b-a)
			tempd -= tempn;
		}
	}

	//use gfc to reduce num and denom
	num /= tempn;
	denom /= tempn;
}