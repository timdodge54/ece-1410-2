#include <iostream>
#include "rational.h"

using namespace std;

/***********************************************
* @name		main
* @brief	performs arithmetic on rational fractions
* @param	none
* @retval	0
***********************************************/

int main() {
	//declare variables
	int n, d;
	char selection = '1';

	//get first fraction from user
	cout << "Enter numerator: ";
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;

	//set fraction value
	Rational fraction(n, d);

	//loop to ask for operation
	while (selection != '0') {
		cout << endl << endl;
		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";
		cin >> selection;



		//perform operation selected, check for exceptions
		try {
			//if selection is 1-4, perform operation
			if (selection > '0' && selection < '5') {

				//get fraction values from user
				cout << "Enter numerator: ";
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;

				//perform selected operation
				if (selection == '1') {
					fraction.add(n, d);
				}
				else if (selection == '2') {
					fraction.sub(n, d);
				}
				else if (selection == '3') {
					fraction.mul(n, d);
				}
				else {
					fraction.div(n, d);
				}

				//output solution
				fraction.print();

			}
			//if selection is 0, exit
			else if (selection == '0') {
				return 0;
			}
			else {
				//if selection is illegal, print error and continue
				throw selection;
			}
		}
		catch (...) {
			//print error and continue
			cout << "'" << selection << "' is not a valid input." << endl;
			selection = '1';
		}


	}

	return 0;
}