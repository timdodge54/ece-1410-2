#include <iostream>
#include "rational.h"
using namespace std;

/*****************************************************************************
* @name Rational
* @brief constructor function for the Rational class, sets first fraction
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

Rational::Rational(int n, int d)
{
	numerator = n;
	denominator = d;
	reduce(numerator, denominator);
	cout << "Fraction reduces to " << numerator << "/" << denominator
		<< endl << endl << endl;
}


/*****************************************************************************
* @name add
* @brief adds 2 fractions together
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

void Rational::add(int n, int d)
{
	numerator = (numerator * d) + (denominator * n);
	denominator *= d;
	reduce(numerator, denominator);
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/*****************************************************************************
* @name sub
* @brief subtracts 2 fractions from eachother
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

void Rational::sub(int n, int d)
{
	numerator = (numerator * d) - (denominator * n);
	denominator *= d;
	reduce(numerator, denominator);
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/*****************************************************************************
* @name mul
* @brief multiplies 2 fractions together
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

void Rational::mul(int n, int d)
{
	numerator *= n;
	denominator *= d;
	reduce(numerator, denominator);
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/*****************************************************************************
* @name div
* @brief divides 2 fractions from eachother
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

void Rational::div(int n, int d)
{
	numerator *= d;
	denominator *= n;
	reduce(numerator, denominator);
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/*****************************************************************************
* @name reduce
* @brief takes current fraction and reduces it to its smallest form
* @param inputs 2 numbers, one being the numerator, the other the denominator
* @retval no output
*****************************************************************************/

void Rational::reduce(int n, int d)
{
	int n1, n2, i;
	bool flag;
	n1 = n;
	n2 = d;
	if (n1 < n2)
	{
		i = n1;
	}
	else {
		i = n2;
	}
	flag = false;
	while (!flag) {
		if ((n1 % i == 0) && (n2 % i == 0)) {
			flag = true;
		}
		else {
			i--;
		}
	}
	numerator /= i;
	denominator /= i;
}