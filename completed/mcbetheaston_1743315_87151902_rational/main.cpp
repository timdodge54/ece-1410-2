/*****************************************************************************
* @name main
* @brief the user enters an initial fraction and are then given options to
*		 add, subtract, multiply, or divide that fraction with another
* @param 2 numbers, one for the numerator and one for the denominator, then
*		 the user enters a number 0-4 for the menu selection
* @retval outputs the new fraction based on menu option selected
*****************************************************************************/

#include <iostream>
#include "rational.h"

using namespace std;

int main()
{
	int menurun = 1, num, denom, selection, validselect;
	cout << "Enter numerator: ";
	cin >> num;
	cout << "Enter denominator: ";
	cin >> denom;

	Rational current(num, denom);

	while (menurun == 1) {
		validselect = 1;

		cout << "1. Add a rational" << endl;
		cout << "2. Subtract a rational" << endl;
		cout << "3. Multiply by a rational" << endl;
		cout << "4. Divide by a rational" << endl;
		cout << "0. Exit" << endl;
		cout << "Enter selection: ";

		//cin >> selection;

		/*if (selection == 0)
		{
			return 0;
		}*/

		try {
			cin >> selection;
			if (cin.fail())
				throw (2);
			if (selection < 0 || selection > 4) {
				throw(selection);
			}
		}
		catch (int wronginput) {
			if (wronginput == 2)
			{
				cout << "Input needs to be a number" << endl << endl;
			}
			else
			{
				cout << "'" << selection << "' " << "isn't a valid selection" 
					<< endl << endl << endl;
			}
			validselect = 0;
		}

		if (selection == 0)
		{
			cout << endl;
			return 0;
		}
		
		if (validselect == 1)
		{
			cout << "Enter numerator: ";
			cin >> num;
			cout << "Enter denominator: ";
			cin >> denom;

			if (selection == 1)
			{
				current.add(num, denom);
			}
			else if (selection == 2)
			{
				current.sub(num, denom);
			}
			else if (selection == 3)
			{
				current.mul(num, denom);
			}
			else if (selection == 4)
			{
				current.div(num, denom);
			}
		}

	}

	return 0;
}