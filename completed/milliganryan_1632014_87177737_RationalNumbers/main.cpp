/**
@name Rational Numbers
@brief a user interface that can do basic arithmetic on rational numbers
@param none
@retval 0
*/
#include "rational.h"

int main()
{
	Rational r;
	int n, d, selection;
	cout << "Enter numerator: ";	//get initial frac
	cin >> n;
	cout << "Enter denominator: ";
	cin >> d;
	r.setvalues(n, d);
	cout << "Fraction reduces to " << n/r.reduce(n, d) << "/" <<
		d/r.reduce(n, d) << endl << endl << endl;	//reduce fraction

	selection = 5;
	while (selection != 0)						//loop print menu
	{
		try {
			cout << "1. Add a rational" << endl;
			cout << "2. Subtract a rational" << endl;
			cout << "3. Multiply by a raional" << endl;
			cout << "4. Divide by a rational" << endl;
			cout << "0. Exit" << endl;
			cout << "Enter selection: ";			//get selection

			cin >> selection;					//look for bad input

			switch (selection)						//Menu options
			{
			case 1:
			{
				cout << "Enter numerator: ";		//Addition
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
				r.add(n, d);						//addition function call
				break;
			}
			case 2:
			{
				cout << "Enter numerator: ";		//Subtraction
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
				r.sub(n, d);					//subtraction function call
				break;
			}
			case 3:
			{
				cout << "Enter numerator: ";		//Multiplication
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
				r.mul(n, d);					//multiplication function call
				break;
			}
			case 4:
			{
				cout << "Enter numerator: ";		//Division
				cin >> n;
				cout << "Enter denominator: ";
				cin >> d;
				r.div(d, n);						//division function call
				break;
			}
			case 0:									//complete
			{
				cout << endl << "Session Terminated";
				exit(EXIT_SUCCESS);
			}
			default:
				throw "Input Invalid";
			}
		}
		catch (const char* msg)						//bad input
		{
			cout << msg << endl << endl << endl;
		}
	}
return 0;
}