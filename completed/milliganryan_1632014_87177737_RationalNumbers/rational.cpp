#include "rational.h"
/**
@name set values
@brief sets private data to user input
@param n numerator
@param d denominator
@retval none
*/
void Rational::setvalues(int n, int d)
{
	numerator = n;
	denominator = d;
}

/**
@name add
@brief adds two fractions
@param n numerator
@param d denominator
@retval none
*/
void Rational::add(int n, int d)
{
	int lcm;
	lcm = denominator * d;
	numerator = (numerator*d) + (n*denominator);
	denominator = lcm;
	reduce();
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/**
@name subtract
@brief subtracts two fractions
@param n numerator
@param d denominator
@retval none
*/
void Rational::sub(int n, int d)
{
	int lcm;
	lcm = denominator * d;
	numerator = (numerator * d) - (n * denominator);
	denominator = lcm;
	reduce();
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/**
@name multiply
@brief multiplies two fractions
@param n numerator
@param d denominator
@retval none
*/
void Rational::mul(int n, int d)
{
	numerator = numerator * n;
	denominator = denominator * d;
	reduce();
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/**
@name divide
@brief divides two fractions
@param n numerator
@param d denominator
@retval none
*/
void Rational::div(int n, int d)
{
	numerator = numerator * n;
	denominator = denominator * d;
	reduce();
	cout << numerator << "/" << denominator << endl << endl << endl;
}

/**
@name reduce
@brief reduces a fraction
@param none
@retval none
*/
void Rational::reduce()
{
	int a, b;
	a = numerator;
	b = denominator;
	while (a != b)
	{
		if (a > b)
		{
			a = a - b;
		}
		else
		{
			b = b - a;
		}
	}
	numerator = numerator / a;
	denominator = denominator / a;
}

/**
@name reduce
@brief returns gcf of a fraction
@param n numerator
@param d denominator
@retval greatest common factor
*/
int Rational::reduce(int n, int d)
{
	int a, b;
	a = n;
	b = d;
	while (a != b)
	{
		if (a > b)
		{
			a = a - b;
		}
		else
		{
			b = b - a;
		}
	}
	return a;
}
