#pragma once
#ifndef RATIONAL_H
#define RATIONAL_H					//multiple inclusion check

#include <iostream>
#include <cmath>
#include <stdexcept>
using namespace std;

class Rational {
	private:
		int numerator, denominator;

	public:
		//Rational(int n, int d);
		void setvalues(int n, int d);
		void add(int n, int d);		//adds a fraction to the current fraction
		void sub(int n, int d);		//subtracts a fraction from the current
		void mul(int n, int d);		//multiplies the current by a new fraction
		void div(int n, int d);		//divides the currect by the new fraction
		void reduce();				//reduce fraction
		int reduce(int n, int d);		//reduce initial fraction
};
#endif 